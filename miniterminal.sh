#!/bin/bash

	CHECK_USB="check_usb"
echo "" > $CHECK_USB
for sysdevpath in $(find /sys/bus/usb/devices/usb*/ -name dev); do
    (

        syspath="${sysdevpath%/dev}"
        devname="$(udevadm info -q name -p $syspath)"
        [[ "$devname" == "bus/"* ]] && continue
        eval "$(udevadm info -q property --export -p $syspath)"
        [[ -z "$ID_SERIAL" ]] && continue

        serial="$(udevadm info -q name -p $syspath)"
        [[ "$serial" != "ttyUSB"* ]] && continue
        echo "$devname  \"$ID_SERIAL\"" >> $CHECK_USB
       
    )
done
sort $CHECK_USB -o $CHECK_USB
I=0
while read; do
   I=$(($I + 1))
   #echo "Linia $I: $REPLY"
value2+=($REPLY)
done < "$CHECK_USB"
rm $CHECK_USB

	DISTROS=$(whiptail --title "WRITE Flash ESP8266 - można sprawdzić port 'dmesg *tty'" --menu "Wybierz port:" 16 58 7 "${value2[@]}" 3>&1 1>&2 2>&3)
	exitstatus=$?
	if [ $exitstatus = 0 ]; then

	PORT_USB="/dev/"$DISTROS
	echo "Wybrano port:" $PORT_USB
	#esptool.py --port $PORT_USB --baud 115200 erase_flash
	#esptool.py --port $PORT_USB --baud 115200 write_flash 0x00000 $FIRMWARE_PATH/"$BOARD_NAME"_"$FLASH_SIZE"_eagle.flash.bin 0x40000 $FIRMWARE_PATH/"$BOARD_NAME"_"$FLASH_SIZE"_eagle.irom0text.bin -fs 32m   

	miniterm.py $PORT_USB 74880
#	./$0

	else
		echo "You chose Cancel."
	fi
#miniterm.py /dev/ttyUSB2 74880
# sprawdzanie dmesg *tty
# dmesg | grep " FTDI USB Serial Device converter now attached to"

