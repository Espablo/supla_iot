#!/bin/bash
DISTROS=$(whiptail --title "Erase Flash ESP8266" --radiolist \
"Wybierz port" 15 60 4 \
"/dev/ttyUSB0" "" ON \
"/dev/ttyUSB1" "" OFF \
"/dev/ttyUSB2" "" OFF \
"/dev/ttyUSB3" "" OFF \
"/dev/ttyUSB4" "" OFF 3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "The chosen distro is:" $DISTROS
esptool.py --port $DISTROS --baud 115200 erase_flash
./$0
else
    echo "You chose Cancel."
fi

# sprawdzanie dmesg *tty
# dmesg | grep " FTDI USB Serial Device converter now attached to"

