# Gotowe Espressif #
```sh
https://drive.google.com/open?id=0B0MLg-Vt773mQjV1VGNwa1ZkUWM
```
# Instalacja Espressif #

### 64-bit Ubuntu (Linux) ###

```sh
 sudo apt-get install git autoconf build-essential gperf bison flex texinfo libtool libncurses5-dev wget gawk libc6-dev-amd64 python-serial libexpat-dev automake
 
 sudo apt-get install libtool-bin

 sudo mkdir /opt/Espressif

 sudo chown espablo /opt/Espressif/
```
### Instalacja Xtensa crosstool-NG (jako local user) ###

```sh
 cd /opt/Espressif
  
 git clone -b lx106 git://github.com/jcmvbkbc/crosstool-NG.git 
  
 cd crosstool-NG
  
 ./bootstrap && ./configure --prefix=`pwd` && make && make install

 ./ct-ng xtensa-lx106-elf

 ./ct-ng build

 wget -O xtensa-lx106-elf.tar.gz "https://drive.google.com/uc?export=download&id=0B0MLg-Vt773mQjlMcVBRcXFBSGs"

 unzip xtensa-lx106-elf.zip
```
### Dopisujemy w plikach ###
```sh
echo 'PATH=/opt/Espressif/crosstool-NG/builds/xtensa-lx106-elf/bin:$PATH' >> ~/.bashrc
lub
echo 'PATH=/opt/Espressif/crosstool-NG/builds/xtensa-lx106-elf/bin:$PATH' >> ~/.profile
```
### Setting up the Espressif SDK ###
```sh
 cd /opt/Espressif
 
 wget -O esp_iot_sdk_v1.2.0_15_07_03.zip https://github.com/esp8266/esp8266-wiki/raw/master/sdk/esp_iot_sdk_v1.2.0_15_07_03.zip
 
 unzip esp_iot_sdk_v1.2.0_15_07_03.zip
 
 mv esp_iot_sdk_v1.2.0 ESP8266_SDK
 
 mv License ESP8266_SDK/
```
### Patching ###
```sh
cd /opt/Espressif/ESP8266_SDK
sed -i -e 's/xt-ar/xtensa-lx106-elf-ar/' -e 's/xt-xcc/xtensa-lx106-elf-gcc/' -e 's/xt-objcopy/xtensa-lx106-elf-objcopy/' Makefile
mv examples/IoT_Demo .
```
### Installing Xtensa libraries and headers ###
```sh
cd /opt/Espressif/ESP8266_SDK
wget -O lib/libc.a https://github.com/esp8266/esp8266-wiki/raw/master/libs/libc.a
wget -O lib/libhal.a https://github.com/esp8266/esp8266-wiki/raw/master/libs/libhal.a
wget -O include.tgz https://github.com/esp8266/esp8266-wiki/raw/master/include.tgz
tar -xvzf include.tgz
rm include.tgz
```
# Installing the ESP image tool
### ESP8266 NONOS SDK V1.5.4 ###
```sh
cd /opt/Espressif

wget --content-disposition "http://bbs.espressif.com/download/file.php?id=1469"
 
unzip ESP8266_NONOS_SDK_V1.5.4_16_05_20.zip
 
mv ESP8266_NONOS_SDK ESP8266_NONOS_SDK154

wget -O esp8266_nonos_sdk_v1.5.4.1_patch_20160704.zip https://espressif.com/sites/default/files/sdks/esp8266_nonos_sdk_v1.5.4.1_patch_20160704.zip
 
unzip esp8266_nonos_sdk_v1.5.4.1_patch_20160704.zip -d /opt/Espressif/ESP8266_NONOS_SDK154/lib

mkdir /opt/Espressif/ESP8266_BIN154

mkdir /opt/Espressif/ESP8266_BIN154/bin
 
sed -e 's/\..\/bin/$(BIN_PATH)\/bin/g' -i /opt/Espressif/ESP8266_NONOS_SDK154/Makefile 
 
sed -e 's/\..\/tools/$(SDK_PATH)\/tools/g' -i /opt/Espressif/ESP8266_NONOS_SDK154/Makefile
```

### Instalacja oprogramowania do flashowania ###
```sh
 cd /opt/Espressif
 
 git clone https://github.com/themadinventor/esptool esptool-py
 
 chmod +w /opt/Espressif/crosstool-NG/builds/xtensa-lx106-elf/bin
 
 ln -s /opt/Espressif/esptool-py/esptool.py crosstool-NG/builds/xtensa-lx106-elf/bin/

 sudo apt install python-pip
```
### Moje repozytorium ###
```sh
 mkdir /home/espablo/SUPLA
 
 mkdir /home/espablo/SUPLA/Firmware
 
 cd /home/espablo/SUPLA
 
 git clone https://Espablo@bitbucket.org/Espablo/SUPLA_IOT.git
```
### Dodanie użytkownika do grupy ###
```sh
sudo usermod -aG dialout espablo
```
### Czyszczenie modułu ###
```sh
 esptool.py --port /dev/ttyUSB0 --baud 115200 erase_flash
```

### Wgrywanie softu do ESP-12 ###
```sh
 esptool.py --port /dev/ttyUSB0 write_flash -fm qio -fs 32m -ff 40m 0x00000 inCan_4096_eagle.flash.bin 0x40000 inCan_4096_eagle.irom0text.bin
```

