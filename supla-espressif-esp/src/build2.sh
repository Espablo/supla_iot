#!/bin/bash

###
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# @author Przemyslaw Zygmunt przemek@supla.org
#
###

DEP_LIBS="-lssl"
NOSSL=0
SPI_MODE="DIO"
FLASH_MODE="dio"
FLASH_SIZE=0
FOTA=3

export SDK_PATH=/opt/Espressif/ESP8266_NONOS_SDK154
export BIN_PATH=/opt/Espressif/ESP8266_BIN154
export FIRMWARE_PATH=/home/$USER/SUPLA/Firmware
LD_DIR=sdk154

export PATH=/opt/Espressif/esptool-py/:$PATH
export COMPILE=gcc

RSA_KEY_PATH=/home/$USER/SUPLA/SUPLA_IOT/supla-esp-signtool/RSA_KEY
RSA_KEY_EXTENSION=_key
SIGTOOL_PATH=/home/$USER/SUPLA/SUPLA_IOT/supla-esp-signtool/

function jumpto
{
  label=$1
  cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
  eval "$cmd"
  exit
}


value=(wifisocket "" \
    wifisocket_oled "" \
    wifisocket_x4 "" \
    wifisocket_54 "" \
    gate_module "" \
    gate_module_dht11 "" \
    gate_module_dht22 "" \
    gate_module_wroom "" \
    gate_module2_wroom "FOTA" \
    rs_module "FOTA" \
    starter1_module_wroom "" \
    lightswitch_x2 "" \
    lightswitch_x2_54 "" \
    lightswitch_x2_DHT11 "" \
    lightswitch_x2_54_DHT11 "" \
    lightswitch_x2_DHT22 "" \
    lightswitch_x2_54_DHT22 "" \
    sonoff "FOTA" \
    sonoff_socket "FOTA" \
    sonoff_touch "" \
    sonoff_touch_dual "FOTA" \
    sonoff_dual "FOTA" \
    sonoff_th16 "FOTA" \
    sonoff_th10 "FOTA" \
    sonoff_ds18b20 "FOTA" \
    EgyIOT "" \
    dimmer "" \
    zam_row_01 "FOTA" \
    zam_sbp_01 "FOTA" \
    ngm "FOTA" \
    rgbw_wroom "" \
  h801 "" )



OPTION=$(whiptail --title "SUPLA" --menu "Wybierz firmware (defaultowi jest DIO):" 16 58 7 "${value[@]}" 3>&1 1>&2 2>&3)
exitstatus=$?
if [ $exitstatus = 0 ]; then

  case $OPTION in
    "wifisocket")
      ;;
    "wifisocket_oled")
      ;;
    "wifisocket_x4")
      ;;
    "wifisocket_54")
      ;;
    "gate_module")
      ;;
    "gate_module_dht11")
      ;;
    "gate_module_dht22")
      ;;
    "gate_module_wroom")
      ;;
    "gate_module2_wroom")
      FOTA=1
      ;;
    "rs_module")
      FOTA=1
      FLASH_SIZE="2048"
      ;;
    "starter1_module_wroom")
      ;;
    "lightswitch_x2")
      FLASH_SIZE="4096"
      ;;
    "lightswitch_x2_54")
      FLASH_SIZE="4096"
      ;;
    "lightswitch_x2_DHT11")
      FLASH_SIZE="4096"
      ;;
    "lightswitch_x2_54_DHT11")
      FLASH_SIZE="4096"
      ;;
    "lightswitch_x2_DHT22")
      FLASH_SIZE="4096"
      ;;
    "lightswitch_x2_54_DHT22")
      FLASH_SIZE="4096"
      ;;
    "sonoff")
      ;;
    "sonoff_socket")
      ;;
    "sonoff_touch")
      SPI_MODE="DOUT"
      ;;
    "sonoff_touch_dual")
      SPI_MODE="DOUT"
      ;;
    "sonoff_dual")
      ;;
    "sonoff_th16")
      ;;
    "sonoff_th10")
      ;;
    "sonoff_ds18b20")
      ;;
    "EgyIOT")
      DEP_LIBS="-lpwm"
      NOSSL=1
      ;;
    "dimmer")
      DEP_LIBS="-lpwm"
      NOSSL=1
      ;;
    "zam_row_01")
      FLASH_SIZE="2048"
      FOTA=1
      ;;
    "zam_sbp_01")
      FLASH_SIZE="2048"
      FOTA=1
      ;;
    "ngm")
      FLASH_SIZE="2048"
      FOTA=1
      ;;
    "rgbw_wroom")
      DEP_LIBS="-lpwm -lssl"
      ;;
    "h801")
      DEP_LIBS="-lpwm -lssl"
      ;;

    *)
  esac

  if [[ $SPI_MODE = QIO ]]; then
    FLASH_MODE="qio"
  elif [[ $SPI_MODE = DIO ]]; then
    FLASH_MODE="dio"
  elif [[ $SPI_MODE = QOUT ]]; then
    FLASH_MODE="qout"
  elif [[ $SPI_MODE = DOUT ]]; then
    FLASH_MODE="dout"
  fi

  BOARD_NAME=$OPTION
  if [[ FOTA -eq 3 ]]; then
    if (whiptail --title "SUPLA" --yesno "Firmware "$BOARD_NAME" ma być FOTA?" 8 78) then
      FOTA=1
    else
      FOTA=0
    fi
  fi

  value_flash_size=(512 "flash size is not supported" \
      1024 "" \
      2048 "" \
    4096 "")

  if [[ FLASH_SIZE -eq 0 ]]; then
    DISTROS_FLASH_SIZE=$(whiptail --title "FLASH SIZE ESP8266" --menu "Wybierz wielkość pamięci:" 16 58 7 "${value_flash_size[@]}" 3>&1 1>&2 2>&3)
    exitstatus=$?
  else
    DISTROS_FLASH_SIZE=$FLASH_SIZE
  fi

  # DISTROS_FLASH_SIZE=$(whiptail --title "FLASH SIZE ESP8266" --menu "Wybierz wielkość pamięci:" 16 58 7 "${value_flash_size[@]}" 3>&1 1>&2 2>&3)
  # exitstatus=$?
  if [ $exitstatus = 0 ]; then
    if [ "$FOTA" -eq 1 ]; then
      case $DISTROS_FLASH_SIZE in
        "512")
          exit 0
          ;;
        "1024")
          FLASH_SIZE="1024"
          CFG_SECTOR=0x7C
          ;;
        "2048")
          FLASH_SIZE="2048"
          SPI_SIZE_MAP=5
          CFG_SECTOR=0xFC
          ;;
        "4096")
          FLASH_SIZE="4096"
          SPI_SIZE_MAP=6
          CFG_SECTOR=0xFC
          ;;
      esac
    else
      CFG_SECTOR=0x3C
      case $DISTROS_FLASH_SIZE in
        "512")
          exit 0
          ;;
        "1024")
          FLASH_SIZE="1024"
          SPI_SIZE_MAP=2
          ;;
        "2048")
          FLASH_SIZE="2048"
          SPI_SIZE_MAP=3
          ;;
        "4096")
          FLASH_SIZE="4096"
          SPI_SIZE_MAP=4
          ;;

        *)
      esac
    fi
  else
    jumpto END
  fi

  make clean
  FIRMWARE_NAME="$BOARD_NAME"_"$FLASH_SIZE"

  if [ "$NOSSL" -eq 1 ]; then
    EXTRA_CCFLAGS="${EXTRA_CCFLAGS} -DNOSSL=1"
    BOARD_NAME="$OPTION"_nossl
  else
    EXTRA_CCFLAGS="${EXTRA_CCFLAGS} -DNOSSL=0"
  fi

  rm -f $FIRMWARE_PATH/$BOARD_NAME/*.bin
  mkdir $FIRMWARE_PATH/$BOARD_NAME

  if [ "$FOTA" -eq 1 ]; then
    APP=1
    make SUPLA_DEP_LIBS="$DEP_LIBS" FOTA="$FOTA" BOARD=$OPTION CFG_SECTOR="$CFG_SECTOR" BOOT=new APP="$APP" SPI_SPEED=40 SPI_MODE="$SPI_MODE" SPI_SIZE_MAP="$SPI_SIZE_MAP" __EXTRA_CCFLAGS="$EXTRA_CCFLAGS" && \
      cp $BIN_PATH/bin/upgrade/user"$APP"."$FLASH_SIZE".new."$SPI_SIZE_MAP".bin $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_user"$APP"."$FLASH_SIZE".new."$SPI_SIZE_MAP"_"$FLASH_MODE".bin
    if [ -e $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_user"$APP"."$FLASH_SIZE".new."$SPI_SIZE_MAP"_"$FLASH_MODE".bin ]; then
      if (whiptail --title "SUPLA" --yesno "Utworzyć "$BOARD_NAME"_user2?" 8 78) then
        APP=2
        make clean
        make SUPLA_DEP_LIBS="$DEP_LIBS" FOTA="$FOTA" BOARD=$OPTION CFG_SECTOR="$CFG_SECTOR" BOOT=new APP="$APP" SPI_SPEED=40 SPI_MODE="$SPI_MODE" SPI_SIZE_MAP="$SPI_SIZE_MAP" __EXTRA_CCFLAGS="$EXTRA_CCFLAGS" && \
          cp $BIN_PATH/bin/upgrade/user"$APP"."$FLASH_SIZE".new."$SPI_SIZE_MAP".bin $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_user"$APP"."$FLASH_SIZE".new."$SPI_SIZE_MAP"_"$FLASH_MODE".bin
      fi
    else
      jumpto ERROR
    fi
    cp $SDK_PATH/bin/boot_v1.5.bin $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_"$FLASH_MODE"_boot_v1.5.bin
    FIRMWARE_NAME="$BOARD_NAME"_"$FLASH_SIZE"_FOTA
    if (whiptail --title "SUPLA" --yesno "Firmware "$FIRMWARE_NAME" utworzony. Podpisać cyfrowo pliki?" 8 78) then
      $SIGTOOL_PATH/supla-esp-sigtool -k $RSA_KEY_PATH/"$BOARD_NAME$RSA_KEY_EXTENSION"/"$BOARD_NAME$RSA_KEY_EXTENSION" -s $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_user1."$FLASH_SIZE".new."$SPI_SIZE_MAP"_"$FLASH_MODE".bin
      $SIGTOOL_PATH/supla-esp-sigtool -k $RSA_KEY_PATH/"$BOARD_NAME$RSA_KEY_EXTENSION"/"$BOARD_NAME$RSA_KEY_EXTENSION" -s $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_user2."$FLASH_SIZE".new."$SPI_SIZE_MAP"_"$FLASH_MODE".bin
    fi
  else
    cp ./ld/"$LD_DIR"/"$FLASH_SIZE"_eagle.app.v6.ld $SDK_PATH/ld/eagle.app.v6.ld || exit 1

    make SUPLA_DEP_LIBS="$DEP_LIBS" BOARD=$OPTION CFG_SECTOR=$CFG_SECTOR BOOT=new APP=0 SPI_SPEED=40 SPI_MODE="$SPI_MODE" SPI_SIZE_MAP="$SPI_SIZE_MAP" __EXTRA_CCFLAGS="$EXTRA_CCFLAGS" && \
      cp $BIN_PATH/bin/eagle.flash.bin $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_"$FLASH_MODE"_eagle.flash.bin && \
      cp $BIN_PATH/bin/eagle.irom0text.bin $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_"$FLASH_MODE"_eagle.irom0text.bin
    if [ -e $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_"$FLASH_MODE"_eagle.flash.bin  ] || [ -e $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_"$FLASH_MODE"_eagle.irom0text.bin ]; then
      echo "Utworzono "$FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_"$FLASH_MODE"
    else
      jumpto ERROR
    fi
  fi
fi
if (whiptail --title "SUPLA" --yesno "Firmware "$FIRMWARE_NAME" utworzony. Chcesz wgrać program?" 8 78) then
  jumpto WRITE
else
  jumpto END
fi

ERROR:
if (whiptail --title "SUPLA" --yesno "Firmware "$FIRMWARE_NAME" ERROR!!!. Chcesz jeszcze raz?" 8 78) then
  jumpto START
else
  jumpto END
fi


WRITE:
CHECK_USB="check_usb"
echo "" > $CHECK_USB
for sysdevpath in $(find /sys/bus/usb/devices/usb*/ -name dev); do
  (
    syspath="${sysdevpath%/dev}"
    devname="$(udevadm info -q name -p $syspath)"
    [[ "$devname" == "bus/"* ]] && continue
    eval "$(udevadm info -q property --export -p $syspath)"
    [[ -z "$ID_SERIAL" ]] && continue

    serial="$(udevadm info -q name -p $syspath)"
    [[ "$serial" != "ttyUSB"* ]] && continue
    echo "$devname  \"$ID_SERIAL\"" >> $CHECK_USB
  )
done
sort $CHECK_USB -o $CHECK_USB
I=0
while read; do
  I=$(($I + 1))
  value2+=($REPLY)
done < "$CHECK_USB"
rm $CHECK_USB

case $FLASH_SIZE in
  "1024")
    F_SIZE=1MB
    ;;
  "2048")
    F_SIZE=2MB-c1
    ;;
  "4096")
    F_SIZE=4MB-c1
    ;;
esac
DISTROS=$(whiptail --title "WRITE Flash ESP8266" --menu "Wybierz port:" 16 58 7 "${value2[@]}" 3>&1 1>&2 2>&3)
exitstatus=$?
if [ $exitstatus = 0 ]; then
  PORT_USB="/dev/"$DISTROS
  esptool.py --port $PORT_USB --baud 115200 erase_flash
  if [ "$FOTA" == 1 ]; then
    # esptool.py --port $PORT_USB --baud 115200 write_flash --flash_mode $FLASH_MODE --flash_freq 40m --flash_size 32m 0x00000 $FIRMWARE_PATH/$BOARD_NAME/boot_v1.5.bin 0x01000 $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_user"$APP"."$FLASH_SIZE".new."$SPI_SIZE_MAP".bin
    esptool.py --port $PORT_USB --baud 115200 write_flash --flash_mode $FLASH_MODE --flash_freq 40m --flash_size $F_SIZE 0x00000 $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_"$FLASH_MODE"_boot_v1.5.bin 0x01000 $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_user"$APP"."$FLASH_SIZE".new."$SPI_SIZE_MAP"_"$FLASH_MODE".bin
  else
    # esptool.py --port $PORT_USB --baud 115200 write_flash --flash_mode $FLASH_MODE --flash_freq 40m --flash_size 4MB 0x00000 $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_eagle.flash.bin 0x40000 $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_eagle.irom0text.bin
    echo "esptool.py -p "$PORT_USB" -b 115200 write_flash -fm "$FLASH_MODE" -ff 40m -fs "$F_SIZE" 0x00000 "$FIRMWARE_PATH"/"$BOARD_NAME"/"$BOARD_NAME"_"$FLASH_SIZE"_"$FLASH_MODE"_eagle.flash.bin 0x40000 "$FIRMWARE_PATH"/"$BOARD_NAME"/"$BOARD_NAME"_"$FLASH_SIZE"_"$FLASH_MODE"_eagle.irom0text.bin"
    esptool.py -p $PORT_USB -b 115200 write_flash -fm $FLASH_MODE -ff 40m -fs $F_SIZE 0x00000 $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_"$FLASH_MODE"_eagle.flash.bin 0x40000 $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_"$FLASH_MODE"_eagle.irom0text.bin
  fi
  miniterm $PORT_USB 74880
  jumpto START
else
  jumpto END
fi


END:
echo "Wybrano KONIEC!"
exit 0


START:
$0
