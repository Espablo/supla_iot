#!/bin/bash

###
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# @author Przemyslaw Zygmunt przemek@supla.org
#
###

DEP_LIBS="-lssl"
NOSSL=0
SPI_MODE="DIO"
FLASH_MODE="dio"

# export PATH=/opt/Espressif/crosstool-NG/builds/xtensa-lx106-elf/bin/:$PATH

# export PATH=/opt/Espressif/xtensa-lx106-elf/bin:$PATH
export PATH=/opt/Espressif/esptool-py/:$PATH
export COMPILE=gcc

function jumpto
{
  label=$1
  cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
  eval "$cmd"
  exit
}

value=(multiboard "" \
    inCan_test "" \
    inCan_F "FOTA" \
    inCan_F2 "FOTA2" \
    inCan "" \
    inCan_Easy "" \
    espablo "" \
    espablo_Easy "" \
    wifisocket "" \
    wifisocket_oled "" \
    wifisocket_x4 "" \
    wifisocket_54 "" \
    gate_module "" \
    gate_module_dht11 "" \
    gate_module_dht22 "" \
    gate_module_wroom "" \
    gate_module2_wroom "FOTA" \
    rs_module "FOTA" \
    starter1_module_wroom "" \
    lightswitch_x2 "" \
    lightswitch_x2_54 "" \
    lightswitch_x2_DHT11 "" \
    lightswitch_x2_54_DHT11 "" \
    lightswitch_x2_DHT22 "" \
    lightswitch_x2_54_DHT22 "" \
    sonoff "FOTA" \
    sonoff_socket "FOTA" \
    sonoff_touch "" \
    sonoff_touch_dual "FOTA" \
    sonoff_dual "FOTA" \
    sonoff_th16 "FOTA" \
    sonoff_th10 "FOTA" \
    sonoff_ds18b20 "FOTA" \
    EgyIOT "" \
    dimmer "" \
    zam_row_01 "FOTA" \
    zam_sbp_01 "FOTA" \
    ngm "FOTA" \
    rgbw_wroom "" \
  h801 "" )

OPTION=$(whiptail --title "SUPLA" --menu "Choose board firmware:" 16 58 7 "${value[@]}" 3>&1 1>&2 2>&3)

exitstatus=$?
if [ $exitstatus = 0 ]; then

  case $OPTION in
    "multiboard")
      ;;
    "inCan_test")
      FLASH_SIZE="4096"
      ;;
    "inCan_F")
      SPI_MODE="QIO"
      FLASH_MODE="qio"
      FLASH_SIZE="4096"
      FOTA=1
      OPTION="inCan"
      ;;
    "inCan_F2")
      SPI_MODE="QIO"
      FLASH_MODE="qio"
      FLASH_SIZE="4096"
      FOTA=1
      OPTION="inCan"
      USER_2=2
      ;;
    "inCan")
      SPI_MODE="QIO"
      FLASH_MODE="qio"
      FLASH_SIZE="4096"
      ;;
    "inCan_Easy")
      SPI_MODE="DOUT"
      FLASH_MODE="dout"
      # FLASH_SIZE="4096"
      ;;
    "espablo")
      SPI_MODE="QIO"
      FLASH_MODE="qio"
      FLASH_SIZE="4096"
      ;;
    "espablo_Easy")
      #    SPI_MODE="DOUT"
      #    FLASH_MODE="dout"
      FLASH_SIZE="4096"
      ;;

    "wifisocket")
      ;;
    "wifisocket_oled")
      ;;
    "wifisocket_x4")
      ;;
    "wifisocket_54")
      ;;
    "gate_module")
      ;;
    "gate_module_dht11")
      ;;
    "gate_module_dht22")
      ;;
    "gate_module_wroom")
      ;;
    "gate_module2_wroom")
      FOTA=1
      ;;
    "rs_module")
      # FOTA=1
      FLASH_SIZE="2048"
      ;;
    "starter1_module_wroom")
      ;;
    "lightswitch_x2")
      FLASH_SIZE="4096"
      ;;
    "lightswitch_x2_54")
      FLASH_SIZE="4096"
      ;;
    "lightswitch_x2_DHT11")
      FLASH_SIZE="4096"
      ;;
    "lightswitch_x2_54_DHT11")
      FLASH_SIZE="4096"
      ;;
    "lightswitch_x2_DHT22")
      FLASH_SIZE="4096"
      ;;
    "lightswitch_x2_54_DHT22")
      FLASH_SIZE="4096"
      ;;
    "sonoff")
      FOTA=1
      ;;
    "sonoff_socket")
      FOTA=1
      ;;
    "sonoff_touch")
      SPI_MODE="DOUT"
      ;;
    "sonoff_touch_dual")
      SPI_MODE="DOUT"
      FOTA=1
      ;;
    "sonoff_dual")
      FOTA=1
      ;;
    "sonoff_th16")
      FOTA=1
      ;;
    "sonoff_th10")
      FOTA=1
      ;;
    "sonoff_ds18b20")
      FOTA=1
      ;;
    "EgyIOT")
      DEP_LIBS="-lpwm"
      NOSSL=1
      ;;
    "dimmer")
      DEP_LIBS="-lpwm"
      NOSSL=1
      ;;
    "zam_row_01")
      FLASH_SIZE="2048"
      FOTA=1
      ;;
    "zam_sbp_01")
      FLASH_SIZE="2048"
      FOTA=1
      ;;
    "ngm")
      FLASH_SIZE="2048"
      FOTA=1
      ;;
    "rgbw_wroom")
      DEP_LIBS="-lpwm -lssl"
      ;;
    "h801")
      DEP_LIBS="-lpwm -lssl"
      ;;

    *)
  esac

  CFG_SECTOR=0x3C

  case $FLASH_SIZE in
    "512")
      "512 flash size is not supported"
      exit 0
      ;;
    "2048")
      SPI_SIZE_MAP=3
      ;;
    "4096")
      SPI_SIZE_MAP=4
      ;;
    *)
      FLASH_SIZE="1024"
      SPI_SIZE_MAP=2
      ;;
  esac


  export SDK_PATH=/opt/Espressif/ESP8266_NONOS_SDK154
  export BIN_PATH=/opt/Espressif/ESP8266_BIN154
  export FIRMWARE_PATH=/home/$USER/SUPLA/Firmware
  LD_DIR=sdk154

  make clean

  BOARD_NAME=$OPTION

  if [ "$NOSSL" -eq 1 ]; then
    EXTRA_CCFLAGS="${EXTRA_CCFLAGS} -DNOSSL=1"
    BOARD_NAME="$OPTION"_nossl
  else
    EXTRA_CCFLAGS="${EXTRA_CCFLAGS} -DNOSSL=0"
  fi

  # rm -f $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_eagle.flash.bin
  # rm -f $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_eagle.irom0text.bin
  # rm -f $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_user"$APP"."$FLASH_SIZE".new."$SPI_SIZE_MAP".bin
  # rm -f $FIRMWARE_PATH/$BOARD_NAME/boot_v1.5.bin
  rm -f $FIRMWARE_PATH/$BOARD_NAME/*.bin
  mkdir $FIRMWARE_PATH/$BOARD_NAME


  if [ "$FOTA" -eq 1 ]; then

    APP=1

    if [ "user2" = "$2" ]; then
      APP=2
    fi

    if [ "$USER_2" -eq 2 ]; then
      APP=2
    fi

    case $FLASH_SIZE in
      "1024")
        CFG_SECTOR=0x7C
        ;;
      "2048")
        SPI_SIZE_MAP=5
        CFG_SECTOR=0xFC
        ;;
      "4096")
        SPI_SIZE_MAP=6
        CFG_SECTOR=0xFC
        ;;
    esac

    make SUPLA_DEP_LIBS="$DEP_LIBS" FOTA="$FOTA" BOARD=$OPTION CFG_SECTOR="$CFG_SECTOR" BOOT=new APP="$APP" SPI_SPEED=40 SPI_MODE="$SPI_MODE" SPI_SIZE_MAP="$SPI_SIZE_MAP" __EXTRA_CCFLAGS="$EXTRA_CCFLAGS" && \
      cp $BIN_PATH/bin/upgrade/user"$APP"."$FLASH_SIZE".new."$SPI_SIZE_MAP".bin $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_user"$APP"."$FLASH_SIZE".new."$SPI_SIZE_MAP".bin && \
      cp $SDK_PATH/bin/boot_v1.5.bin $FIRMWARE_PATH/$BOARD_NAME/boot_v1.5.bin
  else
    cp ./ld/"$LD_DIR"/"$FLASH_SIZE"_eagle.app.v6.ld $SDK_PATH/ld/eagle.app.v6.ld || exit 1

    make SUPLA_DEP_LIBS="$DEP_LIBS" BOARD=$OPTION CFG_SECTOR=$CFG_SECTOR BOOT=new APP=0 SPI_SPEED=40 SPI_MODE="$SPI_MODE" SPI_SIZE_MAP="$SPI_SIZE_MAP" __EXTRA_CCFLAGS="$EXTRA_CCFLAGS" && \
      cp $BIN_PATH/bin/eagle.flash.bin $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_eagle.flash.bin && \
      cp $BIN_PATH/bin/eagle.irom0text.bin $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_eagle.irom0text.bin
  fi

  if [ -e $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_eagle.flash.bin  ] || [ -e $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_user"$APP"."$FLASH_SIZE".new."$SPI_SIZE_MAP".bin ]; then

    if [ "$FOTA" == 1 ]; then
      FIRMWARE_NAME="$BOARD_NAME"_"$FLASH_SIZE"_FOTA
      if (whiptail --title "SUPLA" --yesno "Firmware "$FIRMWARE_NAME" utworzony. Podpisać cyfrowo plik?" 8 78) then
        ~/SUPLA/SUPLA_IOT/supla-esp-signtool/supla-esp-sigtool -k ~/SUPLA/SUPLA_IOT/supla-esp-signtool/RSA_KEY/inCan_key/inCan_key -s $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_user"$APP"."$FLASH_SIZE".new."$SPI_SIZE_MAP".bin
        jumpto WRITE
      else
        # jumpto END
        jumpto WRITE
      fi
    else
      FIRMWARE_NAME="$BOARD_NAME"_"$FLASH_SIZE"
      jumpto WRITE
    fi

    WRITE:
    if (whiptail --title "SUPLA" --yesno "Firmware "$FIRMWARE_NAME" utworzony. Chcesz wgrać program?" 8 78) then

      CHECK_USB="check_usb"
      echo "" > $CHECK_USB
      for sysdevpath in $(find /sys/bus/usb/devices/usb*/ -name dev); do
        (

          syspath="${sysdevpath%/dev}"
          devname="$(udevadm info -q name -p $syspath)"
          [[ "$devname" == "bus/"* ]] && continue
          eval "$(udevadm info -q property --export -p $syspath)"
          [[ -z "$ID_SERIAL" ]] && continue

          serial="$(udevadm info -q name -p $syspath)"
          [[ "$serial" != "ttyUSB"* ]] && continue
          echo "$devname  \"$ID_SERIAL\"" >> $CHECK_USB

        )
      done
      sort $CHECK_USB -o $CHECK_USB
      I=0
      while read; do
        I=$(($I + 1))
        value2+=($REPLY)
      done < "$CHECK_USB"
      rm $CHECK_USB

      DISTROS=$(whiptail --title "WRITE Flash ESP8266 - można sprawdzić port 'dmesg *tty'" --menu "Wybierz port:" 16 58 7 "${value2[@]}" 3>&1 1>&2 2>&3)
      exitstatus=$?

      if [ $exitstatus = 0 ]; then

        PORT_USB="/dev/"$DISTROS
        echo "Wybrano port:" $PORT_USB
        esptool.py --port $PORT_USB --baud 115200 erase_flash
        # echo "Wgrywamy!"
        if [ "$FOTA" -eq 1 ]; then
          # esptool.py --port $PORT_USB --baud 115200 write_flash --flash_mode $FLASH_MODE --flash_freq 40m --flash_size 32m 0x00000 $FIRMWARE_PATH/$BOARD_NAME/boot_v1.5.bin 0x01000 $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_user"$APP"."$FLASH_SIZE".new."$SPI_SIZE_MAP".bin
          esptool.py --port $PORT_USB --baud 115200 write_flash --flash_mode $FLASH_MODE --flash_freq 40m --flash_size 4MB-c1 0x00000 $FIRMWARE_PATH/$BOARD_NAME/boot_v1.5.bin 0x01000 $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_user"$APP"."$FLASH_SIZE".new."$SPI_SIZE_MAP".bin
        else
          # esptool.py --port $PORT_USB --baud 115200 write_flash --flash_mode $FLASH_MODE --flash_freq 40m --flash_size 4MB 0x00000 $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_eagle.flash.bin 0x40000 $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_eagle.irom0text.bin
          esptool.py -p $PORT_USB -b 115200 write_flash -fm $FLASH_MODE -ff 40m -fs 4MB 0x00000 $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_eagle.flash.bin 0x40000 $FIRMWARE_PATH/$BOARD_NAME/"$BOARD_NAME"_"$FLASH_SIZE"_eagle.irom0text.bin
        fi

        miniterm $PORT_USB 74880
        $0

      else
        jumpto END
      fi
    else
      jumpto END
    fi

  else

    if (whiptail --title "SUPLA" --yesno "Firmware SUPLA_"$BOARD_NAME"_"$FLASH_SIZE" ERROR!!!. Chcesz jeszcze raz?" 8 78) then

      ./$0
    else
      jumpto END
    fi
  fi
else
  jumpto END
fi
END:
echo "Wybrano KONIEC!"
exit 1
