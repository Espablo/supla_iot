/*
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "supla_esp.h"
#include "supla_dht.h"
#include "supla_ds18b20.h"

void supla_esp_board_set_device_name(char *buffer, uint8 buffer_size) {

 #if defined(__BOARD_lightswitch_x2) || defined(__BOARD_lightswitch_x2_54)

	ets_snprintf(buffer, buffer_size, "SUPLA-LIGHTSWITCH-x2-DS");

  #elif defined(__BOARD_lightswitch_x2_DHT11) || defined(__BOARD_lightswitch_x2_54_DHT11)

	ets_snprintf(buffer, buffer_size, "SUPLA-LIGHTSWITCH-x2-DHT11");

  #elif defined(__BOARD_lightswitch_x2_DHT22) || defined(__BOARD_lightswitch_x2_54_DHT22)

	ets_snprintf(buffer, buffer_size, "SUPLA-LIGHTSWITCH-x2-DHT22");

 #endif

}
void ICACHE_FLASH_ATTR supla_esp_board_set_channels_gpio(TDS_SuplaDeviceChannel_B *channel_set, uint8 channels_gpio) {
	switch_chanel_gpio = channels_gpio;

	Setting_Up_Button_Relay(B_CFG_PORT, \
	                  B_RELAY1_PORT, \
	                  supla_esp_cfg.CfgButtonType == BTN_TYPE_BISTABLE ? INPUT_TYPE_BTN_BISTABLE : INPUT_TYPE_BTN_MONOSTABLE, \
	                  INPUT_FLAG_PULLUP, \
	                  RELAY_FLAG_RESTORE_FORCE, \
	                  channel_set);

	Setting_Up_Button_Relay(B_BTN2_PORT, \
	                  B_RELAY2_PORT, \
	                  supla_esp_cfg.CfgButtonType == BTN_TYPE_BISTABLE ? INPUT_TYPE_BTN_BISTABLE : INPUT_TYPE_BTN_MONOSTABLE, \
	                  INPUT_FLAG_PULLUP, \
	                  RELAY_FLAG_RESTORE_FORCE, \
	                  channel_set);
#if defined(DS18B20)
	Setting_Up_Temperature(2, SUPLA_CHANNELTYPE_THERMOMETERDS18B20, channel_set);
#elif defined(SENSOR_DHT11)
	Setting_Up_Temperature(2, SUPLA_CHANNELTYPE_DHT11, channel_set);
#elif defined(SENSOR_DHT22)
	Setting_Up_Temperature(2, SUPLA_CHANNELTYPE_DHT22, channel_set);
  #endif
// ---------------------------------------
	Setting_Up_Button_Relay_Configure(B_CFG_PORT, supla_esp_cfg.CfgButtonType == BTN_TYPE_BISTABLE ? INPUT_TYPE_BTN_BISTABLE : INPUT_TYPE_BTN_MONOSTABLE);

}

void supla_esp_board_send_channel_values_with_delay(void *srpc) {

}
