/*
 Copyright (C) AC SOFTWARE SP. Z O.O.

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


// #include "supla_esp.h"
#include "supla_ds18b20.h"

// #define CHANN_RELAY_1		0
// #define CHANN_RELAY_2		1
// #define CHANN_THERMOMETER	2
// #define CHANN_SENSOR_1		3
// #define CHANN_SENSOR_2		4

#define B_WEJSCIE          13


		// char Relay1GPIO;
		// char Relay2GPIO ;
		// char Button1GPIO;
		// char Button2GPIO;
		// char Sensor1GPIO;
		// char Sensor2GPIO;
		// char ThermometerGPIO;
		// char BtnConfigGPIO;
		// char ConfigGPIO;
		// char LedConfigGPIO;


void ICACHE_FLASH_ATTR supla_esp_board_set_device_name(char *buffer, uint8 buffer_size) {
	// #if defined(INCAN_EASY_TYPE_SELECTION)
  //   ets_snprintf(buffer, buffer_size, NAME_BOARD "-EASY");
  //   #else
    if(supla_esp_cfg.RollerShutter == ROLLER_SHUTTER_OFF){
    	ets_snprintf(buffer, buffer_size, NAME_BOARD );
    }
    else {
			ets_snprintf(buffer, buffer_size, NAME_BOARD "-RollerShutter");
	}
// #endif
}


void ICACHE_FLASH_ATTR supla_esp_board_gpio_init(void) {

	#if defined(INCAN_TYPE_SELECTION) || defined(__BOARD_inCan_test)

		// Relay1GPIO = B_RELAY1_PORT;
		// Relay2GPIO = B_RELAY2_PORT;
		// Button1GPIO = B_BTN1_PORT;
		// Button2GPIO = B_BTN2_PORT;
		// Sensor1GPIO = B_SENSOR_PORT1;
		// Sensor2GPIO = B_SENSOR_PORT2;
		// BtnConfigGPIO = B_BTN_CFG_PORT;
	  // ConfigGPIO = B_CFG_PORT;
	  // ThermometerGPIO = TEMPERATURE_PORT;
		// LedConfigGPIO = LED_CONFIG_PORT;
	#elif defined(INCAN_EASY_TYPE_SELECTION)
			// LedConfigGPIO = LED_CONFIG_PORT;

			if(supla_esp_cfg.Relay1_GPIO >= 17) Relay1GPIO = supla_esp_cfg.Relay1_GPIO - 7;
	    else Relay1GPIO = supla_esp_cfg.Relay1_GPIO;

	    if(supla_esp_cfg.Relay2_GPIO >= 17) Relay2GPIO = supla_esp_cfg.Relay2_GPIO - 7;
	    else Relay2GPIO = supla_esp_cfg.Relay2_GPIO;

	    if(supla_esp_cfg.Button1_GPIO >= 17) Button1GPIO = supla_esp_cfg.Button1_GPIO -7;
	    else Button1GPIO = supla_esp_cfg.Button1_GPIO;

	    if(supla_esp_cfg.Button2_GPIO >= 17) Button2GPIO = supla_esp_cfg.Button2_GPIO -7;
	    else Button2GPIO = supla_esp_cfg.Button2_GPIO;

	    if(supla_esp_cfg.Sensor1_GPIO >= 17) Sensor1GPIO = supla_esp_cfg.Sensor1_GPIO - 7;
	    else Sensor1GPIO = supla_esp_cfg.Sensor1_GPIO;

	    if(supla_esp_cfg.Sensor2_GPIO >= 17) Sensor2GPIO = supla_esp_cfg.Sensor2_GPIO - 7;
	    else Sensor2GPIO = supla_esp_cfg.Sensor2_GPIO;

	    if(supla_esp_cfg.Thermometer_GPIO >= 17) ThermometerGPIO = supla_esp_cfg.Thermometer_GPIO - 7;
	    else ThermometerGPIO = supla_esp_cfg.Thermometer_GPIO;

	    if(supla_esp_cfg.BtnConfig_GPIO >= 17) BtnConfigGPIO = supla_esp_cfg.BtnConfig_GPIO - 7;
	    else BtnConfigGPIO = supla_esp_cfg.BtnConfig_GPIO;

	    if(supla_esp_cfg.Config_GPIO >= 17) ConfigGPIO = supla_esp_cfg.Config_GPIO - 7;
	    else ConfigGPIO = supla_esp_cfg.Config_GPIO;

	#endif

    uint8_t wejscia = 0;
    supla_input_cfg[wejscia].channel = 0;
		supla_input_cfg[wejscia].type = INPUT_TYPE_BTN_MONOSTABLE;
		supla_input_cfg[wejscia].gpio_id = B_BTN_CFG_PORT;
		supla_input_cfg[wejscia].flags = INPUT_FLAG_PULLUP | INPUT_FLAG_CFG_BTN;

    wejscia++;
    supla_input_cfg[wejscia].channel = 1;
		supla_input_cfg[wejscia].gpio_id = B_SENSOR_PORT1;
		supla_input_cfg[wejscia].type = INPUT_TYPE_SENSOR;
		supla_input_cfg[wejscia].flags = INPUT_FLAG_PULLUP;

    wejscia++;
    supla_input_cfg[wejscia].channel = 2;
		supla_input_cfg[wejscia].gpio_id = B_SENSOR_PORT2;
		supla_input_cfg[wejscia].type = INPUT_TYPE_SENSOR;
		supla_input_cfg[wejscia].flags = INPUT_FLAG_PULLUP;

    if(supla_esp_cfg.RollerShutter == ROLLER_SHUTTER_OFF){
        wejscia++;
        supla_input_cfg[wejscia].channel = 3;
	    	supla_input_cfg[wejscia].gpio_id = B_BTN1_PORT;
        supla_input_cfg[wejscia].relay_gpio_id = B_RELAY1_PORT;
        supla_input_cfg[wejscia].type = supla_esp_cfg.Button1Type == BTN_TYPE_BISTABLE ? INPUT_TYPE_BTN_BISTABLE : INPUT_TYPE_BTN_MONOSTABLE;
        supla_input_cfg[wejscia].flags = INPUT_FLAG_PULLUP;

        wejscia++;
        supla_input_cfg[wejscia].channel = 4;
	    	supla_input_cfg[wejscia].gpio_id = B_BTN2_PORT;
        supla_input_cfg[wejscia].relay_gpio_id = B_RELAY2_PORT;
        supla_input_cfg[wejscia].type = supla_esp_cfg.Button2Type == BTN_TYPE_BISTABLE ? INPUT_TYPE_BTN_BISTABLE : INPUT_TYPE_BTN_MONOSTABLE;
	    	supla_input_cfg[wejscia].flags = INPUT_FLAG_PULLUP;
	    // ---------------------------------------
        supla_relay_cfg[0].gpio_id = B_RELAY1_PORT;
        supla_relay_cfg[0].flags = RELAY_FLAG_RESTORE_FORCE;
        supla_relay_cfg[0].channel = 3;

        supla_relay_cfg[1].gpio_id = B_RELAY2_PORT;
        supla_relay_cfg[1].flags = RELAY_FLAG_RESTORE_FORCE;
        supla_relay_cfg[1].channel = 4;
    }
    else {
        wejscia++;
		    supla_input_cfg[wejscia].gpio_id = B_BTN1_PORT;
		    supla_input_cfg[wejscia].relay_gpio_id = B_RELAY1_PORT;
				supla_input_cfg[wejscia].type = supla_esp_cfg.Button1Type == BTN_TYPE_BISTABLE ? INPUT_TYPE_BTN_BISTABLE_RS : INPUT_TYPE_BTN_MONOSTABLE_RS;
        supla_input_cfg[wejscia].flags = INPUT_FLAG_PULLUP;

        wejscia++;
		    supla_input_cfg[wejscia].gpio_id = B_BTN2_PORT;
		    supla_input_cfg[wejscia].relay_gpio_id = B_RELAY2_PORT;
				supla_input_cfg[wejscia].type = supla_esp_cfg.Button2Type == BTN_TYPE_BISTABLE ? INPUT_TYPE_BTN_BISTABLE_RS : INPUT_TYPE_BTN_MONOSTABLE_RS;
        supla_input_cfg[wejscia].flags = INPUT_FLAG_PULLUP;
	    // ---------------------------------------
        supla_relay_cfg[0].gpio_id = B_RELAY1_PORT;
        supla_relay_cfg[0].channel = 3;

        supla_relay_cfg[1].gpio_id = B_RELAY2_PORT;
        supla_relay_cfg[1].channel = 3;

        supla_rs_cfg[0].up = &supla_relay_cfg[0];
        supla_rs_cfg[0].down = &supla_relay_cfg[1];

    }

		wejscia++;
		supla_input_cfg[wejscia].type = INPUT_TYPE_SENSOR;
		supla_input_cfg[wejscia].gpio_id = B_WEJSCIE;

		wejscia++;
		supla_input_cfg[wejscia].type = INPUT_TYPE_BTN_BISTABLE;
		supla_input_cfg[wejscia].gpio_id = B_CFG_PORT;
		supla_input_cfg[wejscia].flags = INPUT_FLAG_PULLUP | INPUT_FLAG_CFG_BTN;


}

void supla_esp_board_set_channels(TDS_SuplaDeviceChannel_B *channels, unsigned char *channel_count) {
	if(supla_esp_cfg.RollerShutter == ROLLER_SHUTTER_OFF){
    *channel_count = 5;
    }
    else *channel_count = 4;

    uint8_t nr_channel = 0;
    if(supla_esp_cfg.ThermometerType == THERMOMETER_TYPE_DS18B20){
	    channels[nr_channel].Number = nr_channel;
	    channels[nr_channel].Type = SUPLA_CHANNELTYPE_THERMOMETERDS18B20;
	    channels[nr_channel].FuncList = 0;
	    channels[nr_channel].Default = 0;
      supla_get_temperature(channels[nr_channel].value);
    }
    else if(supla_esp_cfg.ThermometerType == THERMOMETER_TYPE_DHT11){
			channels[nr_channel].Type = SUPLA_CHANNELTYPE_DHT11;
			channels[nr_channel].Default = SUPLA_CHANNELFNC_HUMIDITYANDTEMPERATURE;
			channels[nr_channel].FuncList = 0;
			supla_get_temp_and_humidity(channels[nr_channel].value);
		}
		else if(supla_esp_cfg.ThermometerType == THERMOMETER_TYPE_DHT22){
			channels[nr_channel].Type = SUPLA_CHANNELTYPE_DHT22;
			channels[nr_channel].Default = SUPLA_CHANNELFNC_HUMIDITYANDTEMPERATURE;
			channels[nr_channel].FuncList = 0;
			supla_get_temp_and_humidity(channels[nr_channel].value);
		}

    nr_channel++;
		channels[nr_channel].Number = nr_channel;
		channels[nr_channel].Type = supla_esp_cfg.Sensor1Type == SNR_TYPE_NO ? SUPLA_CHANNELTYPE_SENSORNO : SUPLA_CHANNELTYPE_SENSORNC;
    channels[nr_channel].value[0] = !(gpio__input_get(B_SENSOR_PORT1));

    nr_channel++;
		channels[nr_channel].Number = nr_channel;
		channels[nr_channel].Type = supla_esp_cfg.Sensor2Type == SNR_TYPE_NO ? SUPLA_CHANNELTYPE_SENSORNO : SUPLA_CHANNELTYPE_SENSORNC;
		channels[nr_channel].value[0] = !(gpio__input_get(B_SENSOR_PORT2));

    if(supla_esp_cfg.RollerShutter == ROLLER_SHUTTER_OFF){
		nr_channel++;
		channels[nr_channel].Number = nr_channel;
    channels[nr_channel].Type = SUPLA_CHANNELTYPE_RELAY;
    channels[nr_channel].FuncList =  SUPLA_BIT_RELAYFUNC_LIGHTSWITCH \
								| SUPLA_BIT_RELAYFUNC_POWERSWITCH\
								| SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEGATEWAYLOCK \
								| SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEGATE \
								| SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEGARAGEDOOR \
								| SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEDOORLOCK \
                | SUPLA_BIT_RELAYFUNC_STAIRCASETIMER;
    channels[nr_channel].Default = SUPLA_CHANNELFNC_LIGHTSWITCH;
    channels[nr_channel].value[0] = supla_esp_gpio_relay_on(B_RELAY1_PORT);

		nr_channel++;
		channels[nr_channel].Number = nr_channel;
    channels[nr_channel].Type = SUPLA_CHANNELTYPE_RELAY;
    channels[nr_channel].FuncList =  SUPLA_BIT_RELAYFUNC_LIGHTSWITCH \
								| SUPLA_BIT_RELAYFUNC_POWERSWITCH\
								| SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEGATEWAYLOCK \
								| SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEGATE \
								| SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEGARAGEDOOR \
								| SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEDOORLOCK \
                | SUPLA_BIT_RELAYFUNC_STAIRCASETIMER;
    channels[nr_channel].Default = SUPLA_CHANNELFNC_LIGHTSWITCH;
    channels[nr_channel].value[0] = supla_esp_gpio_relay_on(B_RELAY2_PORT);
    }
    else{
	    nr_channel++;
			channels[nr_channel].Number = nr_channel;
			channels[nr_channel].Type = SUPLA_CHANNELTYPE_RELAY;
			channels[nr_channel].FuncList =  SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEROLLERSHUTTER;
			channels[nr_channel].Default = SUPLA_CHANNELFNC_CONTROLLINGTHEROLLERSHUTTER;
			channels[nr_channel].value[0] = (*supla_rs_cfg[0].position)-1;
    }
}

void supla_esp_board_send_channel_values_with_delay(void *srpc) {

  // supla_esp_channel_value_changed(1, gpio__input_get(B_SENSOR_PORT1));
	// supla_esp_channel_value_changed(2, gpio__input_get(B_SENSOR_PORT2));
  //
  // supla_esp_channel_value_changed(3, supla_esp_gpio_relay_on(B_RELAY1_PORT));
  // supla_esp_channel_value_changed(4, supla_esp_gpio_relay_on(B_RELAY2_PORT));
}
