/*
   ============================================================================
   Name        : multiboard.c
   Author      : Espablo
   Copyright   : GPLv2
   ============================================================================
 */

#include <os_type.h>
#include "supla_esp.h"
#include "supla_dht.h"
#include "supla_ds18b20.h"

#define D3      0
#define TX      1
#define D4      2
#define RX      3
#define D2      4
#define D1      5
#define D11     9
#define D12     10
#define D6      12
#define D7      13
#define D5      14
#define D8      15
#define D0      16





const char * Supported_Modules[MAX_SUPORTED_MODULES] = {
	"CHOOSE BOARD!",
	"ElectroDragon",
	"inCan",
	"inCan-RollerShutter",
	"Melink",
	"Neo Coolcam",
	"RELAYS x9",
	"SENSORS x8",
	"Shelly1",
	"Shelly2",
	"Shelly2-RollerShutter",
	"SONOFF BASIC",
	"SONOFF DUAL R2",
	"SONOFF S2X",
	"SONOFF TH",
	"SONOFF TOUCH",
	"SONOFF TOUCH DUAL",
	"SONOFF TOUCH TRIPLE",
	"SONOFF 4CH",
	"Yunshan",
	"YUNTONG Smart"
	// "test"
};


void ICACHE_FLASH_ATTR supla_esp_board_set_device_name(char *buffer, uint8 buffer_size) {
	char *prefix = (char*)os_malloc(buffer_size+1);
	strcpy(prefix, "*");
	strcat(prefix, Supported_Modules[nr_module]);
	ets_snprintf(buffer, buffer_size, prefix);
	free(prefix);
}

void ICACHE_FLASH_ATTR supla_esp_board_set_channels_gpio(TDS_SuplaDeviceChannel_B *channel_set, uint8 channels_gpio) {
	// supla_esp_cfg.BoardNames = Shelly_1;
	switch_chanel_gpio = channels_gpio;
	nr_module = chartoint(supla_esp_cfg.BoardNames);

	supla_esp_cfg.UartSwap = 0;
	switch( nr_module )
	{
	case ElectroDragon:
		Setting_Up_Button_Relay(0, 13,
		                        supla_esp_cfg.Button1Type,
		                        channel_set);
		Setting_Up_Button_Relay(2, 12,
		                        supla_esp_cfg.Button2Type,
		                        channel_set);
		Setting_Up_User_Configurable(1, channel_set);
		Setting_Up_User_Configurable(3, channel_set);
		Setting_Up_User_Configurable(4, channel_set);
		Setting_Up_User_Configurable(5, channel_set);
		Setting_Up_User_Configurable(14, channel_set);
		Setting_Up_User_Configurable(15, channel_set);
		Setting_Up_Button_Configure(0, false);
		Setting_Up_Led_Configure(16, LED_HIGH_LEVEL);
		break;
// -----------------------------------------------------------------------
	case inCan:
		// supla_esp_cfg.UartSwap = 0;
		Setting_Up_Sensor(4, channel_set);
		Setting_Up_Sensor(16, channel_set);
		Setting_Up_Button_Relay(14, 5,
		                        supla_esp_cfg.Button1Type,
		                        channel_set);
		Setting_Up_Button_Relay(12, 13,
		                        supla_esp_cfg.Button2Type,
		                        channel_set);
		Setting_Up_Temperature(2, supla_esp_cfg.ThermometerType, channel_set);
		Setting_Up_User_Configurable(1, channel_set);
		Setting_Up_User_Configurable(3, channel_set);
		Setting_Up_Button_Configure(0, false);
		Setting_Up_Button_Configure(4, BUTTON_CONFIGURE_x10);
		Setting_Up_Led_Configure(2, LED_HIGH_LEVEL);
		break;
	// -----------------------------------------------------------------------
	case inCan_RollerShutter:
		// supla_esp_cfg.UartSwap = 0;
		Setting_Up_Buttons_RS(14, 12,
		                      5, 13,
		                      supla_esp_cfg.Button1Type,
		                      channel_set );
		Setting_Up_Sensor(4, channel_set);
		Setting_Up_Sensor(16, channel_set);
		Setting_Up_Temperature(2, supla_esp_cfg.ThermometerType, channel_set);
		Setting_Up_User_Configurable(1, channel_set);
		Setting_Up_User_Configurable(3, channel_set);
		Setting_Up_Button_Configure(0, false);
		Setting_Up_Button_Configure(4, BUTTON_CONFIGURE_x10);
		Setting_Up_Led_Configure(2, LED_HIGH_LEVEL);
		break;
	// -----------------------------------------------------------------------
	case Melink:
		Setting_Up_Button_Relay(5, 4,
		                        supla_esp_cfg.Button1Type,
		                        channel_set);
		Setting_Up_User_Configurable(3, channel_set);
		Setting_Up_Button_Configure(5, false);
		Setting_Up_Led_Configure(12, LED_HIGH_LEVEL);
		led_relay = 13;
		break;
	// -----------------------------------------------------------------------
	case Neo_Coolcam:
		Setting_Up_Button_Relay(13, 12,
		                        supla_esp_cfg.Button1Type,
		                        channel_set);
		Setting_Up_Button_Configure(13, false);
		Setting_Up_Led_Configure(4, LED_HIGH_LEVEL);
		break;
	// ----------------------------------------------------------------------
	case RELAYS_x9:
		Setting_Up_Relay(1, channel_set);
		Setting_Up_Relay(3, channel_set);
		Setting_Up_Relay(4, channel_set);
		Setting_Up_Relay(5, channel_set);
		Setting_Up_Relay(12, channel_set);
		Setting_Up_Relay(13, channel_set);
		Setting_Up_Relay(14, channel_set);
		Setting_Up_Relay(15, channel_set);
		Setting_Up_Relay(16, channel_set);
		Setting_Up_Temperature(2, supla_esp_cfg.ThermometerType, channel_set);
		Setting_Up_Button_Configure(0, false);
		Setting_Up_Led_Configure(2, LED_HIGH_LEVEL);
		break;
	// -----------------------------------------------------------------------
	case SENSORS_x8:
		Setting_Up_Sensor(4, channel_set);
		Setting_Up_Sensor(5, channel_set);
		Setting_Up_Sensor(16, channel_set);
		Setting_Up_Sensor(12, channel_set);
		Setting_Up_Sensor(13, channel_set);
		Setting_Up_Sensor(14, channel_set);
		Setting_Up_Sensor(3, channel_set);
		Setting_Up_Sensor(1, channel_set);
		Setting_Up_Temperature(2, supla_esp_cfg.ThermometerType, channel_set);
		Setting_Up_Button_Configure(0, false);
		Setting_Up_Led_Configure(2, LED_HIGH_LEVEL);
		break;
	// -----------------------------------------------------------------------
	case Shelly_1:
		// supla_esp_cfg.UartSwap = 0;
		phaze50Hz = true;
		socket_channel = ONLY_SOCKET_CHANNEL;
		Setting_Up_Button_Relay(5, 4,
		                        supla_esp_cfg.Button1Type,
		                        channel_set);
		Setting_Up_Button_Configure(5, BUTTON_CONFIGURE_x10);
		break;
	// -----------------------------------------------------------------------
	case Shelly_2:
		// supla_esp_cfg.UartSwap = 0;
		phaze50Hz = true;
		socket_channel = ONLY_SOCKET_CHANNEL;
		Setting_Up_Button_Relay(12, 4,
		                        supla_esp_cfg.Button1Type,
		                        channel_set);
		socket_channel = ONLY_SOCKET_CHANNEL;
		Setting_Up_Button_Relay(14, 5,
		                        supla_esp_cfg.Button2Type,
		                        channel_set);

		Setting_Up_Button_Configure(12, BUTTON_CONFIGURE_x10);
		break;
	// -----------------------------------------------------------------------
	case Shelly_2_RollerShutter:
		// supla_esp_cfg.UartSwap = 0;
		Setting_Up_Buttons_RS(12, 14,
		                      4, 5,
		                      supla_esp_cfg.Button1Type,
		                      channel_set );
		Setting_Up_Button_Configure(12, BUTTON_CONFIGURE_x10);
		break;
	// -----------------------------------------------------------------------
	case SONOFF_BASIC:
		supla_esp_cfg.Button1Type = BTN_TYPE_MONOSTABLE;
		Setting_Up_Button_Relay(14, 12,                                                                                                                                                                                                                                                                                                                                                                                                                                             //D2
		                        supla_esp_cfg.Button2Type,
		                        channel_set);
		Setting_Up_Temperature(1, supla_esp_cfg.ThermometerType, channel_set);
		Setting_Up_User_Configurable(3, channel_set);
		Setting_Up_Button_Configure(0, false);
		Setting_Up_Led_Configure(13, LED_LOW_LEVEL);
		new_channel = 0;
		Setting_Up_Button(0, 12,
		                  supla_esp_cfg.Button1Type);
		break;
// -----------------------------------------------------------------------
	case SONOFF_DUAL_R2:
		Setting_Up_Button_Relay(0, 12,
		                        supla_esp_cfg.Button2Type,
		                        channel_set);
		Setting_Up_Button_Relay(9, 5,
		                        supla_esp_cfg.Button2Type,
		                        channel_set);
		Setting_Up_User_Configurable(1, channel_set);
		Setting_Up_User_Configurable(3, channel_set);
		Setting_Up_Button_Configure(10, false);
		Setting_Up_Led_Configure(13, LED_LOW_LEVEL);
		break;
	// -----------------------------------------------------------------------
	case SONOFF_S2X:
		Setting_Up_Button_Relay(0, 12,
		                        supla_esp_cfg.Button1Type,
		                        channel_set);
		Setting_Up_User_Configurable(1, channel_set);
		Setting_Up_User_Configurable(2, channel_set);
		Setting_Up_User_Configurable(3, channel_set);
		Setting_Up_Button_Configure(0, false);
		Setting_Up_Led_Configure(13, LED_LOW_LEVEL);
		break;
	// -----------------------------------------------------------------------
	case SONOFF_TH:
		supla_esp_cfg.Button1Type = BTN_TYPE_MONOSTABLE;
		Setting_Up_Button_Relay(1, 12,
		                        supla_esp_cfg.Button2Type,
		                        channel_set);
		Setting_Up_Temperature(14, supla_esp_cfg.ThermometerType, channel_set);
		Setting_Up_User_Configurable(3, channel_set);
		Setting_Up_Button_Configure(0, false);
		Setting_Up_Led_Configure(13, LED_LOW_LEVEL);
		new_channel = 0;
		Setting_Up_Button(0, 12,
		                  supla_esp_cfg.Button1Type);
		break;
	// -----------------------------------------------------------------------
	case SONOFF_TOUCH:
		// supla_esp_cfg.UartSwap = 0;
		Setting_Up_Button_Relay(0, 12,
		                        supla_esp_cfg.Button1Type,
		                        channel_set);
		Setting_Up_Temperature(2, supla_esp_cfg.ThermometerType, channel_set);
		Setting_Up_User_Configurable(1, channel_set);
		Setting_Up_User_Configurable(3, channel_set);
		Setting_Up_User_Configurable(5, channel_set);
		Setting_Up_Button_Configure(0, false);
		Setting_Up_Led_Configure(13, LED_HIGH_LEVEL);
		break;
	// -----------------------------------------------------------------------
	case SONOFF_TOUCH_DUAL:
		Setting_Up_Button_Relay(0, 12,
		                        supla_esp_cfg.Button1Type,
		                        channel_set);
		Setting_Up_Button_Relay(9, 5,
		                        supla_esp_cfg.Button2Type,
		                        channel_set);
		Setting_Up_Temperature(2, supla_esp_cfg.ThermometerType, channel_set);
		Setting_Up_User_Configurable(1, channel_set);
		Setting_Up_User_Configurable(3, channel_set);
		Setting_Up_Button_Configure(0, false);
		Setting_Up_Led_Configure(13, LED_LOW_LEVEL);
		break;
	// -----------------------------------------------------------------------
	case SONOFF_TOUCH_TRIPLE:
		Setting_Up_Button_Relay(0, 12,
		                        supla_esp_cfg.Button1Type,
		                        channel_set);
		Setting_Up_Button_Relay(9, 5,
		                        supla_esp_cfg.Button2Type,
		                        channel_set);
		Setting_Up_Button_Relay(10, 4,
		                        supla_esp_cfg.Button3Type,
		                        channel_set);
		Setting_Up_Temperature(2, supla_esp_cfg.ThermometerType, channel_set);
		Setting_Up_User_Configurable(1, channel_set);
		Setting_Up_User_Configurable(3, channel_set);
		Setting_Up_Button_Configure(0, false);
		Setting_Up_Led_Configure(13, LED_LOW_LEVEL);
		break;
	// -----------------------------------------------------------------------
	case SONOFF_4CH:
		Setting_Up_Button_Relay(0, 12,
		                        supla_esp_cfg.Button1Type,
		                        channel_set);
		Setting_Up_Button_Relay(9, 5,
		                        supla_esp_cfg.Button2Type,
		                        channel_set);
		Setting_Up_Button_Relay(10, 4,
		                        supla_esp_cfg.Button3Type,
		                        channel_set);
		Setting_Up_Button_Relay(14, 15,
		                        supla_esp_cfg.Button4Type,
		                        channel_set);
		Setting_Up_Temperature(2, supla_esp_cfg.ThermometerType, channel_set);
		Setting_Up_User_Configurable(1, channel_set);
		Setting_Up_User_Configurable(3, channel_set);
		Setting_Up_Button_Configure(0, false);
		Setting_Up_Led_Configure(13, LED_LOW_LEVEL);
		break;
	// -----------------------------------------------------------------------
	case Yunshan:
		Setting_Up_Button_Relay(0, 4,
		                        supla_esp_cfg.Button1Type,
		                        channel_set);
		Setting_Up_User_Configurable(1, channel_set);
		Setting_Up_User_Configurable(3, channel_set);
		Setting_Up_User_Configurable(5, channel_set);
		Setting_Up_Button_Configure(0, false);
		Setting_Up_Led_Configure(2, LED_HIGH_LEVEL);
		break;
	// -----------------------------------------------------------------------
	case YUNTONG_Smart:
		Setting_Up_Button_Relay(12, 4,
		                        supla_esp_cfg.Button1Type,
		                        channel_set);
		Setting_Up_User_Configurable(5, channel_set);
		Setting_Up_Button_Configure(12, false);
		Setting_Up_Led_Configure(15, LED_HIGH_LEVEL);
		led_relay = 13;
		break;

	// -----------------------------------------------------------------------
	// case test:
	//      Setting_Up_User_Configurable(1, channel_set);
	//      Setting_Up_User_Configurable(3, channel_set);
	//      Setting_Up_User_Configurable(4, channel_set);
	//      Setting_Up_User_Configurable(5, channel_set);
	//      Setting_Up_User_Configurable(12, channel_set);
	//      Setting_Up_User_Configurable(13, channel_set);
	//      Setting_Up_User_Configurable(14, channel_set);
	//      Setting_Up_User_Configurable(15, channel_set);
	//
	//      Setting_Up_Button_Configure(0, false);
	//      Setting_Up_Led_Configure(2, LED_HIGH_LEVEL);
	//
	//      break;
	default:
		//jakiś kod
		break;
	}
	static int lock;

	if (channels_gpio == GPIO && lock == 0) {
		lock++;

		supla_log(LOG_DEBUG, "-------------------------");
		supla_log(LOG_DEBUG, "Supported Module - %s", Supported_Modules[nr_module]);
		supla_log(LOG_DEBUG, "-------------------------");

		if (supla_esp_cfg.BoardNames != 0) {
			uint8 nr = 1;
			for (size_t i = 0; i < relay_counter; i++) {
				supla_log(LOG_DEBUG, "%i. Relay: GPIO %i", nr, supla_relay_cfg[i].gpio_id);
				nr++;
			}
			if (nr_button != 0) {
				supla_log(LOG_DEBUG, "~~~~~~~~~~");
				nr = 1;
				for (size_t i = 0; i <= input_counter; i++) {
					if (supla_input_cfg[i].type == INPUT_TYPE_BTN_BISTABLE || supla_input_cfg[i].type == INPUT_TYPE_BTN_MONOSTABLE || supla_input_cfg[i].type == INPUT_TYPE_BTN_MONOSTABLE_RS) {
						// Supported_Button[]
						if (supla_input_cfg[i].type == INPUT_TYPE_BTN_BISTABLE) {
							supla_log(LOG_DEBUG, "%i. Button Bistable: GPIO %i", nr, supla_input_cfg[i].gpio_id);
						}
						else supla_log(LOG_DEBUG, "%i. Button Monostable: GPIO %i", nr, supla_input_cfg[i].gpio_id);
						nr++;
					}
				}
			}

			if (nr_sensor != 0) {
				supla_log(LOG_DEBUG, "~~~~~~~~~~");
				nr = 1;
				for (size_t i = 0; i <= input_counter; i++) {
					if (supla_input_cfg[i].type == INPUT_TYPE_SENSOR) {
						supla_log(LOG_DEBUG, "%i. Sensor: GPIO %i", nr, supla_input_cfg[i].gpio_id);
						nr++;
					}
				}
			}

			// if (nr_temperature != 0) {
			// supla_log(LOG_DEBUG, "~~~~~~~~~~");
			// if(supla_esp_cfg.Thermometer_GPIO >= 17) supla_log(LOG_DEBUG, "Thermometer: GPIO %i", supla_esp_cfg.Thermometer_GPIO -7);
			// else supla_log(LOG_DEBUG, "Thermometer: GPIO %i", supla_esp_cfg.Thermometer_GPIO);
			// supla_log(LOG_DEBUG, "Thermometer_DHT: GPIO %i", supla_esp_cfg.Thermometer_DHT_GPIO);
			// supla_log(LOG_DEBUG, "ThermometerType %i", supla_esp_cfg.ThermometerType);
			// }
			supla_log(LOG_DEBUG, "~~~~~~~~~~");

			if(supla_esp_cfg.BtnConfig_GPIO >= 24) supla_log(LOG_DEBUG, "Btn Config: OFF");
			else if(supla_esp_cfg.BtnConfig_GPIO >= 17) supla_log(LOG_DEBUG, "Btn Config: GPIO %i", supla_esp_cfg.BtnConfig_GPIO -7);
			else supla_log(LOG_DEBUG, "Btn Config: GPIO %i", supla_esp_cfg.BtnConfig_GPIO);

			if(supla_esp_cfg.LedConfig_GPIO >= 24) supla_log(LOG_DEBUG, "Led Config: OFF");
			else if(supla_esp_cfg.LedConfig_GPIO >= 17) {
				supla_log(LOG_DEBUG, "Led Config: GPIO %i", supla_esp_cfg.LedConfig_GPIO -7);
			}
			else {
				supla_log(LOG_DEBUG, "Led Config: GPIO %i", supla_esp_cfg.LedConfig_GPIO);
			}
			supla_log(LOG_DEBUG, "-------------------------");
			if (supla_esp_cfg.StatusLedOff == 0) {
				supla_log(LOG_DEBUG, "Led Config: OFF");
			}
			else if (supla_esp_cfg.StatusLedOff == 1) {
				supla_log(LOG_DEBUG, "Led Config: ON");
			}
			else if (supla_esp_cfg.StatusLedOff == 2) {
				supla_log(LOG_DEBUG, "Led Config: Config Only");
			}
			supla_log(LOG_DEBUG, "-------------------------");
		}
		supla_log(LOG_DEBUG, "UartSwap = %i", supla_esp_cfg.UartSwap);

	}

}

void supla_esp_board_send_channel_values_with_delay(void *srpc) {
	// if (supla_esp_cfg.UartSwap == 1) {
	//      if (uart_rx_io == GPIO_OUTPUT) {
	//              supla_esp_gpio_set_hi(3, 0);
	//      }
	//      if (uart_tx_io == GPIO_OUTPUT) {
	//              supla_esp_gpio_set_hi(1, 0);
	//      }
	// }
}
