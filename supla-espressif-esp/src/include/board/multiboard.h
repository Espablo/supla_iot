/*
   ============================================================================
   Name        : multiboard.h
   Author      : Espablo
   Copyright   : GPLv2
   ============================================================================
 */

#ifndef MULTIBOARD_H_
#define MULTIBOARD_H_

#define ESP8266_SUPLA_PROTO_VERSION 7
#define MULTIBOARD_TYPE_SELECTION
#define NAME_BOARD    "Multiboard by Espablo"

extern const char * Supported_Modules[];
extern const char * Supported_Temperature[];
extern const char * Supported_Button[];
// extern const char * Supported_GPIO[];
extern const char * Supported_LED[];
extern const char * Supported_ButtonConfigure[];
extern const char * User_Configurable[];



enum {
  CHOSE_BOARD,
  ElectroDragon,
  inCan,
  inCan_RollerShutter,
  Melink,
  Neo_Coolcam,
  RELAYS_x9,
  SENSORS_x8,
  Shelly_1,
  Shelly_2,
  Shelly_2_RollerShutter,
  SONOFF_BASIC,
  SONOFF_DUAL_R2,
  SONOFF_S2X,
  SONOFF_TH,
  SONOFF_TOUCH,
  SONOFF_TOUCH_DUAL,
  SONOFF_TOUCH_TRIPLE,
  SONOFF_4CH,
  Yunshan,
  YUNTONG_Smart,
  // test,
  MAX_SUPORTED_MODULES
};

  #define LED_CONFIG_PORT     2

  #define B_RELAY1_PORT       5
  #define B_RELAY2_PORT       13

  #define B_CFG_PORT          4
  #define B_BTN_CFG_PORT      0

  #define B_BTN1_PORT         14
  #define B_BTN2_PORT         12

  #define B_SENSOR_PORT1      4
  #define B_SENSOR_PORT2      16

  #define TEMPERATURE_PORT    2

  #define USE_GPIO16_INPUT


  #define DS18B20
  #define DHTSENSOR
  #define SENSOR_DHT22

  #define RELAY_MIN_DELAY     100
  // #define WATCHDOG_TIMEOUT    90000000
  #define _ROLLERSHUTTER_SUPPORT


  #define MAX_NAME_BOARD      12

void supla_esp_board_send_channel_values_with_delay(void *srpc);


#endif //MULTIBOARD_H_
