/*
   ============================================================================
   Name        : inCan.c
   Author      : Espablo
   Copyright   : GPLv2
   ============================================================================
 */


#include "supla_esp.h"
#include "supla_dht.h"
#include "supla_ds18b20.h"

// #define CHANN_RELAY_1		0
// #define CHANN_RELAY_2		1
// #define CHANN_THERMOMETER	2
// #define CHANN_SENSOR_1		3
// #define CHANN_SENSOR_2		4

#define B_WEJSCIE          13


char Relay1GPIO;
char Relay2GPIO;
char Button1GPIO;
char Button2GPIO;
char Sensor1GPIO;
char Sensor2GPIO;
char ThermometerGPIO;
char BtnConfigGPIO;
char ConfigGPIO;
char LedConfigGPIO;

const char * Supported_GPIO[] = {
	"GPIO_0(D3)",
	"GPIO_1(TX)",
	"GPIO_2(D4)",
	"GPIO_3(RX)",
	"GPIO_4(D2)",
	"GPIO_5(D1)",
	"",
	"",
	"",
	"GPIO_9(D11)",
	"GPIO_10(D12)",
	"",
	"GPIO_12(D6)",
	"GPIO_13(D7)",
	"GPIO_14(D5)",
	"GPIO_15(D8)",
	"GPIO_16(D0)",
	"OFF"
};

const uint8_t rsa_public_key_bytes[512] = {
	0xd8, 0xa6, 0x9a, 0x59, 0xdd, 0x48, 0x82, 0xdd,
	0x28, 0x32, 0x9e, 0x62, 0xd4, 0x73, 0x2b, 0xc0,
	0xf7, 0xb9, 0x47, 0x7c, 0x20, 0x6b, 0x8b, 0xa2,
	0xf7, 0x3b, 0x65, 0x9b, 0xa5, 0x37, 0xf0, 0x4c,
	0x94, 0xfb, 0x5e, 0xa5, 0xca, 0x29, 0x45, 0x42,
	0x1d, 0x57, 0xd5, 0x94, 0xbe, 0x23, 0xbc, 0x2d,
	0x8f, 0x06, 0x3b, 0x73, 0x24, 0x37, 0x60, 0x35,
	0xd5, 0x9d, 0xed, 0xb7, 0x9e, 0x81, 0x5e, 0xf9,
	0x1d, 0xa1, 0xdc, 0x30, 0x3d, 0xec, 0x9e, 0x9d,
	0x62, 0xe4, 0xe1, 0x0e, 0x48, 0x5d, 0xa1, 0xb2,
	0xe3, 0xcd, 0xc8, 0xeb, 0xf9, 0xdc, 0xfe, 0x67,
	0xb0, 0x0f, 0x0a, 0x94, 0xa7, 0xc6, 0x67, 0x11,
	0x5d, 0x16, 0x43, 0x72, 0xbb, 0x8e, 0x9d, 0xbc,
	0x65, 0x8f, 0xb2, 0x7b, 0x91, 0xc4, 0x38, 0x35,
	0xa9, 0x30, 0xad, 0x64, 0xf8, 0x16, 0x97, 0xbc,
	0x27, 0x84, 0x62, 0xc6, 0x13, 0x19, 0x3b, 0xa6,
	0xcd, 0xd5, 0x02, 0xc2, 0x5d, 0x65, 0x8b, 0x88,
	0x43, 0x6d, 0x70, 0xdb, 0xad, 0xf7, 0xe7, 0xf1,
	0xca, 0xa1, 0x1a, 0xe5, 0x0f, 0x0d, 0xb7, 0xa1,
	0x28, 0x4c, 0x62, 0x52, 0x38, 0x22, 0xc5, 0x33,
	0xa9, 0x99, 0xcc, 0x19, 0xe1, 0xe4, 0xc4, 0x9e,
	0x56, 0x75, 0x91, 0x96, 0x65, 0xaa, 0x8d, 0xc4,
	0xc3, 0x66, 0xa5, 0x70, 0xeb, 0xa7, 0x25, 0xc0,
	0xda, 0xbe, 0x61, 0xee, 0x04, 0xfa, 0x68, 0x0a,
	0x81, 0xb6, 0x84, 0x70, 0x29, 0xf0, 0xfa, 0xfe,
	0x78, 0xc3, 0x3e, 0x0f, 0xcb, 0x25, 0x0b, 0xb8,
	0x6a, 0xe9, 0x62, 0xdc, 0x1f, 0xa2, 0x50, 0x07,
	0x13, 0xb6, 0x9b, 0x15, 0xbd, 0x5c, 0x9e, 0x29,
	0x2f, 0xc9, 0x0a, 0x02, 0x01, 0xf8, 0x1f, 0x67,
	0xce, 0x29, 0xdd, 0x7a, 0xf6, 0x19, 0x72, 0x13,
	0x0b, 0xb2, 0x40, 0x2b, 0x22, 0x12, 0xd5, 0x03,
	0xa4, 0x8e, 0xe8, 0x06, 0x0e, 0xe6, 0x24, 0x49,
	0xcc, 0xa6, 0x9a, 0xa4, 0x4e, 0x16, 0xa3, 0x6e,
	0x3b, 0x43, 0xe3, 0x58, 0xa9, 0xff, 0x70, 0xfe,
	0x9d, 0xc1, 0x8b, 0x3c, 0x01, 0x32, 0xae, 0x64,
	0xb8, 0x5d, 0xf6, 0xcb, 0x2d, 0xda, 0xd6, 0x44,
	0xf9, 0x11, 0x51, 0xce, 0x56, 0xb2, 0x2e, 0x60,
	0xb7, 0xbd, 0xb6, 0x31, 0x69, 0xad, 0xda, 0x5a,
	0x5e, 0x2c, 0x30, 0x06, 0x74, 0xe8, 0xf4, 0xa0,
	0x90, 0x4d, 0x3f, 0xc9, 0xc3, 0x64, 0xd1, 0x7b,
	0x9a, 0xc0, 0x3f, 0x4e, 0x1e, 0x3f, 0x49, 0x62,
	0xdf, 0xb5, 0x50, 0x30, 0x82, 0xd6, 0x32, 0x2e,
	0x8b, 0x5a, 0x0c, 0x9c, 0xdd, 0xc9, 0xb8, 0x32,
	0x8f, 0xb4, 0x88, 0xc6, 0xd7, 0x2a, 0x73, 0xb4,
	0x62, 0x7e, 0x39, 0xa5, 0x1c, 0xd3, 0x9c, 0x29,
	0x50, 0x17, 0x81, 0x14, 0x83, 0x30, 0xad, 0xff,
	0xf5, 0x74, 0x96, 0xd6, 0x6b, 0x63, 0x32, 0x69,
	0x73, 0xee, 0x79, 0xda, 0x0f, 0xfc, 0xb2, 0x57,
	0x60, 0xed, 0x54, 0xe0, 0xf4, 0x21, 0x0c, 0x4e,
	0x7f, 0x53, 0x71, 0xc4, 0xd2, 0xd3, 0x2e, 0x24,
	0xbf, 0x2b, 0x76, 0x47, 0x75, 0x29, 0xc1, 0xe7,
	0xe7, 0xe9, 0x66, 0xa3, 0x3c, 0x8b, 0x8d, 0x3e,
	0x6f, 0x44, 0x81, 0x59, 0x9a, 0x1a, 0x83, 0xac,
	0x20, 0x27, 0x98, 0xe0, 0x0d, 0xc6, 0xd0, 0xf7,
	0xff, 0xad, 0x0b, 0xff, 0x6a, 0x87, 0x59, 0x38,
	0x92, 0x53, 0x40, 0x33, 0x8a, 0x0b, 0x09, 0xf9,
	0x21, 0xc0, 0xb4, 0xe9, 0xc3, 0x2e, 0x39, 0x52,
	0x99, 0x72, 0x7b, 0xf3, 0xda, 0xa4, 0x17, 0x76,
	0x56, 0xc1, 0xe6, 0xd1, 0xde, 0x2a, 0x43, 0x35,
	0x0b, 0xe3, 0x08, 0x63, 0x1a, 0x1a, 0x60, 0x45,
	0x84, 0xcf, 0x98, 0xb4, 0x56, 0x13, 0xc8, 0x7c,
	0x5a, 0xd2, 0x87, 0xe2, 0xc9, 0x4e, 0xfb, 0x27,
	0x38, 0x2d, 0xd3, 0x28, 0xa6, 0x0f, 0x48, 0x97,
	0x0e, 0xb8, 0x05, 0x26, 0x8f, 0xfd, 0xd1, 0x21
};


void ICACHE_FLASH_ATTR supla_esp_board_set_device_name(char *buffer, uint8 buffer_size) {
	// #if defined(INCAN_EASY_TYPE_SELECTION)
	//   ets_snprintf(buffer, buffer_size, NAME_BOARD "-EASY");
	//   #else
	if(supla_esp_cfg.RollerShutter == ROLLER_SHUTTER_OFF) {
		ets_snprintf(buffer, buffer_size, NAME_BOARD );
	}
	else {
		ets_snprintf(buffer, buffer_size, NAME_BOARD "-RollerShutter");
	}
// #endif
}


void ICACHE_FLASH_ATTR supla_esp_board_gpio_init(void) {

	sensor_channel = USE_GPIO16_INPUT;
  #if defined(INCAN_TYPE_SELECTION) || defined(__BOARD_inCan_test)

	Relay1GPIO = B_RELAY1_PORT;
	Relay2GPIO = B_RELAY2_PORT;
	Button1GPIO = B_BTN1_PORT;
	Button2GPIO = B_BTN2_PORT;
	Sensor1GPIO = B_SENSOR_PORT1;
	Sensor2GPIO = B_SENSOR_PORT2;
	BtnConfigGPIO = B_BTN_CFG_PORT;
	ConfigGPIO = B_CFG_PORT;
	ThermometerGPIO = TEMPERATURE_PORT;
	LedConfigGPIO = LED_CONFIG_PORT;

  #elif defined(INCAN_EASY_TYPE_SELECTION)
	// LedConfigGPIO = LED_CONFIG_PORT;

	if(supla_esp_cfg.Relay1_GPIO >= 17) Relay1GPIO = supla_esp_cfg.Relay1_GPIO - 7;
	else Relay1GPIO = supla_esp_cfg.Relay1_GPIO;

	if(supla_esp_cfg.Relay2_GPIO >= 17) Relay2GPIO = supla_esp_cfg.Relay2_GPIO - 7;
	else Relay2GPIO = supla_esp_cfg.Relay2_GPIO;

	if(supla_esp_cfg.Button1_GPIO >= 17) Button1GPIO = supla_esp_cfg.Button1_GPIO -7;
	else Button1GPIO = supla_esp_cfg.Button1_GPIO;

	if(supla_esp_cfg.Button2_GPIO >= 17) Button2GPIO = supla_esp_cfg.Button2_GPIO -7;
	else Button2GPIO = supla_esp_cfg.Button2_GPIO;

	if(supla_esp_cfg.Sensor1_GPIO >= 17) Sensor1GPIO = supla_esp_cfg.Sensor1_GPIO - 7;
	else Sensor1GPIO = supla_esp_cfg.Sensor1_GPIO;

	if(supla_esp_cfg.Sensor2_GPIO >= 17) Sensor2GPIO = supla_esp_cfg.Sensor2_GPIO - 7;
	else Sensor2GPIO = supla_esp_cfg.Sensor2_GPIO;

	if(supla_esp_cfg.Thermometer_GPIO >= 17) ThermometerGPIO = supla_esp_cfg.Thermometer_GPIO - 7;
	else ThermometerGPIO = supla_esp_cfg.Thermometer_GPIO;

	if(supla_esp_cfg.BtnConfig_GPIO >= 17) BtnConfigGPIO = supla_esp_cfg.BtnConfig_GPIO - 7;
	else BtnConfigGPIO = supla_esp_cfg.BtnConfig_GPIO;

	if(supla_esp_cfg.Config_GPIO >= 17) ConfigGPIO = supla_esp_cfg.Config_GPIO - 7;
	else ConfigGPIO = supla_esp_cfg.Config_GPIO;

  #endif

	uint8_t wejscia = 0;
	supla_input_cfg[wejscia].channel = 0;
	// supla_input_cfg[wejscia].type = INPUT_TYPE_BTN_MONOSTABLE;
	supla_input_cfg[wejscia].gpio_id = BtnConfigGPIO;
	supla_input_cfg[wejscia].type = supla_esp_cfg.CfgButtonType == BTN_TYPE_BISTABLE ? INPUT_TYPE_BTN_BISTABLE : INPUT_TYPE_BTN_MONOSTABLE;
	supla_input_cfg[wejscia].flags = INPUT_FLAG_PULLUP | INPUT_FLAG_CFG_BTN;

	wejscia++;
	supla_input_cfg[wejscia].channel = 1;
	supla_input_cfg[wejscia].gpio_id = Sensor1GPIO;
	supla_input_cfg[wejscia].type = INPUT_TYPE_SENSOR;
	supla_input_cfg[wejscia].flags = INPUT_FLAG_PULLUP;

	wejscia++;
	supla_input_cfg[wejscia].channel = 2;
	supla_input_cfg[wejscia].gpio_id = Sensor2GPIO;
	supla_input_cfg[wejscia].type = INPUT_TYPE_SENSOR;
	supla_input_cfg[wejscia].flags = INPUT_FLAG_PULLUP;

	if(supla_esp_cfg.RollerShutter == ROLLER_SHUTTER_OFF) {
		wejscia++;
		supla_input_cfg[wejscia].channel = 3;
		supla_input_cfg[wejscia].gpio_id = Button1GPIO;
		supla_input_cfg[wejscia].relay_gpio_id = Relay1GPIO;
		supla_input_cfg[wejscia].type = supla_esp_cfg.Button1Type == BTN_TYPE_BISTABLE ? INPUT_TYPE_BTN_BISTABLE : INPUT_TYPE_BTN_MONOSTABLE;
		supla_input_cfg[wejscia].flags = INPUT_FLAG_PULLUP;

		wejscia++;
		supla_input_cfg[wejscia].channel = 4;
		supla_input_cfg[wejscia].gpio_id = Button2GPIO;
		supla_input_cfg[wejscia].relay_gpio_id = Relay2GPIO;
		supla_input_cfg[wejscia].type = supla_esp_cfg.Button2Type == BTN_TYPE_BISTABLE ? INPUT_TYPE_BTN_BISTABLE : INPUT_TYPE_BTN_MONOSTABLE;
		supla_input_cfg[wejscia].flags = INPUT_FLAG_PULLUP;
		// ---------------------------------------
		supla_relay_cfg[0].gpio_id = Relay1GPIO;
		if( supla_esp_cfg.Relay1Level == RELAY_LOW_LEVEL) supla_relay_cfg[0].flags = RELAY_FLAG_RESTORE_FORCE \
			                                                                     | RELAY_FLAG_LO_LEVEL_TRIGGER;
		else supla_relay_cfg[0].flags = RELAY_FLAG_RESTORE_FORCE;
		supla_relay_cfg[0].channel = 3;

		supla_relay_cfg[1].gpio_id = Relay2GPIO;
		if( supla_esp_cfg.Relay2Level == RELAY_LOW_LEVEL) supla_relay_cfg[1].flags = RELAY_FLAG_RESTORE_FORCE \
			                                                                     | RELAY_FLAG_LO_LEVEL_TRIGGER;
		else supla_relay_cfg[1].flags = RELAY_FLAG_RESTORE_FORCE;
		supla_relay_cfg[1].channel = 4;
	}
	else {
		wejscia++;
		supla_input_cfg[wejscia].gpio_id = Button1GPIO;
		supla_input_cfg[wejscia].relay_gpio_id = Relay1GPIO;
		supla_input_cfg[wejscia].type = supla_esp_cfg.Button1Type == BTN_TYPE_BISTABLE ? INPUT_TYPE_BTN_BISTABLE : INPUT_TYPE_BTN_MONOSTABLE_RS;
		supla_input_cfg[wejscia].flags = INPUT_FLAG_PULLUP;

		wejscia++;
		supla_input_cfg[wejscia].gpio_id = Button2GPIO;
		supla_input_cfg[wejscia].relay_gpio_id = Relay2GPIO;
		supla_input_cfg[wejscia].type = supla_esp_cfg.Button2Type == BTN_TYPE_BISTABLE ? INPUT_TYPE_BTN_BISTABLE : INPUT_TYPE_BTN_MONOSTABLE_RS;
		supla_input_cfg[wejscia].flags = INPUT_FLAG_PULLUP;
		// ---------------------------------------
		supla_relay_cfg[0].gpio_id = Relay1GPIO;
		supla_relay_cfg[0].channel = 3;

		supla_relay_cfg[1].gpio_id = Relay2GPIO;
		supla_relay_cfg[1].channel = 3;

		supla_rs_cfg[0].up = &supla_relay_cfg[0];
		supla_rs_cfg[0].down = &supla_relay_cfg[1];

	}

	wejscia++;
	supla_input_cfg[wejscia].type = INPUT_TYPE_SENSOR;
	supla_input_cfg[wejscia].gpio_id = B_WEJSCIE;
#if defined(INCAN_TYPE_SELECTION)
	wejscia++;
	supla_input_cfg[wejscia].type = INPUT_TYPE_BTN_BISTABLE;
	supla_input_cfg[wejscia].gpio_id = ConfigGPIO;
	supla_input_cfg[wejscia].flags = INPUT_FLAG_PULLUP | INPUT_FLAG_CFG_BTN;
#endif

}

void supla_esp_board_set_channels(TDS_SuplaDeviceChannel_B *channels, unsigned char *channel_count) {
	if(supla_esp_cfg.RollerShutter == ROLLER_SHUTTER_OFF) {
		*channel_count = 5;
	}
	else *channel_count = 4;

	uint8_t nr_channel = 0;
	if(supla_esp_cfg.ThermometerType == THERMOMETER_TYPE_DS18B20) {
		channels[nr_channel].Number = nr_channel;
		channels[nr_channel].Type = SUPLA_CHANNELTYPE_THERMOMETERDS18B20;
		channels[nr_channel].FuncList = 0;
		channels[nr_channel].Default = 0;
		supla_get_temperature(channels[nr_channel].value);
	}
	else if(supla_esp_cfg.ThermometerType == THERMOMETER_TYPE_DHT11) {
		channels[nr_channel].Type = SUPLA_CHANNELTYPE_DHT11;
		channels[nr_channel].Default = SUPLA_CHANNELFNC_HUMIDITYANDTEMPERATURE;
		channels[nr_channel].FuncList = 0;
		supla_get_temp_and_humidity(channels[nr_channel].value);
	}
	else if(supla_esp_cfg.ThermometerType == THERMOMETER_TYPE_DHT22) {
		channels[nr_channel].Type = SUPLA_CHANNELTYPE_DHT22;
		channels[nr_channel].Default = SUPLA_CHANNELFNC_HUMIDITYANDTEMPERATURE;
		channels[nr_channel].FuncList = 0;
		supla_get_temp_and_humidity(channels[nr_channel].value);
	}

	nr_channel++;
	channels[nr_channel].Number = nr_channel;
	channels[nr_channel].Type = supla_esp_cfg.Sensor1Type == SNR_TYPE_NO ? SUPLA_CHANNELTYPE_SENSORNO : SUPLA_CHANNELTYPE_SENSORNC;
	channels[nr_channel].value[0] = !(gpio__input_get(Sensor1GPIO));

	nr_channel++;
	channels[nr_channel].Number = nr_channel;
	channels[nr_channel].Type = supla_esp_cfg.Sensor2Type == SNR_TYPE_NO ? SUPLA_CHANNELTYPE_SENSORNO : SUPLA_CHANNELTYPE_SENSORNC;
	channels[nr_channel].value[0] = !(gpio__input_get(Sensor2GPIO));

	if(supla_esp_cfg.RollerShutter == ROLLER_SHUTTER_OFF) {
		nr_channel++;
		channels[nr_channel].Number = nr_channel;
		channels[nr_channel].Type = SUPLA_CHANNELTYPE_RELAY;
		channels[nr_channel].FuncList =  SUPLA_BIT_RELAYFUNC_LIGHTSWITCH \
		                                | SUPLA_BIT_RELAYFUNC_POWERSWITCH \
		                                | SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEGATEWAYLOCK \
		                                | SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEGATE \
		                                | SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEGARAGEDOOR \
		                                | SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEDOORLOCK \
		                                | SUPLA_BIT_RELAYFUNC_STAIRCASETIMER;
		channels[nr_channel].Default = SUPLA_CHANNELFNC_LIGHTSWITCH;
		channels[nr_channel].value[0] = supla_esp_gpio_relay_on(Relay1GPIO);

		nr_channel++;
		channels[nr_channel].Number = nr_channel;
		channels[nr_channel].Type = SUPLA_CHANNELTYPE_RELAY;
		channels[nr_channel].FuncList =  SUPLA_BIT_RELAYFUNC_LIGHTSWITCH \
		                                | SUPLA_BIT_RELAYFUNC_POWERSWITCH \
		                                | SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEGATEWAYLOCK \
		                                | SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEGATE \
		                                | SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEGARAGEDOOR \
		                                | SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEDOORLOCK \
		                                | SUPLA_BIT_RELAYFUNC_STAIRCASETIMER;
		channels[nr_channel].Default = SUPLA_CHANNELFNC_LIGHTSWITCH;
		channels[nr_channel].value[0] = supla_esp_gpio_relay_on(Relay2GPIO);
	}
	else{
		nr_channel++;
		channels[nr_channel].Number = nr_channel;
		channels[nr_channel].Type = SUPLA_CHANNELTYPE_RELAY;
		channels[nr_channel].FuncList =  SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEROLLERSHUTTER;
		channels[nr_channel].Default = SUPLA_CHANNELFNC_CONTROLLINGTHEROLLERSHUTTER;
		channels[nr_channel].value[0] = (*supla_rs_cfg[0].position)-1;
	}
}

void supla_esp_board_send_channel_values_with_delay(void *srpc) {

	// supla_esp_channel_value_changed(1, gpio__input_get(Sensor1GPIO));
	// supla_esp_channel_value_changed(2, gpio__input_get(Sensor2GPIO));

	// supla_esp_channel_value_changed(3, supla_esp_gpio_relay_is_hi(Relay1GPIO));
	// supla_esp_channel_value_changed(4, supla_esp_gpio_relay_is_hi(Relay2GPIO));
}
