/*
 ============================================================================
 Name        : inCan.h
 Author      : Espablo
 Copyright   : GPLv2
 ============================================================================
*/

#ifndef ESPABLO_H_
#define ESPABLO_H_

#ifdef __BOARD_espablo_test

    #define NAME_BOARD		"espablo_test"
    //#define OLED_DISPLAY

#elif defined(__BOARD_espablo_Easy)

  #define NAME_BOARD		"inCan-Easy"
  #define INCAN_EASY_TYPE_SELECTION

  #define LED_RED_PORT    	2
  #define LEDCONFIG_PORT		2

  char LedConfigGPIO;
#else

    #define NAME_BOARD		"inCan"
    //#define OLED_DISPLAY

    #define INCAN_TYPE_SELECTION
    #define LED_RED_PORT    	   2
    // #define LED_CONFIG_PORT		2
    #define ESP8266_SUPLA_PROTO_VERSION 7
    
#endif

    #define B_RELAY1_PORT       5
    #define B_RELAY2_PORT       13

    #define B_CFG_PORT        	4
    #define B_BTN_CFG_PORT      0


    #define B_BTN1_PORT			14
    #define B_BTN2_PORT			12

    #define B_SENSOR_PORT1     	4
    #define B_SENSOR_PORT2     	16

// #define USE_GPIO16_INPUT  16
#define USE_GPIO16_INPUT	2
//#define USE_GPIO16_OUTPUT

//GPIO02
#define DS18B20
#define TEMPERATURE_CHANNEL 	0

#define DHTSENSOR
//#define SENSOR_DHT11
#define SENSOR_DHT22
#define TEMPERATURE_HUMIDITY_CHANNEL	0

#define TEMPERATURE_PORT		2
#define RELAY_MIN_DELAY  100
#define WATCHDOG_TIMEOUT 90000000
#define _ROLLERSHUTTER_SUPPORT

void supla_esp_board_send_channel_values_with_delay(void *srpc);
#endif
