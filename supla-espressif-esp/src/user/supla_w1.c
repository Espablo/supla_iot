/*
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#include <os_type.h>
#include <osapi.h>
#include <eagle_soc.h>
#include <gpio.h>

#include "supla_esp_cfg.h"
#include "supla_w1.h"


#if defined(INCAN_EASY_TYPE_SELECTION)

int supla_w1_pin;

#elif defined(INCAN_TYPE_SELECTION)

  #ifdef TEMPERATURE_PORT
int supla_w1_pin = GPIO_ID_PIN(TEMPERATURE_PORT);
  #endif

#else

     #ifdef W1_GPIO0
int supla_w1_pin = GPIO_ID_PIN(0);
    #elif defined(W1_GPIO3)
int supla_w1_pin = GPIO_ID_PIN(3);
    #elif defined(W1_GPIO4)
int supla_w1_pin = GPIO_ID_PIN(4);
     #elif defined(W1_GPIO5)
int supla_w1_pin = GPIO_ID_PIN(5);
     #elif defined(W1_GPIO14)
int supla_w1_pin = GPIO_ID_PIN(14);
     #else
int supla_w1_pin = GPIO_ID_PIN(2);
     #endif

#endif

void ICACHE_FLASH_ATTR supla_w1_init(void) {

#if defined(INCAN_EASY_TYPE_SELECTION) || defined(MULTIBOARD_TYPE_SELECTION)
	int w1_pin_gpio;
	if(supla_esp_cfg.Thermometer_GPIO >= 17) w1_pin_gpio = supla_esp_cfg.Thermometer_GPIO - 7;
	else w1_pin_gpio = supla_esp_cfg.Thermometer_GPIO;

	supla_w1_pin = GPIO_ID_PIN( w1_pin_gpio);

	if(w1_pin_gpio == 0) {
		PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO0_U, FUNC_GPIO0);
		PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO0_U);
	}
	else if(w1_pin_gpio == 2) {
		PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO2_U, FUNC_GPIO2);
		PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO2_U);
	}
	else if(w1_pin_gpio == 3) {
		// system_uart_swap ();
		PIN_FUNC_SELECT(PERIPHS_IO_MUX_U0RXD_U, FUNC_GPIO3);
		PIN_PULLUP_EN(PERIPHS_IO_MUX_U0RXD_U);
	}
	else if(w1_pin_gpio == 4) {
		PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO4_U, FUNC_GPIO4);
		PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO4_U);
	}
	else if(w1_pin_gpio == 5) {
		PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO5_U, FUNC_GPIO5);
		PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO5_U);
	}
	else if(w1_pin_gpio == 9) {
		PIN_FUNC_SELECT(PERIPHS_IO_MUX_SD_DATA2_U, FUNC_GPIO9);
		PIN_PULLUP_EN(PERIPHS_IO_MUX_SD_DATA2_U);
	}
	else if(w1_pin_gpio == 10) {
		PIN_FUNC_SELECT(PERIPHS_IO_MUX_SD_DATA3_U, FUNC_GPIO10);
		PIN_PULLUP_EN(PERIPHS_IO_MUX_SD_DATA3_U);
	}
	else if(w1_pin_gpio == 12) {
		PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTDI_U, FUNC_GPIO12);
		PIN_PULLUP_EN(PERIPHS_IO_MUX_MTDI_U);
	}
	else if(w1_pin_gpio == 13) {
		PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTCK_U, FUNC_GPIO13);
		PIN_PULLUP_EN(PERIPHS_IO_MUX_MTCK_U);
	}
	else if(w1_pin_gpio == 14) {
		PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTMS_U, FUNC_GPIO14);
		PIN_PULLUP_EN(PERIPHS_IO_MUX_MTMS_U);
	}
	else if(w1_pin_gpio == 15) {
		PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTDO_U, FUNC_GPIO15);
		PIN_PULLUP_EN(PERIPHS_IO_MUX_MTDO_U);
	}
	else if(w1_pin_gpio == 16) {
	}


#endif

	#ifdef W1_GPIO0
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO0_U, FUNC_GPIO0);
	PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO0_U);
    #elif defined(W1_GPIO5)
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO5_U, FUNC_GPIO5);
	PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO5_U);
	#elif defined(W1_GPIO3)

	system_uart_swap ();

	PIN_FUNC_SELECT(PERIPHS_IO_MUX_U0RXD_U, FUNC_GPIO3);
	PIN_PULLUP_EN(PERIPHS_IO_MUX_U0RXD_U);
	#elif defined(W1_GPIO14)
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTMS_U, FUNC_GPIO14);
	PIN_PULLUP_EN(PERIPHS_IO_MUX_MTMS_U);
    #else
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO2_U, FUNC_GPIO2);
	PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO2_U);
    #endif

	GPIO_DIS_OUTPUT( supla_w1_pin );
}
