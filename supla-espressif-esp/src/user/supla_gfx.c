

#include <os_type.h>
#include <stdlib.h>

#include "supla_ssd1306.h"
#include "supla_w1.h"

#include "supla_gfx.h"
#include "supla_bitmap.h"
#include "supla_font.h"
#include "pgmspace.h"
#include "stdlib_noniso.h"

#ifdef OLED_DISPLAY


int cursor_x, cursor_y;


uint8_t ssd1306_bufor[BUF_SIZE] = {

};


void ICACHE_FLASH_ATTR ssd1306_draw_pixel(int x, int y, uint8_t color){

	if ((x < 0) || (x >= SSD1306_WIDTH) || (y < 0) || (y >= SSD1306_HEIGHT))return;

	if(color) ssd1306_bufor[x+ (y/8)*SSD1306_WIDTH] |= (1<<(y%8));
	else ssd1306_bufor[x+ (y/8)*SSD1306_WIDTH] &= ~(1<<(y%8));
}
/*
void reverse(char* begin, char* end) {
    char *is = begin;
    char *ie = end - 1;
    while(is < ie) {
        char tmp = *ie;
        *ie = *is;
        *is = tmp;
        ++is;
        --ie;
    }
}
*/
char* itoa(int value, char* result, int base) {
    if(base < 2 || base > 16) {
        *result = 0;
        return result;
    }

    char* out = result;
    int quotient = abs(value);

    do {
        const int tmp = quotient / base;
        *out = "0123456789abcdef"[quotient - (tmp * base)];
        ++out;
        quotient = tmp;
    } while(quotient);

    // Apply negative sign
    if(value < 0)
        *out++ = '-';

    reverse(result, out);
    *out = 0;
    return result;
}

void ICACHE_FLASH_ATTR ssd1306_position_xy(int x, int y) {
	cursor_x = x;
	cursor_y = y;

}
void ICACHE_FLASH_ATTR ssd1306_put_int(int data, uint8_t txt_size, uint8_t color, uint8_t bg) {
	char buf[16];
	ssd1306_puts(itoa(data, buf, 10), txt_size, color, bg);
}


void ICACHE_FLASH_ATTR ssd1306_puts(char * str, uint8_t txt_size, uint8_t color, uint8_t bg) {

	while( *str){
	  ssd1306_drawChar( cursor_x, cursor_y, *str++, color, bg, txt_size);
	  cursor_x += txt_size*6;
	}
	ssd1306_display();

}

void ICACHE_FLASH_ATTR ssd1306_drawChar(int x, int y, char c, uint8_t color, uint8_t bg, uint8_t size) {

        if((x >= SSD1306_WIDTH)            || // Clip right
           (y >= SSD1306_HEIGHT)           || // Clip bottom
           ((x + 6 * size - 1) < 0) || // Clip left
           ((y + 8 * size - 1) < 0))   // Clip top
            return;

        for(int8_t i=0; i<5; i++ ) { // Char bitmap = 5 columns
            uint8_t line = pgm_read_byte(&font[c * 5 + i]);
            for(int8_t j=0; j<8; j++, line >>= 1) {
                if(line & 1) {
                    if(size == 1)
                        ssd1306_draw_pixel(x+i, y+j, color);
                    else
                        ssd1306_fill_rect(x+i*size, y+j*size, size, size, color);
                } else if(bg != color) {
                    if(size == 1)
                       ssd1306_draw_pixel(x+i, y+j, bg);
                    else
                        ssd1306_fill_rect(x+i*size, y+j*size, size, size, bg);
                }
            }
        }
/*
        if(bg != color) { // If opaque, draw vertical line for last column
            if(size == 1) ssd1306_drawFastVLine(x+5, y, 8, bg);
            else          ssd1306_fill_rect(x+5*size, y, size, 8*size, bg);
        }
*/
}

void ICACHE_FLASH_ATTR ssd1306_fill_rect(int x, int y,  int w, int h, uint8_t color) {

    for (int16_t i=x; i<x+w; i++) {
        ssd1306_drawFastVLine(i, y, h, color);
    }
}

void ICACHE_FLASH_ATTR ssd1306_drawFastVLine(int x, int y,  int h, uint8_t color) {
	ssd1306_drawLine(x, y, x, y+h-1, color);
}

// Bresenham's algorithm - thx wikpedia
void ICACHE_FLASH_ATTR ssd1306_drawLine(int x0, int y0, int x1, int y1, uint8_t color) {
    int16_t steep = abs(y1 - y0) > abs(x1 - x0);
    if (steep) {
        _swap_int16_t(x0, y0);
        _swap_int16_t(x1, y1);
    }

    if (x0 > x1) {
        _swap_int16_t(x0, x1);
        _swap_int16_t(y0, y1);
    }

    int16_t dx, dy;
    dx = x1 - x0;
    dy = abs(y1 - y0);

    int16_t err = dx / 2;
    int16_t ystep;

    if (y0 < y1) {
        ystep = 1;
    } else {
        ystep = -1;
    }

    for (; x0<=x1; x0++) {
        if (steep) {
            ssd1306_draw_pixel(y0, x0, color);
        } else {
            ssd1306_draw_pixel(x0, y0, color);
        }
        err -= dy;
        if (err < 0) {
            y0 += ystep;
            err += dx;
        }
    }
}
void ICACHE_FLASH_ATTR ssd1306_drawBitmap(int x, int y, const uint8_t bitmap[], int w, int h, uint8_t color) {

    int16_t byteWidth = (w + 7) / 8; // Bitmap scanline pad = whole byte
    uint8_t byte = 0;

    
    for(int16_t j=0; j<h; j++, y++) {
        for(int16_t i=0; i<w; i++) {
            if(i & 7) byte <<= 1;
            else      byte   = pgm_read_byte(&bitmap[j * byteWidth + i / 8]);
            if(byte & 0x80) ssd1306_draw_pixel(x+i, y, color);
        }
    }
   ssd1306_display();
}


#endif

