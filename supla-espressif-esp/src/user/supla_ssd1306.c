


#include <os_type.h>
#include <osapi.h>
#include <eagle_soc.h>
#include <stdlib.h>


#include "supla_i2c.h"
#include "supla_ssd1306.h"
#include "supla_w1.h"

#include "supla_gfx.h"

#ifdef OLED_DISPLAY

void ICACHE_FLASH_ATTR ssd1306_command(uint8_t cmd) {

	uint8_t control = 0x00;   // Co = 0, D/C = 0
	i2c_start();
   	i2c_write(OLED_I2C_ADDRESS);
   	i2c_write(control);
    	i2c_write(cmd);
	i2c_stop();
}
/*
void ssd1306_data(uint8_t dat) {
	uint8_t control = 0x40;   // Co = 0, D/C = 0
	i2c_start();
   	i2c_write(OLED_I2C_ADDRESS);
   	i2c_write(control);
    	i2c_write(dat);
	i2c_stop();

}
*/
void ICACHE_FLASH_ATTR ssd1306_display(void){

	ssd1306_command(SSD1306_SETLOWCOLUMN | 0x0);
	ssd1306_command(SSD1306_SETHIGHCOLUMN | 0x0);
	ssd1306_command(SSD1306_SETSTARTLINE | 0x0);

	i2c_write_buf(OLED_I2C_ADDRESS, 0x40, BUF_SIZE, ssd1306_bufor);
}

void ICACHE_FLASH_ATTR ssd1306_init(uint8_t vccstate, uint8_t refresh){

	i2c_init();
	os_delay_us(10000);

  // Init sequence
  ssd1306_command(SSD1306_DISPLAYOFF);                    // 0xAE
  ssd1306_command(SSD1306_SETDISPLAYCLOCKDIV);            // 0xD5
  ssd1306_command(refresh);                                  // the suggested ratio 0x80

  ssd1306_command(SSD1306_SETMULTIPLEX);                  // 0xA8
  ssd1306_command(SSD1306_HEIGHT - 1);

  ssd1306_command(SSD1306_SETDISPLAYOFFSET);              // 0xD3
  ssd1306_command(0x0);                                   // no offset
  ssd1306_command(SSD1306_SETSTARTLINE | 0x0);            // line #0
  ssd1306_command(SSD1306_CHARGEPUMP);                    // 0x8D
  if (vccstate == SSD1306_EXTERNALVCC)
    { ssd1306_command(0x10); }
  else
    { ssd1306_command(0x14); }
  ssd1306_command(SSD1306_MEMORYMODE);                    // 0x20
  ssd1306_command(0x00);                                  // 0x0 act like ks0108
  ssd1306_command(SSD1306_SEGREMAP | 0x1);
  ssd1306_command(SSD1306_COMSCANDEC);

 #if defined SSD1306_128_32
  ssd1306_command(SSD1306_SETCOMPINS);                    // 0xDA
  ssd1306_command(0x02);
  ssd1306_command(SSD1306_SETCONTRAST);                   // 0x81
  ssd1306_command(0x8F);

#elif defined SSD1306_128_64
  ssd1306_command(SSD1306_SETCOMPINS);                    // 0xDA
  ssd1306_command(0x12);
  ssd1306_command(SSD1306_SETCONTRAST);                   // 0x81
  if (vccstate == SSD1306_EXTERNALVCC)
    { ssd1306_command(0x9F); }
  else
    { ssd1306_command(0xCF); }

#elif defined SSD1306_96_16
  ssd1306_command(SSD1306_SETCOMPINS);                    // 0xDA
  ssd1306_command(0x2);   //ada x12
  ssd1306_command(SSD1306_SETCONTRAST);                   // 0x81
  if (vccstate == SSD1306_EXTERNALVCC)
    { ssd1306_command(0x10); }
  else
    { ssd1306_command(0xAF); }

#endif

  ssd1306_command(SSD1306_SETPRECHARGE);                  // 0xd9
  if (vccstate == SSD1306_EXTERNALVCC)
    { ssd1306_command(0x22); }
  else
    { ssd1306_command(0xF1); }
  ssd1306_command(SSD1306_SETVCOMDETECT);                 // 0xDB
  ssd1306_command(0x40);
  ssd1306_command(SSD1306_DISPLAYALLON_RESUME);           // 0xA4
  ssd1306_command(SSD1306_NORMALDISPLAY);                 // 0xA6

  ssd1306_command(SSD1306_DEACTIVATE_SCROLL);

  ssd1306_command(SSD1306_DISPLAYON);//--turn on oled panel

//	ssd1306_display();

}
#endif
