#ifndef _SUPLA_I2C_H_
#define _SUPLA_I2C_H_

#include "supla_esp.h"

#ifdef OLED_DISPLAY
/**
 * @name Pin selection and configuration block
 * D1 miniNodemcu board
 * 
 * A0 - A0
 * D0 - GPIO16
 * D1 - GPIO5
 * D2 - GPIO4
 * D3 - GPIO0
 * D4 - GPIO2
 * D5 - GPIO14
 * D6 - GPIO12
 * D7 - GPIO13
 * D8 - GPIO15
 */

#define SDA	4
#define SCL	5


#if SDA == 5
	//! @brief GPIO MUX for SDA pin
	#define SDA_MUX  PERIPHS_IO_MUX_GPIO5_U
	//! @brief GPIO FUNC for SDA pin
	#define SDA_FUNC FUNC_GPIO5
	//! @brief GPIO pin location for SDA pin
	#define SDA_PIN  5
	//! @brief GPIO bit location for SDA pin
	#define SDA_BIT  BIT5
#elif SDA == 4
	//! @brief GPIO MUX for SDA pin
	#define SDA_MUX  PERIPHS_IO_MUX_GPIO4_U
	//! @brief GPIO FUNC for SDA pin
	#define SDA_FUNC FUNC_GPIO4
	//! @brief GPIO pin location for SDA pin
	#define SDA_PIN  4
	//! @brief GPIO bit location for SDA pin
	#define SDA_BIT  BIT4
#endif

#if SCL == 4
	//! @brief GPIO MUX for SCL pin
	#define SCL_MUX  PERIPHS_IO_MUX_GPIO4_U
	//! @brief GPIO FUNC for SCL pin
	#define SCL_FUNC FUNC_GPIO4
	//! @brief GPIO pin location for SCL pin
	#define SCL_PIN  4
	//! @brief GPIO bit location for SCL pin
	#define SCL_BIT  BIT4
#elif SCL == 5
	//! @brief GPIO MUX for SCL pin
	#define SCL_MUX  PERIPHS_IO_MUX_GPIO5_U
	//! @brief GPIO FUNC for SCL pin
	#define SCL_FUNC FUNC_GPIO5
	//! @brief GPIO pin location for SCL pin
	#define SCL_PIN  5
	//! @brief GPIO bit location for SCL pin
	#define SCL_BIT  BIT5
#endif

void ICACHE_FLASH_ATTR i2c_init(void);
bool ICACHE_FLASH_ATTR i2c_start(void);
void ICACHE_FLASH_ATTR i2c_stop(void);
bool ICACHE_FLASH_ATTR i2c_write(uint8_t data);
uint8_t ICACHE_FLASH_ATTR i2c_read(void);
void ICACHE_FLASH_ATTR i2c_set_ack(bool ack);
void ICACHE_FLASH_ATTR i2c_write_buf(uint8_t SLA, uint8_t adr, uint16_t len, uint8_t *buf);

#endif

#endif /* _SUPLA_I2C_H_ */
