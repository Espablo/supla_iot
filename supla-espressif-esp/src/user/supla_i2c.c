

#include <os_type.h>
#include <osapi.h>
#include <eagle_soc.h>
#include <stdlib.h>

#include "supla_i2c.h"
#include "supla_ssd1306.h"
#include "supla_w1.h"


#ifdef OLED_DISPLAY


//! Delay amount in-between bits, with os_delay_us(1) I get ~300kHz I2C clock
#define _DELAY os_delay_us(1)

/** @} */



#define SDA_HI GPIO_REG_WRITE(GPIO_OUT_W1TS_ADDRESS, SDA_BIT)
#define SDA_LO GPIO_REG_WRITE(GPIO_OUT_W1TC_ADDRESS, SDA_BIT)

#define SCL_HI GPIO_REG_WRITE(GPIO_OUT_W1TS_ADDRESS, SCL_BIT)
#define SCL_LO GPIO_REG_WRITE(GPIO_OUT_W1TC_ADDRESS, SCL_BIT)

#define _SDAX ((GPIO_REG_READ(GPIO_IN_ADDRESS) >> SDA_PIN) & 0x01)
#define _SCLX ((GPIO_REG_READ(GPIO_IN_ADDRESS) >> SCL_PIN) & 0x01)

// this is found in gpio.h from IoT SDK but not in FreeRTOS SDK
#ifndef GPIO_PIN_ADDR
#define GPIO_PIN_ADDR(i) (GPIO_PIN0_ADDRESS + i*4)
#endif


void ICACHE_FLASH_ATTR i2c_init(void)
{
    // MUX selection
    PIN_FUNC_SELECT(SDA_MUX, SDA_FUNC);
    PIN_FUNC_SELECT(SCL_MUX, SCL_FUNC);

    // Set SDA as OD output
    GPIO_REG_WRITE
    (
        GPIO_PIN_ADDR(GPIO_ID_PIN(SDA_PIN)),
        GPIO_REG_READ(GPIO_PIN_ADDR(GPIO_ID_PIN(SDA_PIN))) | GPIO_PIN_PAD_DRIVER_SET(GPIO_PAD_DRIVER_ENABLE)
    );
    // Set SCK as OD output
    GPIO_REG_WRITE
    (
        GPIO_PIN_ADDR(GPIO_ID_PIN(SCL_PIN)),
        GPIO_REG_READ(GPIO_PIN_ADDR(GPIO_ID_PIN(SCL_PIN))) | GPIO_PIN_PAD_DRIVER_SET(GPIO_PAD_DRIVER_ENABLE)
    );
    // Set idle bus high
    SDA_HI;
    SDA_LO;
    // Set both output
    GPIO_REG_WRITE(GPIO_ENABLE_ADDRESS, GPIO_REG_READ(GPIO_ENABLE_ADDRESS) | SDA_BIT | SCL_BIT);
    return;
}


bool ICACHE_FLASH_ATTR i2c_start(void)
{
    SDA_HI;
    SCL_HI;
    _DELAY;
    if (_SDAX == 0) return false; // Bus busy
    SDA_LO;
    _DELAY;
    SCL_LO;
    return true;
}

void ICACHE_FLASH_ATTR i2c_stop(void)
{
    SDA_LO;
    SCL_HI;
    _DELAY;
    while (_SCLX == 0); // clock stretching
    SDA_HI;
    _DELAY;
}

// return: true - ACK; false - NACK
bool ICACHE_FLASH_ATTR i2c_write(uint8_t data)
{
    uint8_t ibit;
    bool ret;

    for (ibit = 0; ibit < 8; ++ibit)
    {
        if (data & 0x80)
            SDA_HI;
        else
            SDA_LO;
        _DELAY;
        SCL_HI;
        _DELAY;
        data = data << 1;
        SCL_LO;
    }
    SDA_HI;
    _DELAY;
    SCL_HI;
    _DELAY;
    ret = (_SDAX == 0);
    SCL_LO;
    _DELAY;

    return ret;
}

uint8_t ICACHE_FLASH_ATTR i2c_read(void)
{
    uint8_t data = 0;
    uint8_t ibit = 8;

    SDA_HI;
    while (ibit--)
    {
        data = data << 1;
        SCL_LO;
        _DELAY;
        SCL_HI;
        _DELAY;
        if (_SDAX)
            data = data | 0x01;
    }
    SCL_LO;

    return data;
}

void ICACHE_FLASH_ATTR i2c_set_ack(bool ack)
{
    SCL_LO;
    if (ack)
        SDA_LO;  // ACK
    else
        SDA_HI;  // NACK
    _DELAY;
    // Send clock
    SCL_HI;
    _DELAY;
    SCL_LO;
    _DELAY;
    // ACK end
    SDA_HI;
}

void ICACHE_FLASH_ATTR i2c_write_buf(uint8_t SLA, uint8_t adr, uint16_t len, uint8_t *buf){

	i2c_start();
	i2c_write(SLA);
	i2c_write(adr);
	while (len--) i2c_write(*buf++);
	i2c_stop();
}

#endif




