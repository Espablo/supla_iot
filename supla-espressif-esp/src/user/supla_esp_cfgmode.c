/*
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
// #include <os_type.h>
#include "stdlib_noniso.h"


#include <string.h>
#include <stdio.h>
#include <stdlib.h>


#include <ip_addr.h>
#include <user_interface.h>
#include <espconn.h>
#include <spi_flash.h>
#include <osapi.h>
#include <mem.h>

#include "supla_esp.h"
#include "supla_esp_cfg.h"
#include "supla_esp_cfgmode.h"
#include "supla_esp_devconn.h"
#include "supla_esp_gpio.h"

#include "supla-dev/log.h"

#include "supla_html_inCan.h"
#include "supla_temperatura.h"

#define TYPE_UNKNOWN  0
#define TYPE_GET      1
#define TYPE_POST     2

#define STEP_TYPE        0
#define STEP_GET         2
#define STEP_POST        3
#define STEP_PARSE_VARS  4
#define STEP_DONE        10

#define VAR_NONE         0
#define VAR_SID          1
#define VAR_WPW          2
#define VAR_SVR          3
#define VAR_LID          4
#define VAR_PWD          5
#define VAR_CFGBTN       6
#define VAR_BTN1         7
#define VAR_BTN2         8
#define VAR_ICF          9
#define VAR_LED          10
#define VAR_UPD          11
#define VAR_RBT          12
#define VAR_EML          20
#define VAR_USD          21
#define VAR_TRG          22
//****************************************dla modulu inCan****************************************
#if defined(INCAN_TYPE_SELECTION) || defined(INCAN_EASY_TYPE_SELECTION) || defined(MULTIBOARD_TYPE_SELECTION)
#define VAR_MULTIBOARD_TYPE     100
#define VAR_BTN3         101
#define VAR_BTN4         102
#define VAR_BTN5         103
#define VAR_SNR1                104
#define VAR_SNR2                105
#define VAR_THERM               106
#define VAR_ROLL                107
#define VAR_RELAY1_GPIO         108
#define VAR_RELAY2_GPIO         109
#define VAR_BUTTON1_GPIO        110
#define VAR_BUTTON2_GPIO        111
#define VAR_SENSOR1_GPIO        112
#define VAR_SENSOR2_GPIO        113
#define VAR_THERMOMETER_GPIO    114
#define VAR_LED_CFG_GPIO        115
#define VAR_BTN_CFG_GPIO        116
#define VAR_OLED                117
#define VAR_STRONA              118
// #define VAR_PIN_CONFIGURABLE    225

#define VAR_HOSTNAME            119
#define VAR_PIN_CONFIGURABLE0    120
#define VAR_PIN_CONFIGURABLE1    121
#define VAR_PIN_CONFIGURABLE2    122
#define VAR_PIN_CONFIGURABLE3    123
#define VAR_PIN_CONFIGURABLE4    124
#define VAR_PIN_CONFIGURABLE5    125
#define VAR_PIN_CONFIGURABLE6    126

#define VAR_RELAY0_LEVEL        130
#define VAR_RELAY1_LEVEL        131
#define VAR_RELAY2_LEVEL        132
#define VAR_RELAY3_LEVEL        133
#define VAR_RELAY4_LEVEL        134
#define VAR_RELAY5_LEVEL        135
#define VAR_RELAY6_LEVEL        136
#define VAR_RELAY7_LEVEL        137
#define VAR_RELAY8_LEVEL        138
#define VAR_RELAY9_LEVEL        139
#define VAR_RELAY0_FLAG         140
#define VAR_RELAY1_FLAG         141
#define VAR_RELAY2_FLAG         142
#define VAR_RELAY3_FLAG                 143
#define VAR_RELAY4_FLAG                 144
#define VAR_RELAY5_FLAG                 145
#define VAR_RELAY6_FLAG                 146
#define VAR_RELAY7_FLAG                 147
#define VAR_RELAY8_FLAG                 148
#define VAR_RELAY9_FLAG                 149


#endif
//****************************************KONIEC dla modulu inCan*********************************

typedef struct {

	char step;
	char type;
	char current_var;

	short matched;

	char *pbuff;
	int buff_size;
	int offset;
	char intval[12];

}TrivialHttpParserVars;


unsigned int supla_esp_cfgmode_entertime = 0;
ETSTimer http_response_timer;
uint32 http_response_buff_len = NULL;
uint32 http_response_pos = 0;
char *http_response_buff = 0;

char *start_settings = 0;
char *board_name = 0;
char *thermometer_name = 0;
char *button_type = 0;
char *relay_level = 0;
char *led_select = 0;
char *button_cfg_type = 0;
char *pin_type = 0;


void ICACHE_FLASH_ATTR
supla_esp_http_send_response_cb(struct espconn *pespconn) {

	uint32 len = http_response_buff_len - http_response_pos;
	if ( len > 200 ) {
		len = 200;
	}

	if ( len <= 0 ) {
		os_timer_disarm(&http_response_timer);
		return;
	}

	if ( 0 == espconn_sent(pespconn, (unsigned char*)&http_response_buff[http_response_pos], len) ) {
		http_response_pos+=len;
	}
}


void ICACHE_FLASH_ATTR
supla_esp_http_send_response(struct espconn *pespconn, const char *code, const char *html) {

	os_timer_disarm(&http_response_timer);

	if ( http_response_buff ) {
		free(http_response_buff);
		http_response_buff = NULL;
	}

	int html_len = html != NULL ? strlen(html) : 0;
	char response[] = "HTTP/1.1 %s\r\nAccept-Ranges: bytes\r\nContent-Length: %i\r\nContent-Type: text/html; charset=UTF-8\r\nConnection: close\r\n\r\n";

	http_response_buff_len = strlen(code)+strlen(response)+html_len+1;
	http_response_buff = os_malloc(http_response_buff_len);
	http_response_pos = 0;
	ets_snprintf(http_response_buff, http_response_buff_len, response, code, html_len);

	if ( html_len > 0 ) {
		memcpy(&http_response_buff[http_response_buff_len-html_len-1], html, html_len);
	}

	http_response_buff[http_response_buff_len-1] = 0;

	http_response_buff_len = strlen(http_response_buff);

	os_timer_setfn(&http_response_timer, (os_timer_func_t *)supla_esp_http_send_response_cb, pespconn);
	os_timer_arm (&http_response_timer, 10, true);
}

void ICACHE_FLASH_ATTR
supla_esp_http_ok(struct espconn *pespconn, const char *html) {
	supla_esp_http_send_response(pespconn, "200 OK", html);
}

void ICACHE_FLASH_ATTR
supla_esp_http_404(struct espconn *pespconn) {
	supla_esp_http_send_response(pespconn, "404 Not Found", "Not Found");
}

void ICACHE_FLASH_ATTR
supla_esp_http_error(struct espconn *pespconn) {
	supla_esp_http_send_response(pespconn, "500 Internal Server Error", "Error");
}

int ICACHE_FLASH_ATTR
Power(int x, int y) {

	int result = 1;
	while (y)
	{
		if (y & 1)
			result *= x;

		y >>= 1;
		x *= x;
	}

	return result;
}

int ICACHE_FLASH_ATTR
HexToInt(char *str, int len) {

	int a, n, p;
	int result = 0;

	if ( len%2 != 0 )
		return 0;

	p = len - 1;

	for(a=0; a<len; a++) {
		n = 0;

		if ( str[a] >= 'A' && str[a] <= 'F' )
			n = str[a]-55;
		else if ( str[a] >= 'a' && str[a] <= 'f' )
			n = str[a]-87;
		else if ( str[a] >= '0' && str[a] <= '9' )
			n = str[a]-48;


		result+=Power(16, p)*n;
		p--;
	}

	return result;


};

void ICACHE_FLASH_ATTR
supla_esp_parse_request(TrivialHttpParserVars *pVars, char *pdata, unsigned short len, SuplaEspCfg *cfg, char *reboot) {

	if ( len == 0 )
		return;

	int a, p;

	//for(a=0;a<len;a++)
	//	printf("%c", pdata[a]);

	if ( pVars->step == STEP_TYPE ) {

		char get[] = "GET";
		char post[] = "POST";
		char url[] = " / HTTP";

		if ( len >= 3
		     && memcmp(pdata, get, 3) == 0
		     && len >= 10
		     && memcmp(&pdata[3], url, 7) == 0 ) {

			pVars->step = STEP_GET;
			pVars->type = TYPE_GET;

		} else if ( len >= 4
		            && memcmp(pdata, post, 4) == 0
		            && len >= 11
		            && memcmp(&pdata[4], url, 7) == 0 )  {

			pVars->step = STEP_POST;
			pVars->type = TYPE_POST;
		}

	}

	p = 0;

	if ( pVars->step == STEP_POST ) {

		char header_end[4] = { '\r', '\n', '\r', '\n' };

		for(a=p; a<len; a++) {

			if ( len-a >= 4
			     && memcmp(header_end, &pdata[a], 4) == 0 ) {

				pVars->step = STEP_PARSE_VARS;
				p+=3;
			}

		}

	}

	if ( pVars->step == STEP_PARSE_VARS ) {

		for(a=p; a<len; a++) {

			if ( pVars->current_var == VAR_NONE ) {

				char sid[3] = { 's', 'i', 'd' };
				char wpw[3] = { 'w', 'p', 'w' };
				char svr[3] = { 's', 'v', 'r' };
				char lid[3] = { 'l', 'i', 'd' };
				char pwd[3] = { 'p', 'w', 'd' };
				char btncfg[3] = { 'c', 'f', 'g' };
				char btn1[3] = { 'b', 't', '1' };
				char btn2[3] = { 'b', 't', '2' };
				char icf[3] = { 'i', 'c', 'f' };
				char led[3] = { 'l', 'e', 'd' };
				char upd[3] = { 'u', 'p', 'd' };
				char rbt[3] = { 'r', 'b', 't' };
				char eml[3] = { 'e', 'm', 'l' };
				char usd[3] = { 'u', 's', 'd' };
				char trg[3] = { 't', 'r', 'g' };
//****************************************dla modulu inCan****************************************
			#if defined(INCAN_TYPE_SELECTION) || defined(INCAN_EASY_TYPE_SELECTION)  || defined(MULTIBOARD_TYPE_SELECTION)
				char snr1[3] = { 's', 'r', '1' };
				char snr2[3] = { 's', 'r', '2' };
				char therm[3] = { 't', 'r', 'm' };
				char roll[3] = { 'r', 'o', 'l' };

				char rel1_gpio[3] = {'r', 'g', '1'};
				char rel2_gpio[3] = {'r', 'g', '2'};
				char btn1_gpio[3] = {'b', 'g', '1'};
				char btn2_gpio[3] = {'b', 'g', '2'};
				char snr1_gpio[3] = {'s', 'g', '1'};
				char snr2_gpio[3] = {'s', 'g', '2'};
				char therm_gpio[3] = { 't', 'g', 'p' };
				char led_gpio[3] = { 'l', 'g', 'p' };
				char btncfg_gpio[3] = { 'b', 'g', 'p' };
				char strona[3] = { 's', 't', 'r' };
				char rel0_level[3] = {'r', 'l', '0'};
				char rel1_level[3] = {'r', 'l', '1'};
				char rel2_level[3] = {'r', 'l', '2'};
				char rel3_level[3] = {'r', 'l', '3'};
				char rel4_level[3] = {'r', 'l', '4'};
				char rel5_level[3] = {'r', 'l', '5'};
				char rel6_level[3] = {'r', 'l', '6'};
				char rel7_level[3] = {'r', 'l', '7'};
				char rel8_level[3] = {'r', 'l', '8'};
				char rel9_level[3] = {'r', 'l', '9'};
				char rel0_flag[3] = {'f', 'l', '0'};
				char rel1_flag[3] = {'f', 'l', '1'};
				char rel2_flag[3] = {'f', 'l', '2'};
				char rel3_flag[3] = {'f', 'l', '3'};
				char rel4_flag[3] = {'f', 'l', '4'};
				char rel5_flag[3] = {'f', 'l', '5'};
				char rel6_flag[3] = {'f', 'l', '6'};
				char rel7_flag[3] = {'f', 'l', '7'};
				char rel8_flag[3] = {'f', 'l', '8'};
				char rel9_flag[3] = {'f', 'l', '9'};
				char multiboard_type[3] = { 'm', 'l', 't' };

				char btn3[3] = { 'b', 't', '3' };
				char btn4[3] = { 'b', 't', '4' };
				char btn5[3] = { 'b', 't', '5' };
				char hname[3] = { 'h', 's', 'n' };
				char pin_cfg0[3] = { 'p', 'c', '0' };
				char pin_cfg1[3] = { 'p', 'c', '1' };
				char pin_cfg2[3] = { 'p', 'c', '2' };
				char pin_cfg3[3] = { 'p', 'c', '3' };
				char pin_cfg4[3] = { 'p', 'c', '4' };
				char pin_cfg5[3] = { 'p', 'c', '5' };
				char pin_cfg6[3] = { 'p', 'c', '6' };


			#endif
//****************************************KONIEC dla modulu inCan*********************************

				if ( len-a >= 4
				     && pdata[a+3] == '=' ) {

					if ( memcmp(sid, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_SID;
						pVars->buff_size = WIFI_SSID_MAXSIZE;
						pVars->pbuff = cfg->WIFI_SSID;

					} else if ( memcmp(wpw, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_WPW;
						pVars->buff_size = WIFI_PWD_MAXSIZE;
						pVars->pbuff = cfg->WIFI_PWD;

					} else if ( memcmp(svr, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_SVR;
						pVars->buff_size = SERVER_MAXSIZE;
						pVars->pbuff = cfg->Server;

					} else if ( memcmp(lid, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_LID;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(pwd, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_PWD;
						pVars->buff_size = SUPLA_LOCATION_PWD_MAXSIZE;
						pVars->pbuff = cfg->LocationPwd;

					} else if ( memcmp(btncfg, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_CFGBTN;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(btn1, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_BTN1;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(btn2, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_BTN2;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(btn3, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_BTN3;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(btn4, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_BTN4;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(btn5, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_BTN5;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(icf, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_ICF;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(led, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_LED;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(upd, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_UPD;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rbt, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RBT;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(eml, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_EML;
						pVars->buff_size = SUPLA_EMAIL_MAXSIZE;
						pVars->pbuff = cfg->Email;

					} else if ( memcmp(usd, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_USD;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(trg, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_TRG;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

//****************************************dla modulu inCan****************************************
			#if defined(INCAN_TYPE_SELECTION) || defined(INCAN_EASY_TYPE_SELECTION)  || defined(MULTIBOARD_TYPE_SELECTION)
					} else if ( memcmp(snr1, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_SNR1;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(snr2, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_SNR2;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(therm, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_THERM;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(roll, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_ROLL;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;


					} else if ( memcmp(rel1_gpio, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY1_GPIO;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel2_gpio, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY2_GPIO;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(btn1_gpio, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_BUTTON1_GPIO;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(btn2_gpio, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_BUTTON2_GPIO;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(snr1_gpio, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_SENSOR1_GPIO;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(snr2_gpio, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_SENSOR2_GPIO;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(therm_gpio, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_THERMOMETER_GPIO;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(led_gpio, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_LED_CFG_GPIO;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(btncfg_gpio, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_BTN_CFG_GPIO;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(strona, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_STRONA;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel0_level, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY0_LEVEL;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel1_level, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY1_LEVEL;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel2_level, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY2_LEVEL;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel3_level, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY3_LEVEL;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel4_level, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY4_LEVEL;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel5_level, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY5_LEVEL;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel6_level, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY6_LEVEL;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel7_level, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY7_LEVEL;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel8_level, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY8_LEVEL;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel9_level, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY9_LEVEL;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel0_flag, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY0_FLAG;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel1_flag, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY1_FLAG;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel2_flag, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY2_FLAG;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel3_flag, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY3_FLAG;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel4_flag, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY4_FLAG;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel5_flag, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY5_FLAG;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel6_flag, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY6_FLAG;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel7_flag, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY7_FLAG;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel8_flag, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY8_FLAG;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(rel9_flag, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_RELAY9_FLAG;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;
						// } else if ( memcmp(btncfg_type, &pdata[a], 3) == 0 ) {
						//
						//      pVars->current_var = VAR_BTN_CFG_TYPE;
						//      pVars->buff_size = 12;
						//      pVars->pbuff = pVars->intval;

					} else if ( memcmp(multiboard_type, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_MULTIBOARD_TYPE;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(hname, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_HOSTNAME;
						pVars->buff_size = SUPLA_HOSTNAME_MAXSIZE;
						pVars->pbuff = cfg->HostName;

					} else if ( memcmp(pin_cfg0, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_PIN_CONFIGURABLE0;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(pin_cfg1, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_PIN_CONFIGURABLE1;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(pin_cfg2, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_PIN_CONFIGURABLE2;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(pin_cfg3, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_PIN_CONFIGURABLE3;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(pin_cfg4, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_PIN_CONFIGURABLE4;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(pin_cfg5, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_PIN_CONFIGURABLE5;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

					} else if ( memcmp(pin_cfg6, &pdata[a], 3) == 0 ) {

						pVars->current_var = VAR_PIN_CONFIGURABLE6;
						pVars->buff_size = 12;
						pVars->pbuff = pVars->intval;

						#endif
//****************************************KONIEC dla modulu inCan*********************************

					}

					a+=4;
					pVars->offset = 0;
				}

			}

			if ( pVars->current_var != VAR_NONE ) {

				if ( pVars->offset < pVars->buff_size
				     && a < len
				     && pdata[a] != '&' ) {

					if ( pdata[a] == '%' && a+2 < len ) {

						pVars->pbuff[pVars->offset] = HexToInt(&pdata[a+1], 2);
						pVars->offset++;
						a+=2;

					} else if ( pdata[a] == '+' ) {

						pVars->pbuff[pVars->offset] = ' ';
						pVars->offset++;

					} else {

						pVars->pbuff[pVars->offset] = pdata[a];
						pVars->offset++;

					}

				}


				if ( pVars->offset >= pVars->buff_size
				     || a >= len-1
				     || pdata[a] == '&'  ) {

					if ( pVars->offset < pVars->buff_size )
						pVars->pbuff[pVars->offset] = 0;
					else
						pVars->pbuff[pVars->buff_size-1] = 0;


					if ( pVars->current_var == VAR_LID ) {

						cfg->LocationID = 0;

						short s=0;
						while(pVars->intval[s]!=0) {

							if ( pVars->intval[s] >= '0' && pVars->intval[s] <= '9' ) {
								cfg->LocationID = cfg->LocationID*10 + pVars->intval[s] - '0';
							}

							s++;
						}
					} else if ( pVars->current_var == VAR_CFGBTN ) {

						cfg->CfgButtonType = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_BTN1 ) {

						cfg->Button1Type = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_BTN2 ) {

						cfg->Button2Type = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_BTN3 ) {

						cfg->Button3Type = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_BTN4 ) {

						cfg->Button4Type = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_BTN5 ) {

						cfg->Button5Type = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_ICF ) {

						cfg->InputCfgTriggerOff = (pVars->intval[0] - '0') == 1 ? 1 : 0;

					} else if ( pVars->current_var == VAR_LED ) {

						cfg->StatusLedOff = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_UPD ) {

						cfg->FirmwareUpdate = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RBT ) {

						if ( reboot != NULL )
							*reboot = pVars->intval[0] - '0';
					} else if ( pVars->current_var == VAR_USD ) {

						cfg->UpsideDown = (pVars->intval[0] - '0') == 1 ? 1 : 0;

					} else if ( pVars->current_var == VAR_TRG ) {

						cfg->Trigger = pVars->intval[0] - '0';

//****************************************dla modulu inCan****************************************
			#if defined(INCAN_TYPE_SELECTION) || defined(INCAN_EASY_TYPE_SELECTION) || defined(MULTIBOARD_TYPE_SELECTION)

					} else if ( pVars->current_var == VAR_SNR1 ) {

						cfg->Sensor1Type = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_SNR2 ) {

						cfg->Sensor2Type = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_THERM ) {

						cfg->ThermometerType = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_ROLL ) {

						cfg->RollerShutter = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY1_GPIO ) {

						cfg->Relay1_GPIO = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY2_GPIO ) {

						cfg->Relay2_GPIO = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_BUTTON1_GPIO ) {

						cfg->Button1_GPIO = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_BUTTON2_GPIO ) {

						cfg->Button2_GPIO = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_SENSOR1_GPIO ) {

						cfg->Sensor1_GPIO = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_SENSOR2_GPIO ) {

						cfg->Sensor2_GPIO = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_THERMOMETER_GPIO ) {

						cfg->Thermometer_GPIO = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_LED_CFG_GPIO ) {

						cfg->LedConfig_GPIO = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_BTN_CFG_GPIO ) {

						cfg->BtnConfig_GPIO = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_STRONA ) {

						cfg->StronaHTML = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY0_LEVEL ) {

						cfg->RelayLevel[0] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY1_LEVEL ) {

						cfg->RelayLevel[1] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY2_LEVEL ) {

						cfg->RelayLevel[2] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY3_LEVEL ) {

						cfg->RelayLevel[3] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY4_LEVEL ) {

						cfg->RelayLevel[4] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY5_LEVEL ) {

						cfg->RelayLevel[5] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY6_LEVEL ) {

						cfg->RelayLevel[6] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY7_LEVEL ) {

						cfg->RelayLevel[7] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY8_LEVEL ) {

						cfg->RelayLevel[8] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY9_LEVEL ) {

						cfg->RelayLevel[9] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY0_FLAG ) {

						cfg->RelayFlag[0] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY1_FLAG ) {

						cfg->RelayFlag[1] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY2_FLAG ) {

						cfg->RelayFlag[2] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY3_FLAG ) {

						cfg->RelayFlag[3] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY4_FLAG ) {

						cfg->RelayFlag[4] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY5_FLAG ) {

						cfg->RelayFlag[5] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY6_FLAG ) {

						cfg->RelayFlag[6] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY7_FLAG ) {

						cfg->RelayFlag[7] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY8_FLAG ) {

						cfg->RelayFlag[8] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_RELAY9_FLAG ) {

						cfg->RelayFlag[9] = pVars->intval[0] - '0';

						// } else if ( pVars->current_var == VAR_BTN_CFG_TYPE ) {
						//
						//      cfg->ButtonConfigType = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_MULTIBOARD_TYPE ) {

						cfg->BoardNames = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_PIN_CONFIGURABLE0 ) {

						cfg->PinMode_UserConfigurable[0] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_PIN_CONFIGURABLE1 ) {

						cfg->PinMode_UserConfigurable[1] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_PIN_CONFIGURABLE2 ) {

						cfg->PinMode_UserConfigurable[2] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_PIN_CONFIGURABLE3 ) {

						cfg->PinMode_UserConfigurable[3] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_PIN_CONFIGURABLE4 ) {

						cfg->PinMode_UserConfigurable[4] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_PIN_CONFIGURABLE5 ) {

						cfg->PinMode_UserConfigurable[5] = pVars->intval[0] - '0';

					} else if ( pVars->current_var == VAR_PIN_CONFIGURABLE6 ) {

						cfg->PinMode_UserConfigurable[6] = pVars->intval[0] - '0';


									#endif
//****************************************KONIEC dla modulu inCan*********************************

					}

					pVars->matched++;
					pVars->current_var = VAR_NONE;

				}

			}

		}

	}

}

//****************************************dla modulu inCanEasy****************************************
#if defined(INCAN_EASY_TYPE_SELECTION)

char ICACHE_FLASH_ATTR * gpio_selected(char select_config){
	if(select_config >= 17) select_config = select_config - 7;

	char *gpio_name;
	gpio_name =(char*)os_malloc(512);
	for (size_t i = 0; i <= 16; i++) {
		if (select_config != 6 && select_config != 7 && select_config != 8 && select_config != 11) {
			strcpy(gpio_name, "<option value=\"");
			// char *_gpio_name;
			// _gpio_name =(char*)os_malloc(i);
			// os_sprintf(_gpio_name, "%i", i);
			// strcpy(_gpio_name, i);
			strcat(gpio_name, IntToChar[i]);
			// strcat(gpio_name, (char*)i);
			if (select_config == i) {
				strcat(gpio_name, "\" selected>");
			}
			else strcat(gpio_name, "\">");
			strcat(gpio_name, Supported_GPIO[i]);
		}
	}
	return gpio_name;

	// if (select_config == PIN_GPIO_0) return "<option value=\"O\">OFF<option value=\"0\" selected>GPIO_0(D3)<option value=\"1\">GPIO_1(TX)<option value=\"2\">GPIO_2(D4)<option value=\"3\">GPIO_3(RX)<option value=\"4\">GPIO_4(D2)<option value=\"5\">GPIO_5(D1)<option value=\"9\">GPIO_9(D11)<option value=\"A\">GPIO_10(D12)<option value=\"C\">GPIO_12(D6)<option value=\"D\">GPIO_13(D7)<option value=\"E\">GPIO_14(D5)<option value=\"F\">GPIO_15(D8)<option value=\"G\">GPIO_16(D0)";
	// else if (select_config == PIN_GPIO_1) return "<option value=\"O\">OFF<option value=\"0\">GPIO_0(D3)<option value=\"1\" selected>GPIO_1(TX)<option value=\"2\">GPIO_2(D4)<option value=\"3\">GPIO_3(RX)<option value=\"4\">GPIO_4(D2)<option value=\"5\">GPIO_5(D1)<option value=\"9\">GPIO_9(D11)<option value=\"A\">GPIO_10(D12)<option value=\"C\">GPIO_12(D6)<option value=\"D\">GPIO_13(D7)<option value=\"E\">GPIO_14(D5)<option value=\"F\">GPIO_15(D8)<option value=\"G\">GPIO_16(D0)";
	// else if (select_config == PIN_GPIO_2) return "<option value=\"O\">OFF<option value=\"0\">GPIO_0(D3)<option value=\"1\">GPIO_1(TX)<option value=\"2\" selected>GPIO_2(D4)<option value=\"3\">GPIO_3(RX)<option value=\"4\">GPIO_4(D2)<option value=\"5\">GPIO_5(D1)<option value=\"9\">GPIO_9(D11)<option value=\"A\">GPIO_10(D12)<option value=\"C\">GPIO_12(D6)<option value=\"D\">GPIO_13(D7)<option value=\"E\">GPIO_14(D5)<option value=\"F\">GPIO_15(D8)<option value=\"G\">GPIO_16(D0)";
	// else if (select_config == PIN_GPIO_3) return "<option value=\"O\">OFF<option value=\"0\">GPIO_0(D3)<option value=\"1\">GPIO_1(TX)<option value=\"2\">GPIO_2(D4)<option value=\"3\" selected>GPIO_3(RX)<option value=\"4\">GPIO_4(D2)<option value=\"5\">GPIO_5(D1)<option value=\"9\">GPIO_9(D11)<option value=\"A\">GPIO_10(D12)<option value=\"C\">GPIO_12(D6)<option value=\"D\">GPIO_13(D7)<option value=\"E\">GPIO_14(D5)<option value=\"F\">GPIO_15(D8)<option value=\"G\">GPIO_16(D0)";
	// else if (select_config == PIN_GPIO_4) return "<option value=\"O\">OFF<option value=\"0\">GPIO_0(D3)<option value=\"1\">GPIO_1(TX)<option value=\"2\">GPIO_2(D4)<option value=\"3\">GPIO_3(RX)<option value=\"4\" selected>GPIO_4(D2)<option value=\"5\">GPIO_5(D1)<option value=\"9\">GPIO_9(D11)<option value=\"A\">GPIO_10(D12)<option value=\"C\">GPIO_12(D6)<option value=\"D\">GPIO_13(D7)<option value=\"E\">GPIO_14(D5)<option value=\"F\">GPIO_15(D8)<option value=\"G\">GPIO_16(D0)";
	// else if (select_config == PIN_GPIO_5) return "<option value=\"O\">OFF<option value=\"0\">GPIO_0(D3)<option value=\"1\">GPIO_1(TX)<option value=\"2\">GPIO_2(D4)<option value=\"3\">GPIO_3(RX)<option value=\"4\">GPIO_4(D2)<option value=\"5\" selected>GPIO_5(D1)<option value=\"9\">GPIO_9(D11)<option value=\"A\">GPIO_10(D12)<option value=\"C\">GPIO_12(D6)<option value=\"D\">GPIO_13(D7)<option value=\"E\">GPIO_14(D5)<option value=\"F\">GPIO_15(D8)<option value=\"G\">GPIO_16(D0)";
	// else if (select_config == PIN_GPIO_9) return "<option value=\"O\">OFF<option value=\"0\">GPIO_0(D3)<option value=\"1\">GPIO_1(TX)<option value=\"2\">GPIO_2(D4)<option value=\"3\">GPIO_3(RX)<option value=\"4\">GPIO_4(D2)<option value=\"5\">GPIO_5(D1)<option value=\"9\" selected>GPIO_9(D11)<option value=\"A\">GPIO_10(D12)<option value=\"C\">GPIO_12(D6)<option value=\"D\">GPIO_13(D7)<option value=\"E\">GPIO_14(D5)<option value=\"F\">GPIO_15(D8)<option value=\"G\">GPIO_16(D0)";
	// else if ((select_config) == PIN_GPIO_10) return "<option value=\"O\">OFF<option value=\"0\">GPIO_0(D3)<option value=\"1\">GPIO_1(TX)<option value=\"2\">GPIO_2(D4)<option value=\"3\">GPIO_3(RX)<option value=\"4\">GPIO_4(D2)<option value=\"5\">GPIO_5(D1)<option value=\"9\">GPIO_9(D11)<option value=\"A\" selected>GPIO_10(D12)<option value=\"C\">GPIO_12(D6)<option value=\"D\">GPIO_13(D7)<option value=\"E\">GPIO_14(D5)<option value=\"F\">GPIO_15(D8)<option value=\"G\">GPIO_16(D0)";
	// else if ((select_config ) == PIN_GPIO_12) return "<option value=\"O\">OFF<option value=\"0\">GPIO_0(D3)<option value=\"1\">GPIO_1(TX)<option value=\"2\">GPIO_2(D4)<option value=\"3\">GPIO_3(RX)<option value=\"4\">GPIO_4(D2)<option value=\"5\">GPIO_5(D1)<option value=\"9\">GPIO_9(D11)<option value=\"A\">GPIO_10(D12)<option value=\"C\" selected>GPIO_12(D6)<option value=\"D\">GPIO_13(D7)<option value=\"E\">GPIO_14(D5)<option value=\"F\">GPIO_15(D8)<option value=\"G\">GPIO_16(D0)";
	// else if ((select_config) == PIN_GPIO_13) return "<option value=\"O\">OFF<option value=\"0\">GPIO_0(D3)<option value=\"1\">GPIO_1(TX)<option value=\"2\">GPIO_2(D4)<option value=\"3\">GPIO_3(RX)<option value=\"4\">GPIO_4(D2)<option value=\"5\">GPIO_5(D1)<option value=\"9\">GPIO_9(D11)<option value=\"A\">GPIO_10(D12)<option value=\"C\">GPIO_12(D6)<option value=\"D\" selected>GPIO_13(D7)<option value=\"E\">GPIO_14(D5)<option value=\"F\">GPIO_15(D8)<option value=\"G\">GPIO_16(D0)";
	// else if ((select_config) == PIN_GPIO_14) return "<option value=\"O\">OFF<option value=\"0\">GPIO_0(D3)<option value=\"1\">GPIO_1(TX)<option value=\"2\">GPIO_2(D4)<option value=\"3\">GPIO_3(RX)<option value=\"4\">GPIO_4(D2)<option value=\"5\">GPIO_5(D1)<option value=\"9\">GPIO_9(D11)<option value=\"A\">GPIO_10(D12)<option value=\"C\">GPIO_12(D6)<option value=\"D\">GPIO_13(D7)<option value=\"E\" selected>GPIO_14(D5)<option value=\"F\">GPIO_15(D8)<option value=\"G\">GPIO_16(D0)";
	// else if ((select_config) == PIN_GPIO_15) return "<option value=\"O\">OFF<option value=\"0\">GPIO_0(D3)<option value=\"1\">GPIO_1(TX)<option value=\"2\">GPIO_2(D4)<option value=\"3\">GPIO_3(RX)<option value=\"4\">GPIO_4(D2)<option value=\"5\">GPIO_5(D1)<option value=\"9\">GPIO_9(D11)<option value=\"A\">GPIO_10(D12)<option value=\"C\">GPIO_12(D6)<option value=\"D\">GPIO_13(D7)<option value=\"E\">GPIO_14(D5)<option value=\"F\" selected>GPIO_15(D8)<option value=\"G\">GPIO_16(D0)";
	// else if ((select_config) == PIN_GPIO_16) return "<option value=\"O\">OFF<option value=\"0\">GPIO_0(D3)<option value=\"1\">GPIO_1(TX)<option value=\"2\">GPIO_2(D4)<option value=\"3\">GPIO_3(RX)<option value=\"4\">GPIO_4(D2)<option value=\"5\">GPIO_5(D1)<option value=\"9\">GPIO_9(D11)<option value=\"A\">GPIO_10(D12)<option value=\"C\">GPIO_12(D6)<option value=\"D\">GPIO_13(D7)<option value=\"E\">GPIO_14(D5)<option value=\"F\">GPIO_15(D8)<option value=\"G\" selected>GPIO_16(D0)";
	// else return "<option value=\"O\" selected>OFF<option value=\"0\">GPIO_0(D3)<option value=\"1\">GPIO_1(TX)<option value=\"2\">GPIO_2(D4)<option value=\"3\">GPIO_3(RX)<option value=\"4\">GPIO_4(D2)<option value=\"5\">GPIO_5(D1)<option value=\"9\">GPIO_9(D11)<option value=\"A\">GPIO_10(D12)<option value=\"C\">GPIO_12(D6)<option value=\"D\">GPIO_13(D7)<option value=\"E\">GPIO_14(D5)<option value=\"F\">GPIO_15(D8)<option value=\"G\">GPIO_16(D0)";
}
//supla_log(LOG_DEBUG, "select_config: %i", select_config);
#endif
//****************************************KONIEC dla modulu MULTIBOARD_TYPE_SELECTION*********************************
#if defined(MULTIBOARD_TYPE_SELECTION)
// char ICACHE_FLASH_ATTR * host_name(char hname){
// char *supla_host_name = 0;
// supla_host_name =(char*)os_malloc(256);
// }


char ICACHE_FLASH_ATTR * board_start(const char *WIFI_SSID_start, const char *Server_start, const char *Email_start, const char *host_name){
	// if (nr_module == 0) return "";

	start_settings = (char*)os_malloc(550);
	// start_settings =(char*)os_malloc(1024);
	strcpy(start_settings, "<div class=\"w\" style=");
	if (nr_module == 0) strcat(start_settings, "\"visibility:hidden\"");
	else strcat(start_settings, "\"visibility:visible\"");
	strcat(start_settings, " ><h3>Wi-Fi Settings</h3><i><input name=\"sid\"");
	strcat(start_settings, "value=\"");
	strcat(start_settings, WIFI_SSID_start);
	strcat(start_settings, "\"><label>Network name</label></i><i><input name=\"wpw\" ");
	strcat(start_settings, "><label>Password</label></i>");
	strcat(start_settings, "<i><input name=\"hsn\" ");
	strcat(start_settings, "value=\"");
	strcat(start_settings, host_name);
	strcat(start_settings, "\" placeholder=\"Is not required\"><label>Host Name</label></i></div>");

	strcat(start_settings, "<div class=\"w\" style=");
	if (nr_module == 0) strcat(start_settings, "\"visibility:hidden\"");
	else strcat(start_settings, "\"visibility:visible\"");
	strcat(start_settings, " ><h3>Supla Settings</h3><i><input name=\"svr\" ");
	strcat(start_settings, "value=\"");
	strcat(start_settings, Server_start);
	strcat(start_settings, "\"><label>Server</label></i><i><input name=\"eml\" ");
	strcat(start_settings, "value=\"");
	strcat(start_settings, Email_start);
	strcat(start_settings, "\"><label>E-mail</label></i></div>");
	return start_settings;

}

char ICACHE_FLASH_ATTR * board_selected(char select_board){

	board_name = (char*) os_malloc(40 * MAX_SUPORTED_MODULES);
	strcpy(board_name, "<i><label>MODULE</label><select name=\"mlt\">");
	for (int suported_module = 0; suported_module < MAX_SUPORTED_MODULES; suported_module++) {
		strcat(board_name, "<option value=\"");
		strcat(board_name, IntToChar[suported_module]);

		if (chartoint(select_board) == suported_module) {
			strcat(board_name, "\" selected>");
		}
		else strcat(board_name, "\" >");
		strcat(board_name, Supported_Modules[suported_module]);
	}
	strcat(board_name, "</select></i>");
	return board_name;
}

char ICACHE_FLASH_ATTR * led_status(char select_status_led){

	if (led_set == 0) return "";

	led_select = (char*)os_malloc(256);
	// led_select =(char*)os_malloc(256);
	strcpy(led_select, "<i><label>LED config status:</label><select name=\"led\">");
	for (int select = 0; select < MAX_LED_CONFIG_SUPORT; select++) {
		strcat(led_select, "<option value=\"");
		strcat(led_select, IntToChar[select]);

		if (select_status_led == select) {
			strcat(led_select, "\" selected>");
		}
		else strcat(led_select, "\" >");
		strcat(led_select, Supported_LED[select]);
	}
	strcat(led_select, "</select></i>");
	return led_select;
}



char ICACHE_FLASH_ATTR * thermometer_selected(char select_thermometer){
	if (nr_temperature == 0) return "";
	if (nr_temperature > 252) return "";

	thermometer_name = (char*)os_malloc(54 +(33 * MAX_TEMP_SENSOR_SUPORT));
	strcpy(thermometer_name, "<i><label>");
	strcat(thermometer_name, "Thermometer type:</label><select name=\"trm\">");

	for (size_t suported_thermometer = 0; suported_thermometer < MAX_TEMP_SENSOR_SUPORT; suported_thermometer++) {
		strcat(thermometer_name, "<option value=\"");

		strcat(thermometer_name,IntToChar[suported_thermometer]);

		if (select_thermometer == suported_thermometer) {
			strcat(thermometer_name, "\" selected>");
		}
		else strcat(thermometer_name, "\" >");

		strcat(thermometer_name, Supported_Temperature[suported_thermometer]);
	}

	strcat(thermometer_name, "</select></i>");
	return thermometer_name;
}

char ICACHE_FLASH_ATTR * button_selected(char select_button, uint8 nr){

	if (nr_button < nr) return "";

	button_type = (char*) os_malloc(140);
	strcpy(button_type, "<i><label>Button_");
	strcat(button_type, IntToChar[nr]);
	strcat(button_type, " type:</label><select name=\"bt");
	strcat(button_type, IntToChar[nr]);
	strcat(button_type, "\">");

	for (size_t suported_button = 0; suported_button < 2; suported_button++) {
		strcat(button_type, "<option value=\"");
		strcat(button_type,IntToChar[suported_button]);
		if (select_button == suported_button) {
			strcat(button_type, "\" selected>");
		}
		else strcat(button_type, "\" >");
		strcat(button_type, Supported_Button[suported_button]);
	}
	strcat(button_type, "</select></i>");

	return button_type;
}

char ICACHE_FLASH_ATTR * button_configure_selected(char select_button){

	if (button_configure_set == 0) return "";

	button_cfg_type = (char*)os_malloc(120);
	strcpy(button_cfg_type, "<i><label>Button CFG type:</label><select name=\"cfg\">");

	for (size_t suported_button = 0; suported_button < 2; suported_button++) {
		strcat(button_cfg_type, "<option value=\"");

		strcat(button_cfg_type,IntToChar[suported_button]);

		if (select_button == suported_button) {
			strcat(button_cfg_type, "\" selected>");
		}
		else strcat(button_cfg_type, "\" >");

		strcat(button_cfg_type, Supported_ButtonConfigure[suported_button]);
	}

	strcat(button_cfg_type, "</select></i>");
	return button_cfg_type;
}

char ICACHE_FLASH_ATTR * gpio_user_configurable_selected(void){

	if (nr_pin_configurable == 0) return "";

	pin_type = (char*)os_malloc((56 + (30 * END_USER_CONFIGURABLE)) * nr_pin_configurable+10);
	strcpy(pin_type, "");
	for (size_t i = 0; i < nr_pin_configurable; i++) {
		strcat(pin_type, "<i><label>GPIO");
		int ii = supla_esp_cfg.GPIO_UserConfigurable[i];
		strcat(pin_type, IntToCharWeb[ii]);
		strcat(pin_type, NodeMCU_GPIO[ii]);
		strcat(pin_type, ":</label><select name=\"pc");
		strcat(pin_type, IntToChar[i]);
		strcat(pin_type, "\">");
		for (size_t suported_pin = 0; suported_pin < END_USER_CONFIGURABLE; suported_pin++) {
			strcat(pin_type, "<option value=\"");

			strcat(pin_type, IntToChar[suported_pin]);

			if (supla_esp_cfg.PinMode_UserConfigurable[i] == suported_pin) {
				strcat(pin_type, "\" selected>");
			}
			else strcat(pin_type, "\" >");

			strcat(pin_type, User_Configurable[suported_pin]);
		}

		strcat(pin_type, "</select></i>");
	}

	return pin_type;
}

char ICACHE_FLASH_ATTR * ralay_level_selected(void){

	if (relay_counter == 0) return "";

	relay_level = (char*)os_malloc(156 * relay_counter * 2);
	strcpy(relay_level, "");
	for (size_t i = 0; i < relay_counter; i++) {
		strcat(relay_level, "<i><label>RELAY_");
		strcat(relay_level, IntToCharWeb[i+1]);
		strcat(relay_level, ":</label><select name=\"rl");
		strcat(relay_level, IntToChar[i]);
		strcat(relay_level, "\">");
		for (size_t suported_level = 0; suported_level < 2; suported_level++) {
			strcat(relay_level, "<option value=\"");
			strcat(relay_level, IntToChar[suported_level]);
			if (supla_esp_cfg.RelayLevel[i] == suported_level) {
				strcat(relay_level, "\" selected>");
			}
			else strcat(relay_level, "\" >");
			strcat(relay_level, Supported_RelayLevel[suported_level]);
		}
		strcat(relay_level, "</select></i>");

		strcat(relay_level, "<i><label>* relay flag");
		strcat(relay_level, ":</label><select name=\"fl");
		strcat(relay_level, IntToChar[i]);
		strcat(relay_level, "\">");
		for (size_t suported_level = 0; suported_level < 2; suported_level++) {
			strcat(relay_level, "<option value=\"");
			strcat(relay_level, IntToChar[suported_level]);
			if (supla_esp_cfg.RelayFlag[i] == suported_level) {
				strcat(relay_level, "\" selected>");
			}
			else strcat(relay_level, "\" >");
			strcat(relay_level, Supported_RelayFlag[suported_level]);
		}
		strcat(relay_level, "</select></i>");
	}

	return relay_level;
}



#endif


//****************************************KONIEC dla modulu MULTIBOARD_TYPE_SELECTION*********************************

void ICACHE_FLASH_ATTR
supla_esp_recv_callback (void *arg, char *pdata, unsigned short len)
{
	struct espconn *conn = (struct espconn *)arg;
	char mac[6];
	char data_saved = 0;
	char reboot = 0;

	supla_log(LOG_DEBUG, "Free heap size: %i", system_get_free_heap_size());
	supla_log(LOG_DEBUG, "REQUEST LEN: %i", len);


	TrivialHttpParserVars *pVars = (TrivialHttpParserVars *)conn->reverse;

	if ( pdata == NULL || pVars == NULL )
		return;

	SuplaEspCfg new_cfg;
	memcpy(&new_cfg, &supla_esp_cfg, sizeof(SuplaEspCfg));



	supla_esp_parse_request(pVars, pdata, len, &new_cfg, &reboot);

	if ( pVars->type == TYPE_UNKNOWN ) {

		supla_esp_http_404(conn);
		return;
	};

	if ( pVars->type == TYPE_POST ) {

		// supla_log(LOG_DEBUG, "Matched: %i", pVars->matched);

		if ( pVars->matched < 1 ) {
			return;
		}

		if ( reboot > 0 ) {
			system_restart();
			return;
		}

		if ( new_cfg.LocationPwd[0] == 0 )
			memcpy(new_cfg.LocationPwd, supla_esp_cfg.LocationPwd, SUPLA_LOCATION_PWD_MAXSIZE);

		if ( new_cfg.WIFI_PWD[0] == 0 )
			memcpy(new_cfg.WIFI_PWD, supla_esp_cfg.WIFI_PWD, WIFI_PWD_MAXSIZE);

		if ( 1 == supla_esp_cfg_save(&new_cfg) ) {

			memcpy(&supla_esp_cfg, &new_cfg, sizeof(SuplaEspCfg));
			data_saved = 1;

		}
		else {
			supla_esp_http_error(conn);
		}

	}

	if ( false == wifi_get_macaddr(STATION_IF, (unsigned char*)mac) ) {
		supla_esp_http_error(conn);
		return;
	}
	supla_esp_board_gpio_init();
	if (supla_esp_cfg.BoardNames != 0) {
		supla_check_temperatura();
	}

	char dev_name[25];
	supla_esp_board_set_device_name(dev_name, 25);
	dev_name[24] = 0;

	char *buffer = 0;

	#ifdef BOARD_CFG_HTML_TEMPLATE
	buffer = supla_esp_board_cfg_html_template(dev_name, mac, data_saved);
	#else

	#ifdef CFGBTN_TYPE_SELECTION

	char html_template_header[] = "<!DOCTYPE html><meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"><meta name=\"viewport\" content=\"width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no\"><style>body{font-size:14px;font-family:HelveticaNeue,\"Helvetica Neue\",HelveticaNeueRoman,HelveticaNeue-Roman,\"Helvetica Neue Roman\",TeXGyreHerosRegular,Helvetica,Tahoma,Geneva,Arial,sans-serif;font-weight:400;font-stretch:normal;background:#00d151;color:#fff;line-height:20px;padding:0}.s{width:460px;margin:0 auto;margin-top:calc(50vh - 340px);border:solid 3px #fff;padding:0 10px 10px;border-radius:3px}#l{display:block;max-width:150px;height:155px;margin:-80px auto 20px;background:#00d151;padding-right:5px}#l path{fill:#000}.w{margin:3px 0 16px;padding:5px 0;border-radius:3px;background:#fff;box-shadow:0 1px 3px rgba(0,0,0,.3)}h1,h3{margin:10px 8px;font-family:HelveticaNeueLight,HelveticaNeue-Light,\"Helvetica Neue Light\",HelveticaNeue,\"Helvetica Neue\",TeXGyreHerosRegular,Helvetica,Tahoma,Geneva,Arial,sans-serif;font-weight:300;font-stretch:normal;color:#000;font-size:23px}h1{margin-bottom:14px;color:#fff}span{display:block;margin:10px 7px 14px}i{display:block;font-style:normal;position:relative;border-bottom:solid 1px #00d151;height:42px}i:last-child{border:none}label{position:absolute;display:inline-block;top:0;left:8px;color:#00d151;line-height:41px;pointer-events:none}input,select{width:calc(100% - 145px);border:none;font-size:16px;line-height:40px;border-radius:0;letter-spacing:-.5px;background:#fff;color:#000;padding-left:144px;-webkit-appearance:none;-moz-appearance:none;appearance:none;outline:0!important;height:40px}select{padding:0;float:right;margin:1px 3px 1px 2px}button{width:100%;border:0;background:#000;padding:5px 10px;font-size:16px;line-height:40px;color:#fff;border-radius:3px;box-shadow:0 1px 3px rgba(0,0,0,.3);cursor:pointer}.c{background:#ffe836;position:fixed;width:100%;line-height:80px;color:#000;top:0;left:0;box-shadow:0 1px 3px rgba(0,0,0,.3);text-align:center;font-size:26px;z-index:100}@media all and (max-height:920px){.s{margin-top:80px}}@media all and (max-width:900px){.s{width:calc(100% - 20px);margin-top:40px;border:none;padding:0 8px;border-radius:0}#l{max-width:80px;height:auto;margin:10px auto 20px}h1,h3{font-size:19px}i{border:none;height:auto}label{display:block;margin:4px 0 12px;color:#00d151;font-size:13px;position:relative;line-height:18px}input,select{width:calc(100% - 10px);font-size:16px;line-height:28px;padding:0 5px;border-bottom:solid 1px #00d151}select{width:100%;float:none;margin:0}}</style><script type=\"text/javascript\">setTimeout(function(){var element =  document.getElementById('msg');if ( element != null ) element.style.visibility = \"hidden\";},3200);</script>";

		#ifdef __FOTA
	char html_template[] = "%s%s<div class=\"s\"><svg version=\"1.1\" id=\"l\" x=\"0\" y=\"0\" viewBox=\"0 0 200 200\" xml:space=\"preserve\"><path d=\"M59.3,2.5c18.1,0.6,31.8,8,40.2,23.5c3.1,5.7,4.3,11.9,4.1,18.3c-0.1,3.6-0.7,7.1-1.9,10.6c-0.2,0.7-0.1,1.1,0.6,1.5c12.8,7.7,25.5,15.4,38.3,23c2.9,1.7,5.8,3.4,8.7,5.3c1,0.6,1.6,0.6,2.5-0.1c4.5-3.6,9.8-5.3,15.7-5.4c12.5-0.1,22.9,7.9,25.2,19c1.9,9.2-2.9,19.2-11.8,23.9c-8.4,4.5-16.9,4.5-25.5,0.2c-0.7-0.3-1-0.2-1.5,0.3c-4.8,4.9-9.7,9.8-14.5,14.6c-5.3,5.3-10.6,10.7-15.9,16c-1.8,1.8-3.6,3.7-5.4,5.4c-0.7,0.6-0.6,1,0,1.6c3.6,3.4,5.8,7.5,6.2,12.2c0.7,7.7-2.2,14-8.8,18.5c-12.3,8.6-30.3,3.5-35-10.4c-2.8-8.4,0.6-17.7,8.6-22.8c0.9-0.6,1.1-1,0.8-2c-2-6.2-4.4-12.4-6.6-18.6c-6.3-17.6-12.7-35.1-19-52.7c-0.2-0.7-0.5-1-1.4-0.9c-12.5,0.7-23.6-2.6-33-10.4c-8-6.6-12.9-15-14.2-25c-1.5-11.5,1.7-21.9,9.6-30.7C32.5,8.9,42.2,4.2,53.7,2.7c0.7-0.1,1.5-0.2,2.2-0.2C57,2.4,58.2,2.5,59.3,2.5z M76.5,81c0,0.1,0.1,0.3,0.1,0.6c1.6,6.3,3.2,12.6,4.7,18.9c4.5,17.7,8.9,35.5,13.3,53.2c0.2,0.9,0.6,1.1,1.6,0.9c5.4-1.2,10.7-0.8,15.7,1.6c0.8,0.4,1.2,0.3,1.7-0.4c11.2-12.9,22.5-25.7,33.4-38.7c0.5-0.6,0.4-1,0-1.6c-5.6-7.9-6.1-16.1-1.3-24.5c0.5-0.8,0.3-1.1-0.5-1.6c-9.1-4.7-18.1-9.3-27.2-14c-6.8-3.5-13.5-7-20.3-10.5c-0.7-0.4-1.1-0.3-1.6,0.4c-1.3,1.8-2.7,3.5-4.3,5.1c-4.2,4.2-9.1,7.4-14.7,9.7C76.9,80.3,76.4,80.3,76.5,81z M89,42.6c0.1-2.5-0.4-5.4-1.5-8.1C83,23.1,74.2,16.9,61.7,15.8c-10-0.9-18.6,2.4-25.3,9.7c-8.4,9-9.3,22.4-2.2,32.4c6.8,9.6,19.1,14.2,31.4,11.9C79.2,67.1,89,55.9,89,42.6z M102.1,188.6c0.6,0.1,1.5-0.1,2.4-0.2c9.5-1.4,15.3-10.9,11.6-19.2c-2.6-5.9-9.4-9.6-16.8-8.6c-8.3,1.2-14.1,8.9-12.4,16.6C88.2,183.9,94.4,188.6,102.1,188.6z M167.7,88.5c-1,0-2.1,0.1-3.1,0.3c-9,1.7-14.2,10.6-10.8,18.6c2.9,6.8,11.4,10.3,19,7.8c7.1-2.3,11.1-9.1,9.6-15.9C180.9,93,174.8,88.5,167.7,88.5z\"/></svg><h1>%s</h1><span>LAST STATE: %s<br>Firmware: %s<br>GUID: %02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X<br>MAC: %02X:%02X:%02X:%02X:%02X:%02X</span><form method=\"post\"><div class=\"w\"><h3>Wi-Fi Settings</h3><i><input name=\"sid\" value=\"%s\"><label>Network name</label></i><i><input name=\"wpw\"><label>Password</label></i></div><div class=\"w\"><h3>Supla Settings</h3><i><input name=\"svr\" value=\"%s\"><label>Server</label></i><i><input name=\"eml\" value=\"%s\"><label>E-mail</label></i></div><div class=\"w\"><h3>Additional Settings</h3><i><select name=\"cfg\"><option value=\"0\" %s>monostble<option value=\"1\" %s>bistable</select><label>Button type</label></i><i><select name=\"upd\"><option value=\"0\" %s>NO<option value=\"1\" %s>YES</select><label>Firmware update</label></i></div><button type=\"submit\">SAVE</button></form></div><br><br>";
		#else
	char html_template[] = "%s%s<div class=\"s\"><svg version=\"1.1\" id=\"l\" x=\"0\" y=\"0\" viewBox=\"0 0 200 200\" xml:space=\"preserve\"><path d=\"M59.3,2.5c18.1,0.6,31.8,8,40.2,23.5c3.1,5.7,4.3,11.9,4.1,18.3c-0.1,3.6-0.7,7.1-1.9,10.6c-0.2,0.7-0.1,1.1,0.6,1.5c12.8,7.7,25.5,15.4,38.3,23c2.9,1.7,5.8,3.4,8.7,5.3c1,0.6,1.6,0.6,2.5-0.1c4.5-3.6,9.8-5.3,15.7-5.4c12.5-0.1,22.9,7.9,25.2,19c1.9,9.2-2.9,19.2-11.8,23.9c-8.4,4.5-16.9,4.5-25.5,0.2c-0.7-0.3-1-0.2-1.5,0.3c-4.8,4.9-9.7,9.8-14.5,14.6c-5.3,5.3-10.6,10.7-15.9,16c-1.8,1.8-3.6,3.7-5.4,5.4c-0.7,0.6-0.6,1,0,1.6c3.6,3.4,5.8,7.5,6.2,12.2c0.7,7.7-2.2,14-8.8,18.5c-12.3,8.6-30.3,3.5-35-10.4c-2.8-8.4,0.6-17.7,8.6-22.8c0.9-0.6,1.1-1,0.8-2c-2-6.2-4.4-12.4-6.6-18.6c-6.3-17.6-12.7-35.1-19-52.7c-0.2-0.7-0.5-1-1.4-0.9c-12.5,0.7-23.6-2.6-33-10.4c-8-6.6-12.9-15-14.2-25c-1.5-11.5,1.7-21.9,9.6-30.7C32.5,8.9,42.2,4.2,53.7,2.7c0.7-0.1,1.5-0.2,2.2-0.2C57,2.4,58.2,2.5,59.3,2.5z M76.5,81c0,0.1,0.1,0.3,0.1,0.6c1.6,6.3,3.2,12.6,4.7,18.9c4.5,17.7,8.9,35.5,13.3,53.2c0.2,0.9,0.6,1.1,1.6,0.9c5.4-1.2,10.7-0.8,15.7,1.6c0.8,0.4,1.2,0.3,1.7-0.4c11.2-12.9,22.5-25.7,33.4-38.7c0.5-0.6,0.4-1,0-1.6c-5.6-7.9-6.1-16.1-1.3-24.5c0.5-0.8,0.3-1.1-0.5-1.6c-9.1-4.7-18.1-9.3-27.2-14c-6.8-3.5-13.5-7-20.3-10.5c-0.7-0.4-1.1-0.3-1.6,0.4c-1.3,1.8-2.7,3.5-4.3,5.1c-4.2,4.2-9.1,7.4-14.7,9.7C76.9,80.3,76.4,80.3,76.5,81z M89,42.6c0.1-2.5-0.4-5.4-1.5-8.1C83,23.1,74.2,16.9,61.7,15.8c-10-0.9-18.6,2.4-25.3,9.7c-8.4,9-9.3,22.4-2.2,32.4c6.8,9.6,19.1,14.2,31.4,11.9C79.2,67.1,89,55.9,89,42.6z M102.1,188.6c0.6,0.1,1.5-0.1,2.4-0.2c9.5-1.4,15.3-10.9,11.6-19.2c-2.6-5.9-9.4-9.6-16.8-8.6c-8.3,1.2-14.1,8.9-12.4,16.6C88.2,183.9,94.4,188.6,102.1,188.6z M167.7,88.5c-1,0-2.1,0.1-3.1,0.3c-9,1.7-14.2,10.6-10.8,18.6c2.9,6.8,11.4,10.3,19,7.8c7.1-2.3,11.1-9.1,9.6-15.9C180.9,93,174.8,88.5,167.7,88.5z\"/></svg><h1>%s</h1><span>LAST STATE: %s<br>Firmware: %s<br>GUID: %02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X<br>MAC: %02X:%02X:%02X:%02X:%02X:%02X</span><form method=\"post\"><div class=\"w\"><h3>Wi-Fi Settings</h3><i><input name=\"sid\" value=\"%s\"><label>Network name</label></i><i><input name=\"wpw\"><label>Password</label></i></div><div class=\"w\"><h3>Supla Settings</h3><i><input name=\"svr\" value=\"%s\"><label>Server</label></i><i><input name=\"eml\" value=\"%s\"><label>E-mail</label></i></div><div class=\"w\"><h3>Additional Settings</h3><i><select name=\"cfg\"><option value=\"0\" %s>monostble<option value=\"1\" %s>bistable</select><label>Button type</label></i></div><button type=\"submit\">SAVE</button></form></div><br><br>";
		#endif


	int bufflen = strlen(supla_esp_devconn_laststate())
	              +strlen(dev_name)
	              +strlen(SUPLA_ESP_SOFTVER)
	              +strlen(supla_esp_cfg.WIFI_SSID)
	              +strlen(supla_esp_cfg.Server)
	              +strlen(supla_esp_cfg.Email)
	              +strlen(html_template_header)
	              +strlen(html_template)
	              +200;

	buffer = (char*)malloc(bufflen);

	ets_snprintf(buffer,
	             bufflen,
	             html_template,
	             html_template_header,
	             data_saved == 1 ? "<div id=\"msg\" class=\"c\">Data saved</div>" : "",
	             dev_name,
	             supla_esp_devconn_laststate(),
	             SUPLA_ESP_SOFTVER,
	             (unsigned char)supla_esp_cfg.GUID[0],
	             (unsigned char)supla_esp_cfg.GUID[1],
	             (unsigned char)supla_esp_cfg.GUID[2],
	             (unsigned char)supla_esp_cfg.GUID[3],
	             (unsigned char)supla_esp_cfg.GUID[4],
	             (unsigned char)supla_esp_cfg.GUID[5],
	             (unsigned char)supla_esp_cfg.GUID[6],
	             (unsigned char)supla_esp_cfg.GUID[7],
	             (unsigned char)supla_esp_cfg.GUID[8],
	             (unsigned char)supla_esp_cfg.GUID[9],
	             (unsigned char)supla_esp_cfg.GUID[10],
	             (unsigned char)supla_esp_cfg.GUID[11],
	             (unsigned char)supla_esp_cfg.GUID[12],
	             (unsigned char)supla_esp_cfg.GUID[13],
	             (unsigned char)supla_esp_cfg.GUID[14],
	             (unsigned char)supla_esp_cfg.GUID[15],
	             (unsigned char)mac[0],
	             (unsigned char)mac[1],
	             (unsigned char)mac[2],
	             (unsigned char)mac[3],
	             (unsigned char)mac[4],
	             (unsigned char)mac[5],
	             supla_esp_cfg.WIFI_SSID,
	             supla_esp_cfg.Server,
	             supla_esp_cfg.Email,
	             supla_esp_cfg.CfgButtonType == BTN_TYPE_MONOSTABLE ? "selected" : "",
	             supla_esp_cfg.CfgButtonType == BTN_TYPE_BISTABLE ? "selected" : ""
				#ifdef __FOTA
	             ,
	             supla_esp_cfg.FirmwareUpdate == 0 ? "selected" : "",
	             supla_esp_cfg.FirmwareUpdate == 1 ? "selected" : ""
				#endif
	             );


	#elif defined(BTN1_2_TYPE_SELECTION)


	char html_template_header[] = "<!DOCTYPE html><meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"><meta name=\"viewport\" content=\"width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no\"><style>body{font-size:14px;font-family:HelveticaNeue,\"Helvetica Neue\",HelveticaNeueRoman,HelveticaNeue-Roman,\"Helvetica Neue Roman\",TeXGyreHerosRegular,Helvetica,Tahoma,Geneva,Arial,sans-serif;font-weight:400;font-stretch:normal;background:#00d151;color:#fff;line-height:20px;padding:0}.s{width:460px;margin:0 auto;margin-top:calc(50vh - 340px);border:solid 3px #fff;padding:0 10px 10px;border-radius:3px}#l{display:block;max-width:150px;height:155px;margin:-80px auto 20px;background:#00d151;padding-right:5px}#l path{fill:#000}.w{margin:3px 0 16px;padding:5px 0;border-radius:3px;background:#fff;box-shadow:0 1px 3px rgba(0,0,0,.3)}h1,h3{margin:10px 8px;font-family:HelveticaNeueLight,HelveticaNeue-Light,\"Helvetica Neue Light\",HelveticaNeue,\"Helvetica Neue\",TeXGyreHerosRegular,Helvetica,Tahoma,Geneva,Arial,sans-serif;font-weight:300;font-stretch:normal;color:#000;font-size:23px}h1{margin-bottom:14px;color:#fff}span{display:block;margin:10px 7px 14px}i{display:block;font-style:normal;position:relative;border-bottom:solid 1px #00d151;height:42px}i:last-child{border:none}label{position:absolute;display:inline-block;top:0;left:8px;color:#00d151;line-height:41px;pointer-events:none}input,select{width:calc(100% - 145px);border:none;font-size:16px;line-height:40px;border-radius:0;letter-spacing:-.5px;background:#fff;color:#000;padding-left:144px;-webkit-appearance:none;-moz-appearance:none;appearance:none;outline:0!important;height:40px}select{padding:0;float:right;margin:1px 3px 1px 2px}button{width:100%;border:0;background:#000;padding:5px 10px;font-size:16px;line-height:40px;color:#fff;border-radius:3px;box-shadow:0 1px 3px rgba(0,0,0,.3);cursor:pointer}.c{background:#ffe836;position:fixed;width:100%;line-height:80px;color:#000;top:0;left:0;box-shadow:0 1px 3px rgba(0,0,0,.3);text-align:center;font-size:26px;z-index:100}@media all and (max-height:920px){.s{margin-top:80px}}@media all and (max-width:900px){.s{width:calc(100% - 20px);margin-top:40px;border:none;padding:0 8px;border-radius:0}#l{max-width:80px;height:auto;margin:10px auto 20px}h1,h3{font-size:19px}i{border:none;height:auto}label{display:block;margin:4px 0 12px;color:#00d151;font-size:13px;position:relative;line-height:18px}input,select{width:calc(100% - 10px);font-size:16px;line-height:28px;padding:0 5px;border-bottom:solid 1px #00d151}select{width:100%;float:none;margin:0}}</style><script type=\"text/javascript\">setTimeout(function(){var element =  document.getElementById('msg');if ( element != null ) element.style.visibility = \"hidden\";},3200);</script>";

		#ifdef __FOTA
	char html_template[] = "%s%s<div class=\"s\"><svg version=\"1.1\" id=\"l\" x=\"0\" y=\"0\" viewBox=\"0 0 200 200\" xml:space=\"preserve\"><path d=\"M59.3,2.5c18.1,0.6,31.8,8,40.2,23.5c3.1,5.7,4.3,11.9,4.1,18.3c-0.1,3.6-0.7,7.1-1.9,10.6c-0.2,0.7-0.1,1.1,0.6,1.5c12.8,7.7,25.5,15.4,38.3,23c2.9,1.7,5.8,3.4,8.7,5.3c1,0.6,1.6,0.6,2.5-0.1c4.5-3.6,9.8-5.3,15.7-5.4c12.5-0.1,22.9,7.9,25.2,19c1.9,9.2-2.9,19.2-11.8,23.9c-8.4,4.5-16.9,4.5-25.5,0.2c-0.7-0.3-1-0.2-1.5,0.3c-4.8,4.9-9.7,9.8-14.5,14.6c-5.3,5.3-10.6,10.7-15.9,16c-1.8,1.8-3.6,3.7-5.4,5.4c-0.7,0.6-0.6,1,0,1.6c3.6,3.4,5.8,7.5,6.2,12.2c0.7,7.7-2.2,14-8.8,18.5c-12.3,8.6-30.3,3.5-35-10.4c-2.8-8.4,0.6-17.7,8.6-22.8c0.9-0.6,1.1-1,0.8-2c-2-6.2-4.4-12.4-6.6-18.6c-6.3-17.6-12.7-35.1-19-52.7c-0.2-0.7-0.5-1-1.4-0.9c-12.5,0.7-23.6-2.6-33-10.4c-8-6.6-12.9-15-14.2-25c-1.5-11.5,1.7-21.9,9.6-30.7C32.5,8.9,42.2,4.2,53.7,2.7c0.7-0.1,1.5-0.2,2.2-0.2C57,2.4,58.2,2.5,59.3,2.5z M76.5,81c0,0.1,0.1,0.3,0.1,0.6c1.6,6.3,3.2,12.6,4.7,18.9c4.5,17.7,8.9,35.5,13.3,53.2c0.2,0.9,0.6,1.1,1.6,0.9c5.4-1.2,10.7-0.8,15.7,1.6c0.8,0.4,1.2,0.3,1.7-0.4c11.2-12.9,22.5-25.7,33.4-38.7c0.5-0.6,0.4-1,0-1.6c-5.6-7.9-6.1-16.1-1.3-24.5c0.5-0.8,0.3-1.1-0.5-1.6c-9.1-4.7-18.1-9.3-27.2-14c-6.8-3.5-13.5-7-20.3-10.5c-0.7-0.4-1.1-0.3-1.6,0.4c-1.3,1.8-2.7,3.5-4.3,5.1c-4.2,4.2-9.1,7.4-14.7,9.7C76.9,80.3,76.4,80.3,76.5,81z M89,42.6c0.1-2.5-0.4-5.4-1.5-8.1C83,23.1,74.2,16.9,61.7,15.8c-10-0.9-18.6,2.4-25.3,9.7c-8.4,9-9.3,22.4-2.2,32.4c6.8,9.6,19.1,14.2,31.4,11.9C79.2,67.1,89,55.9,89,42.6z M102.1,188.6c0.6,0.1,1.5-0.1,2.4-0.2c9.5-1.4,15.3-10.9,11.6-19.2c-2.6-5.9-9.4-9.6-16.8-8.6c-8.3,1.2-14.1,8.9-12.4,16.6C88.2,183.9,94.4,188.6,102.1,188.6z M167.7,88.5c-1,0-2.1,0.1-3.1,0.3c-9,1.7-14.2,10.6-10.8,18.6c2.9,6.8,11.4,10.3,19,7.8c7.1-2.3,11.1-9.1,9.6-15.9C180.9,93,174.8,88.5,167.7,88.5z\"/></svg><h1>%s</h1><span>LAST STATE: %s<br>Firmware: %s<br>GUID: %02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X<br>MAC: %02X:%02X:%02X:%02X:%02X:%02X</span><form method=\"post\"><div class=\"w\"><h3>Wi-Fi Settings</h3><i><input name=\"sid\" value=\"%s\"><label>Network name</label></i><i><input name=\"wpw\"><label>Password</label></i></div><div class=\"w\"><h3>Supla Settings</h3><i><input name=\"svr\" value=\"%s\"><label>Server</label></i><i><input name=\"eml\" value=\"%s\"><label>E-mail</label></i></div><div class=\"w\"><h3>Additional Settings</h3><i><select name=\"bt1\"><option value=\"0\" %s>monostble<option value=\"1\" %s>bistable</select><label>Input1 type:</label></i><i><select name=\"bt2\"><option value=\"0\" %s>monostble<option value=\"1\" %s>bistable</select><label>Input2 type:</label></i><i><select name=\"upd\"><option value=\"0\" %s>NO<option value=\"1\" %s>YES</select><label>Firmware update</label></i></div><button type=\"submit\">SAVE</button></form></div><br><br>";
		#else
	char html_template[] = "%s%s<div class=\"s\"><svg version=\"1.1\" id=\"l\" x=\"0\" y=\"0\" viewBox=\"0 0 200 200\" xml:space=\"preserve\"><path d=\"M59.3,2.5c18.1,0.6,31.8,8,40.2,23.5c3.1,5.7,4.3,11.9,4.1,18.3c-0.1,3.6-0.7,7.1-1.9,10.6c-0.2,0.7-0.1,1.1,0.6,1.5c12.8,7.7,25.5,15.4,38.3,23c2.9,1.7,5.8,3.4,8.7,5.3c1,0.6,1.6,0.6,2.5-0.1c4.5-3.6,9.8-5.3,15.7-5.4c12.5-0.1,22.9,7.9,25.2,19c1.9,9.2-2.9,19.2-11.8,23.9c-8.4,4.5-16.9,4.5-25.5,0.2c-0.7-0.3-1-0.2-1.5,0.3c-4.8,4.9-9.7,9.8-14.5,14.6c-5.3,5.3-10.6,10.7-15.9,16c-1.8,1.8-3.6,3.7-5.4,5.4c-0.7,0.6-0.6,1,0,1.6c3.6,3.4,5.8,7.5,6.2,12.2c0.7,7.7-2.2,14-8.8,18.5c-12.3,8.6-30.3,3.5-35-10.4c-2.8-8.4,0.6-17.7,8.6-22.8c0.9-0.6,1.1-1,0.8-2c-2-6.2-4.4-12.4-6.6-18.6c-6.3-17.6-12.7-35.1-19-52.7c-0.2-0.7-0.5-1-1.4-0.9c-12.5,0.7-23.6-2.6-33-10.4c-8-6.6-12.9-15-14.2-25c-1.5-11.5,1.7-21.9,9.6-30.7C32.5,8.9,42.2,4.2,53.7,2.7c0.7-0.1,1.5-0.2,2.2-0.2C57,2.4,58.2,2.5,59.3,2.5z M76.5,81c0,0.1,0.1,0.3,0.1,0.6c1.6,6.3,3.2,12.6,4.7,18.9c4.5,17.7,8.9,35.5,13.3,53.2c0.2,0.9,0.6,1.1,1.6,0.9c5.4-1.2,10.7-0.8,15.7,1.6c0.8,0.4,1.2,0.3,1.7-0.4c11.2-12.9,22.5-25.7,33.4-38.7c0.5-0.6,0.4-1,0-1.6c-5.6-7.9-6.1-16.1-1.3-24.5c0.5-0.8,0.3-1.1-0.5-1.6c-9.1-4.7-18.1-9.3-27.2-14c-6.8-3.5-13.5-7-20.3-10.5c-0.7-0.4-1.1-0.3-1.6,0.4c-1.3,1.8-2.7,3.5-4.3,5.1c-4.2,4.2-9.1,7.4-14.7,9.7C76.9,80.3,76.4,80.3,76.5,81z M89,42.6c0.1-2.5-0.4-5.4-1.5-8.1C83,23.1,74.2,16.9,61.7,15.8c-10-0.9-18.6,2.4-25.3,9.7c-8.4,9-9.3,22.4-2.2,32.4c6.8,9.6,19.1,14.2,31.4,11.9C79.2,67.1,89,55.9,89,42.6z M102.1,188.6c0.6,0.1,1.5-0.1,2.4-0.2c9.5-1.4,15.3-10.9,11.6-19.2c-2.6-5.9-9.4-9.6-16.8-8.6c-8.3,1.2-14.1,8.9-12.4,16.6C88.2,183.9,94.4,188.6,102.1,188.6z M167.7,88.5c-1,0-2.1,0.1-3.1,0.3c-9,1.7-14.2,10.6-10.8,18.6c2.9,6.8,11.4,10.3,19,7.8c7.1-2.3,11.1-9.1,9.6-15.9C180.9,93,174.8,88.5,167.7,88.5z\"/></svg><h1>%s</h1><span>LAST STATE: %s<br>Firmware: %s<br>GUID: %02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X<br>MAC: %02X:%02X:%02X:%02X:%02X:%02X</span><form method=\"post\"><div class=\"w\"><h3>Wi-Fi Settings</h3><i><input name=\"sid\" value=\"%s\"><label>Network name</label></i><i><input name=\"wpw\"><label>Password</label></i></div><div class=\"w\"><h3>Supla Settings</h3><i><input name=\"svr\" value=\"%s\"><label>Server</label></i><i><input name=\"eml\" value=\"%s\"><label>E-mail</label></i></div><div class=\"w\"><h3>Additional Settings</h3><i><select name=\"bt1\"><option value=\"0\" %s>monostble<option value=\"1\" %s>bistable</select><label>Input1 type:</label></i><i><select name=\"bt2\"><option value=\"0\" %s>monostble<option value=\"1\" %s>bistable</select><label>Input2 type:</label></i></div><button type=\"submit\">SAVE</button></form></div><br><br>";
		#endif

	int bufflen = strlen(supla_esp_devconn_laststate())
	              +strlen(dev_name)
	              +strlen(SUPLA_ESP_SOFTVER)
	              +strlen(supla_esp_cfg.WIFI_SSID)
	              +strlen(supla_esp_cfg.Server)
	              +strlen(supla_esp_cfg.Email)
	              +strlen(html_template_header)
	              +strlen(html_template)
	              +200;

	buffer = (char*)malloc(bufflen);

	ets_snprintf(buffer,
	             bufflen,
	             html_template,
	             html_template_header,
	             data_saved == 1 ? "<div id=\"msg\" class=\"c\">Data saved</div>" : "",
	             dev_name,
	             supla_esp_devconn_laststate(),
	             SUPLA_ESP_SOFTVER,
	             (unsigned char)supla_esp_cfg.GUID[0],
	             (unsigned char)supla_esp_cfg.GUID[1],
	             (unsigned char)supla_esp_cfg.GUID[2],
	             (unsigned char)supla_esp_cfg.GUID[3],
	             (unsigned char)supla_esp_cfg.GUID[4],
	             (unsigned char)supla_esp_cfg.GUID[5],
	             (unsigned char)supla_esp_cfg.GUID[6],
	             (unsigned char)supla_esp_cfg.GUID[7],
	             (unsigned char)supla_esp_cfg.GUID[8],
	             (unsigned char)supla_esp_cfg.GUID[9],
	             (unsigned char)supla_esp_cfg.GUID[10],
	             (unsigned char)supla_esp_cfg.GUID[11],
	             (unsigned char)supla_esp_cfg.GUID[12],
	             (unsigned char)supla_esp_cfg.GUID[13],
	             (unsigned char)supla_esp_cfg.GUID[14],
	             (unsigned char)supla_esp_cfg.GUID[15],
	             (unsigned char)mac[0],
	             (unsigned char)mac[1],
	             (unsigned char)mac[2],
	             (unsigned char)mac[3],
	             (unsigned char)mac[4],
	             (unsigned char)mac[5],
	             supla_esp_cfg.WIFI_SSID,
	             supla_esp_cfg.Server,
	             supla_esp_cfg.LocationID,
	             supla_esp_cfg.Button1Type == BTN_TYPE_MONOSTABLE ? "selected" : "",
	             supla_esp_cfg.Button1Type == BTN_TYPE_BISTABLE ? "selected" : "",
	             supla_esp_cfg.Button2Type == BTN_TYPE_MONOSTABLE ? "selected" : "",
	             supla_esp_cfg.Button2Type == BTN_TYPE_BISTABLE ? "selected" : ""

				#ifdef __FOTA
	             ,
	             supla_esp_cfg.FirmwareUpdate == 0 ? "selected" : "",
	             supla_esp_cfg.FirmwareUpdate == 1 ? "selected" : ""
				#endif
	             );

//****************************************dla modulu inCan****************************************
		#elif defined(INCAN_TYPE_SELECTION)

	int bufflen = strlen(supla_esp_devconn_laststate())
	              +strlen(dev_name)
	              +strlen(SUPLA_ESP_SOFTVER)
	              +strlen(supla_esp_cfg.WIFI_SSID)
	              +strlen(supla_esp_cfg.Server)
	              +strlen(supla_esp_cfg.Email)
	              // +strlen(html_template_header)
	              +strlen(html_template_header_inCan())
	              +strlen(html_template_inCan(HTML_STRONA_START))
	              +200;

	buffer = (char*)os_malloc(bufflen);

	ets_snprintf(buffer,
	             bufflen,
	             html_template_inCan(HTML_STRONA_START),
	             html_template_header_inCan(),
	             data_saved == 1 ? "<div id=\"msg\" class=\"c\">Data saved</div>" : "",
	             dev_name,
	             supla_esp_devconn_laststate(),
	             SUPLA_ESP_SOFTVER,
	             (unsigned char)supla_esp_cfg.GUID[0],
	             (unsigned char)supla_esp_cfg.GUID[1],
	             (unsigned char)supla_esp_cfg.GUID[2],
	             (unsigned char)supla_esp_cfg.GUID[3],
	             (unsigned char)supla_esp_cfg.GUID[4],
	             (unsigned char)supla_esp_cfg.GUID[5],
	             (unsigned char)supla_esp_cfg.GUID[6],
	             (unsigned char)supla_esp_cfg.GUID[7],
	             (unsigned char)supla_esp_cfg.GUID[8],
	             (unsigned char)supla_esp_cfg.GUID[9],
	             (unsigned char)supla_esp_cfg.GUID[10],
	             (unsigned char)supla_esp_cfg.GUID[11],
	             (unsigned char)supla_esp_cfg.GUID[12],
	             (unsigned char)supla_esp_cfg.GUID[13],
	             (unsigned char)supla_esp_cfg.GUID[14],
	             (unsigned char)supla_esp_cfg.GUID[15],
	             (unsigned char)mac[0],
	             (unsigned char)mac[1],
	             (unsigned char)mac[2],
	             (unsigned char)mac[3],
	             (unsigned char)mac[4],
	             (unsigned char)mac[5],
	             supla_esp_cfg.WIFI_SSID,
	             supla_esp_cfg.Server,
	             supla_esp_cfg.Email,
	             supla_esp_cfg.RollerShutter == ROLLER_SHUTTER_OFF ? "selected" : "",
	             supla_esp_cfg.RollerShutter == ROLLER_SHUTTER_ON ? "selected" : "",
	             supla_esp_cfg.ThermometerType == THERMOMETER_TYPE_DS18B20 ? "selected" : "",
	             supla_esp_cfg.ThermometerType == THERMOMETER_TYPE_DHT11 ? "selected" : "",
	             supla_esp_cfg.ThermometerType == THERMOMETER_TYPE_DHT22 ? "selected" : "",
	             supla_esp_cfg.Button1Type == BTN_TYPE_BISTABLE ? "selected" : "",
	             supla_esp_cfg.Button1Type == BTN_TYPE_MONOSTABLE ? "selected" : "",
	             supla_esp_cfg.Button2Type == BTN_TYPE_BISTABLE ? "selected" : "",
	             supla_esp_cfg.Button2Type == BTN_TYPE_MONOSTABLE ? "selected" : ""
					#ifdef __FOTA
	             ,
	             supla_esp_cfg.FirmwareUpdate == 0 ? "selected" : "",
	             supla_esp_cfg.FirmwareUpdate == 1 ? "selected" : ""
					#endif
	             );

//****************************************KONIEC dla modulu inCan*************************************
//****************************************dla modulu inCanEASY****************************************
		#elif defined(INCAN_EASY_TYPE_SELECTION)
	if( supla_esp_cfg.StronaHTML == 0) {
		int bufflen = strlen(supla_esp_devconn_laststate())
		              +strlen(dev_name)
		              +strlen(SUPLA_ESP_SOFTVER)
		              +strlen(supla_esp_cfg.WIFI_SSID)
		              +strlen(supla_esp_cfg.Server)
		              +strlen(supla_esp_cfg.Email)
		              +strlen(html_template_header_inCan())
		              +strlen(html_template_inCan(supla_esp_cfg.StronaHTML))
		              +200;

		buffer = (char*)os_malloc(bufflen);

		ets_snprintf(buffer,
		             bufflen,
		             html_template_inCan(supla_esp_cfg.StronaHTML),
		             html_template_header_inCan(),
		             data_saved == 1 ? "<div id=\"msg\" class=\"c\">Data saved</div>" : "",
		             dev_name,
		             supla_esp_devconn_laststate(),
		             SUPLA_ESP_SOFTVER,
		             (unsigned char)supla_esp_cfg.GUID[0],
		             (unsigned char)supla_esp_cfg.GUID[1],
		             (unsigned char)supla_esp_cfg.GUID[2],
		             (unsigned char)supla_esp_cfg.GUID[3],
		             (unsigned char)supla_esp_cfg.GUID[4],
		             (unsigned char)supla_esp_cfg.GUID[5],
		             (unsigned char)supla_esp_cfg.GUID[6],
		             (unsigned char)supla_esp_cfg.GUID[7],
		             (unsigned char)supla_esp_cfg.GUID[8],
		             (unsigned char)supla_esp_cfg.GUID[9],
		             (unsigned char)supla_esp_cfg.GUID[10],
		             (unsigned char)supla_esp_cfg.GUID[11],
		             (unsigned char)supla_esp_cfg.GUID[12],
		             (unsigned char)supla_esp_cfg.GUID[13],
		             (unsigned char)supla_esp_cfg.GUID[14],
		             (unsigned char)supla_esp_cfg.GUID[15],
		             (unsigned char)mac[0],
		             (unsigned char)mac[1],
		             (unsigned char)mac[2],
		             (unsigned char)mac[3],
		             (unsigned char)mac[4],
		             (unsigned char)mac[5],
		             supla_esp_cfg.WIFI_SSID,
		             supla_esp_cfg.Server,
		             supla_esp_cfg.Email
								 #ifdef __FOTA
		             ,
		             supla_esp_cfg.FirmwareUpdate == 0 ? "selected" : "",
		             supla_esp_cfg.FirmwareUpdate == 1 ? "selected" : ""
								#endif
		             );
	}
	else
	if( supla_esp_cfg.StronaHTML == 1) {

		int bufflen = strlen(supla_esp_devconn_laststate())
		              +strlen(dev_name)
		              +strlen(SUPLA_ESP_SOFTVER)
		              +strlen(html_template_header_inCan())
		              +strlen(html_template_inCan(supla_esp_cfg.StronaHTML))
		              +strlen(gpio_selected(supla_esp_cfg.Relay1_GPIO))
		              +strlen(gpio_selected(supla_esp_cfg.Relay2_GPIO))
		              +strlen(gpio_selected(supla_esp_cfg.Button1_GPIO))
		              +strlen(gpio_selected(supla_esp_cfg.Button2_GPIO))
		              +strlen(gpio_selected(supla_esp_cfg.Sensor1_GPIO))
		              +strlen(gpio_selected(supla_esp_cfg.Sensor2_GPIO))
		              +strlen(gpio_selected(supla_esp_cfg.Thermometer_GPIO))
		              +strlen(gpio_selected(supla_esp_cfg.BtnConfig_GPIO))
		              +strlen(gpio_selected(supla_esp_cfg.LedConfig_GPIO))
		              +200;

		buffer = (char*)os_malloc(bufflen);

		ets_snprintf(buffer,
		             bufflen,
		             html_template_inCan(supla_esp_cfg.StronaHTML),
		             html_template_header_inCan(),
		             data_saved == 1 ? "" : "",
		             dev_name,
		             supla_esp_devconn_laststate(),
		             SUPLA_ESP_SOFTVER,
		             (unsigned char)supla_esp_cfg.GUID[0],
		             (unsigned char)supla_esp_cfg.GUID[1],
		             (unsigned char)supla_esp_cfg.GUID[2],
		             (unsigned char)supla_esp_cfg.GUID[3],
		             (unsigned char)supla_esp_cfg.GUID[4],
		             (unsigned char)supla_esp_cfg.GUID[5],
		             (unsigned char)supla_esp_cfg.GUID[6],
		             (unsigned char)supla_esp_cfg.GUID[7],
		             (unsigned char)supla_esp_cfg.GUID[8],
		             (unsigned char)supla_esp_cfg.GUID[9],
		             (unsigned char)supla_esp_cfg.GUID[10],
		             (unsigned char)supla_esp_cfg.GUID[11],
		             (unsigned char)supla_esp_cfg.GUID[12],
		             (unsigned char)supla_esp_cfg.GUID[13],
		             (unsigned char)supla_esp_cfg.GUID[14],
		             (unsigned char)supla_esp_cfg.GUID[15],
		             (unsigned char)mac[0],
		             (unsigned char)mac[1],
		             (unsigned char)mac[2],
		             (unsigned char)mac[3],
		             (unsigned char)mac[4],
		             (unsigned char)mac[5],
		             gpio_selected(supla_esp_cfg.Relay1_GPIO),
		             gpio_selected(supla_esp_cfg.Relay2_GPIO),
		             gpio_selected(supla_esp_cfg.Button1_GPIO),
		             gpio_selected(supla_esp_cfg.Button2_GPIO),
		             gpio_selected(supla_esp_cfg.Sensor1_GPIO),
		             gpio_selected(supla_esp_cfg.Sensor2_GPIO),
		             gpio_selected(supla_esp_cfg.Thermometer_GPIO),
		             gpio_selected(supla_esp_cfg.BtnConfig_GPIO),
		             gpio_selected(supla_esp_cfg.LedConfig_GPIO),
		             supla_esp_cfg.RollerShutter == ROLLER_SHUTTER_OFF ? "selected" : "",
		             supla_esp_cfg.RollerShutter == ROLLER_SHUTTER_ON ? "selected" : "",
		             supla_esp_cfg.ThermometerType == THERMOMETER_TYPE_DS18B20 ? "selected" : "",
		             supla_esp_cfg.ThermometerType == THERMOMETER_TYPE_DHT11 ? "selected" : "",
		             supla_esp_cfg.ThermometerType == THERMOMETER_TYPE_DHT22 ? "selected" : "",
		             supla_esp_cfg.Sensor1Type == SNR_TYPE_NO ? "selected" : "",
		             supla_esp_cfg.Sensor1Type == SNR_TYPE_NC ? "selected" : "",
		             supla_esp_cfg.Sensor2Type == SNR_TYPE_NO ? "selected" : "",
		             supla_esp_cfg.Sensor2Type == SNR_TYPE_NC ? "selected" : "",
		             supla_esp_cfg.Button1Type == BTN_TYPE_BISTABLE ? "selected" : "",
		             supla_esp_cfg.Button1Type == BTN_TYPE_MONOSTABLE ? "selected" : "",
		             supla_esp_cfg.Button2Type == BTN_TYPE_BISTABLE ? "selected" : "",
		             supla_esp_cfg.Button2Type == BTN_TYPE_MONOSTABLE ? "selected" : ""
		             );

	}
	if( supla_esp_cfg.StronaHTML == 2) {

		int bufflen = strlen(supla_esp_devconn_laststate())
		              +strlen(dev_name)
		              +strlen(SUPLA_ESP_SOFTVER)
		              +strlen(html_template_header_inCan())
		              +strlen(html_template_inCan(supla_esp_cfg.StronaHTML))
		              +200;

		buffer = (char*)os_malloc(bufflen);

		ets_snprintf(buffer,
		             bufflen,
		             html_template_inCan(supla_esp_cfg.StronaHTML),
		             html_template_header_inCan(),
		             data_saved == 1 ? "" : "",
		             dev_name,
		             supla_esp_devconn_laststate(),
		             SUPLA_ESP_SOFTVER,
		             (unsigned char)supla_esp_cfg.GUID[0],
		             (unsigned char)supla_esp_cfg.GUID[1],
		             (unsigned char)supla_esp_cfg.GUID[2],
		             (unsigned char)supla_esp_cfg.GUID[3],
		             (unsigned char)supla_esp_cfg.GUID[4],
		             (unsigned char)supla_esp_cfg.GUID[5],
		             (unsigned char)supla_esp_cfg.GUID[6],
		             (unsigned char)supla_esp_cfg.GUID[7],
		             (unsigned char)supla_esp_cfg.GUID[8],
		             (unsigned char)supla_esp_cfg.GUID[9],
		             (unsigned char)supla_esp_cfg.GUID[10],
		             (unsigned char)supla_esp_cfg.GUID[11],
		             (unsigned char)supla_esp_cfg.GUID[12],
		             (unsigned char)supla_esp_cfg.GUID[13],
		             (unsigned char)supla_esp_cfg.GUID[14],
		             (unsigned char)supla_esp_cfg.GUID[15],
		             (unsigned char)mac[0],
		             (unsigned char)mac[1],
		             (unsigned char)mac[2],
		             (unsigned char)mac[3],
		             (unsigned char)mac[4],
		             (unsigned char)mac[5],
		             supla_esp_cfg.RollerShutter == ROLLER_SHUTTER_OFF ? "selected" : "",
		             supla_esp_cfg.RollerShutter == ROLLER_SHUTTER_ON ? "selected" : "",
		             supla_esp_cfg.Relay1Level == RELAY_LOW_LEVEL ? "selected" : "",
		             supla_esp_cfg.Relay1Level == RELAY_HIGH_LEVEL ? "selected" : "",
		             supla_esp_cfg.Relay2Level == RELAY_LOW_LEVEL ? "selected" : "",
		             supla_esp_cfg.Relay2Level == RELAY_HIGH_LEVEL ? "selected" : "",
		             supla_esp_cfg.Button1Type == BTN_TYPE_BISTABLE ? "selected" : "",
		             supla_esp_cfg.Button1Type == BTN_TYPE_MONOSTABLE ? "selected" : "",
		             supla_esp_cfg.Button2Type == BTN_TYPE_BISTABLE ? "selected" : "",
		             supla_esp_cfg.Button2Type == BTN_TYPE_MONOSTABLE ? "selected" : "",
		             supla_esp_cfg.Sensor1Type == SNR_TYPE_NO ? "selected" : "",
		             supla_esp_cfg.Sensor1Type == SNR_TYPE_NC ? "selected" : "",
		             supla_esp_cfg.Sensor2Type == SNR_TYPE_NO ? "selected" : "",
		             supla_esp_cfg.Sensor2Type == SNR_TYPE_NC ? "selected" : "",
		             supla_esp_cfg.ThermometerType == THERMOMETER_TYPE_DS18B20 ? "selected" : "",
		             supla_esp_cfg.ThermometerType == THERMOMETER_TYPE_DHT11 ? "selected" : "",
		             supla_esp_cfg.ThermometerType == THERMOMETER_TYPE_DHT22 ? "selected" : "",
		             supla_esp_cfg.CfgButtonType == BTN_TYPE_BISTABLE ? "selected" : "",
		             supla_esp_cfg.CfgButtonType == BTN_TYPE_MONOSTABLE ? "selected" : ""
		             );

	}

//****************************************KONIEC dla modulu inCanEASY*********************************
//****************************************dla modulu MULTIBOARD_TYPE_SELECTION****************************************
		#elif defined(MULTIBOARD_TYPE_SELECTION)

	int bufflen;
	if (supla_esp_cfg.BoardNames == 0) {
		bufflen = strlen(supla_esp_devconn_laststate())
		          +strlen(dev_name)
		          +strlen(SUPLA_ESP_SOFTVER)
		          +strlen(board_start(supla_esp_cfg.WIFI_SSID, supla_esp_cfg.Server, supla_esp_cfg.Email, supla_esp_cfg.HostName))
		          +strlen(html_template_header_inCan())
		          +strlen(html_template_inCan(HTML_STRONA_START))
		          +strlen(board_selected(supla_esp_cfg.BoardNames))
		          +strlen(thermometer_selected(supla_esp_cfg.ThermometerType))
		          +strlen(button_selected(supla_esp_cfg.Button1Type, 1))
		          +strlen(button_selected(supla_esp_cfg.Button2Type, 2))
		          +strlen(button_selected(supla_esp_cfg.Button3Type, 3))
		          +strlen(button_selected(supla_esp_cfg.Button4Type, 4))
		          +strlen(button_selected(supla_esp_cfg.Button5Type, 5))
		          +strlen(ralay_level_selected())
		          +strlen(led_status(supla_esp_cfg.StatusLedOff))
		          +strlen(button_configure_selected(supla_esp_cfg.CfgButtonType))
		          +strlen(gpio_user_configurable_selected())
		          +200;

		buffer = (char*)os_malloc(bufflen);

		ets_snprintf(buffer,
		             bufflen,
		             html_template_inCan(HTML_STRONA_START),
		             html_template_header_inCan(),
		             data_saved == 1 ? "<div id=\"msg\" class=\"c\">Data saved</div>" : "",
		             dev_name,
		             supla_esp_devconn_laststate(),
		             SUPLA_ESP_SOFTVER,
		             (unsigned char)supla_esp_cfg.GUID[0],
		             (unsigned char)supla_esp_cfg.GUID[1],
		             (unsigned char)supla_esp_cfg.GUID[2],
		             (unsigned char)supla_esp_cfg.GUID[3],
		             (unsigned char)supla_esp_cfg.GUID[4],
		             (unsigned char)supla_esp_cfg.GUID[5],
		             (unsigned char)supla_esp_cfg.GUID[6],
		             (unsigned char)supla_esp_cfg.GUID[7],
		             (unsigned char)supla_esp_cfg.GUID[8],
		             (unsigned char)supla_esp_cfg.GUID[9],
		             (unsigned char)supla_esp_cfg.GUID[10],
		             (unsigned char)supla_esp_cfg.GUID[11],
		             (unsigned char)supla_esp_cfg.GUID[12],
		             (unsigned char)supla_esp_cfg.GUID[13],
		             (unsigned char)supla_esp_cfg.GUID[14],
		             (unsigned char)supla_esp_cfg.GUID[15],
		             (unsigned char)mac[0],
		             (unsigned char)mac[1],
		             (unsigned char)mac[2],
		             (unsigned char)mac[3],
		             (unsigned char)mac[4],
		             (unsigned char)mac[5],
		             " ", //board_start(supla_esp_cfg.WIFI_SSID, supla_esp_cfg.Server, supla_esp_cfg.Email, supla_esp_cfg.HostName),
		             board_selected(supla_esp_cfg.BoardNames),
		             " ",// thermometer_selected(supla_esp_cfg.ThermometerType),
		             " ",// button_selected(supla_esp_cfg.Button1Type, 1),
		             " ",// button_selected(supla_esp_cfg.Button2Type, 2),
		             " ",// button_selected(supla_esp_cfg.Button3Type, 3),
		             " ",// button_selected(supla_esp_cfg.Button4Type, 4),
		             " ",// button_selected(supla_esp_cfg.Button5Type, 5),
		             " ",// ralay_level_selected(),
		             " ",// led_status(supla_esp_cfg.StatusLedOff),
		             " ",// button_configure_selected(supla_esp_cfg.CfgButtonType),
		             " "// gpio_user_configurable_selected()
		             );
	}
	else {
		int bufflen = strlen(supla_esp_devconn_laststate())
		              +strlen(dev_name)
		              +strlen(SUPLA_ESP_SOFTVER)
		              // +strlen(supla_esp_cfg.WIFI_SSID)
		              // +strlen(supla_esp_cfg.Server)
		              // +strlen(supla_esp_cfg.Email)
		              +strlen(board_start(supla_esp_cfg.WIFI_SSID, supla_esp_cfg.Server, supla_esp_cfg.Email, supla_esp_cfg.HostName))
		              +strlen(html_template_header_inCan())
		              +strlen(html_template_inCan(HTML_STRONA_START))
		              +strlen(board_selected(supla_esp_cfg.BoardNames))
		              +strlen(thermometer_selected(supla_esp_cfg.ThermometerType))
		              +strlen(button_selected(supla_esp_cfg.Button1Type, 1))
		              +strlen(button_selected(supla_esp_cfg.Button2Type, 2))
		              +strlen(button_selected(supla_esp_cfg.Button3Type, 3))
		              +strlen(button_selected(supla_esp_cfg.Button4Type, 4))
		              +strlen(button_selected(supla_esp_cfg.Button5Type, 5))
		              +strlen(ralay_level_selected())
		              +strlen(led_status(supla_esp_cfg.StatusLedOff))
		              +strlen(button_configure_selected(supla_esp_cfg.CfgButtonType))
		              +strlen(gpio_user_configurable_selected())
		              +400;

		buffer = (char*)os_malloc(bufflen);

		ets_snprintf(buffer,
		             bufflen,
		             html_template_inCan(HTML_STRONA_START),
		             html_template_header_inCan(),
		             data_saved == 1 ? "<div id=\"msg\" class=\"c\">Data saved</div>" : "",
		             dev_name,
		             supla_esp_devconn_laststate(),
		             SUPLA_ESP_SOFTVER,
		             (unsigned char)supla_esp_cfg.GUID[0],
		             (unsigned char)supla_esp_cfg.GUID[1],
		             (unsigned char)supla_esp_cfg.GUID[2],
		             (unsigned char)supla_esp_cfg.GUID[3],
		             (unsigned char)supla_esp_cfg.GUID[4],
		             (unsigned char)supla_esp_cfg.GUID[5],
		             (unsigned char)supla_esp_cfg.GUID[6],
		             (unsigned char)supla_esp_cfg.GUID[7],
		             (unsigned char)supla_esp_cfg.GUID[8],
		             (unsigned char)supla_esp_cfg.GUID[9],
		             (unsigned char)supla_esp_cfg.GUID[10],
		             (unsigned char)supla_esp_cfg.GUID[11],
		             (unsigned char)supla_esp_cfg.GUID[12],
		             (unsigned char)supla_esp_cfg.GUID[13],
		             (unsigned char)supla_esp_cfg.GUID[14],
		             (unsigned char)supla_esp_cfg.GUID[15],
		             (unsigned char)mac[0],
		             (unsigned char)mac[1],
		             (unsigned char)mac[2],
		             (unsigned char)mac[3],
		             (unsigned char)mac[4],
		             (unsigned char)mac[5],
		             // supla_esp_cfg.WIFI_SSID,
		             // supla_esp_cfg.Server,
		             // supla_esp_cfg.Email,
		             board_start(supla_esp_cfg.WIFI_SSID, supla_esp_cfg.Server, supla_esp_cfg.Email, supla_esp_cfg.HostName),
		             board_selected(supla_esp_cfg.BoardNames),
		             thermometer_selected(supla_esp_cfg.ThermometerType),
		             button_selected(supla_esp_cfg.Button1Type, 1),
		             button_selected(supla_esp_cfg.Button2Type, 2),
		             button_selected(supla_esp_cfg.Button3Type, 3),
		             button_selected(supla_esp_cfg.Button4Type, 4),
		             button_selected(supla_esp_cfg.Button5Type, 5),
		             ralay_level_selected(),
		             led_status(supla_esp_cfg.StatusLedOff),
		             button_configure_selected(supla_esp_cfg.CfgButtonType),
		             gpio_user_configurable_selected()


		 #ifdef __FOTA
		             ,
		             supla_esp_cfg.FirmwareUpdate == 0 ? "selected" : "",
		             supla_esp_cfg.FirmwareUpdate == 1 ? "selected" : ""
		 #endif
		             );

	}
//****************************************KONIEC dla modulu MULTIBOARD_TYPE_SELECTION*************************************

	#else

	char html_template_header[] = "<!DOCTYPE html><meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"><meta name=\"viewport\" content=\"width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no\"><style>body{font-size:14px;font-family:HelveticaNeue,\"Helvetica Neue\",HelveticaNeueRoman,HelveticaNeue-Roman,\"Helvetica Neue Roman\",TeXGyreHerosRegular,Helvetica,Tahoma,Geneva,Arial,sans-serif;font-weight:400;font-stretch:normal;background:#00d151;color:#fff;line-height:20px;padding:0}.s{width:460px;margin:0 auto;margin-top:calc(50vh - 340px);border:solid 3px #fff;padding:0 10px 10px;border-radius:3px}#l{display:block;max-width:150px;height:155px;margin:-80px auto 20px;background:#00d151;padding-right:5px}#l path{fill:#000}.w{margin:3px 0 16px;padding:5px 0;border-radius:3px;background:#fff;box-shadow:0 1px 3px rgba(0,0,0,.3)}h1,h3{margin:10px 8px;font-family:HelveticaNeueLight,HelveticaNeue-Light,\"Helvetica Neue Light\",HelveticaNeue,\"Helvetica Neue\",TeXGyreHerosRegular,Helvetica,Tahoma,Geneva,Arial,sans-serif;font-weight:300;font-stretch:normal;color:#000;font-size:23px}h1{margin-bottom:14px;color:#fff}span{display:block;margin:10px 7px 14px}i{display:block;font-style:normal;position:relative;border-bottom:solid 1px #00d151;height:42px}i:last-child{border:none}label{position:absolute;display:inline-block;top:0;left:8px;color:#00d151;line-height:41px;pointer-events:none}input,select{width:calc(100% - 145px);border:none;font-size:16px;line-height:40px;border-radius:0;letter-spacing:-.5px;background:#fff;color:#000;padding-left:144px;-webkit-appearance:none;-moz-appearance:none;appearance:none;outline:0!important;height:40px}select{padding:0;float:right;margin:1px 3px 1px 2px}button{width:100%;border:0;background:#000;padding:5px 10px;font-size:16px;line-height:40px;color:#fff;border-radius:3px;box-shadow:0 1px 3px rgba(0,0,0,.3);cursor:pointer}.c{background:#ffe836;position:fixed;width:100%;line-height:80px;color:#000;top:0;left:0;box-shadow:0 1px 3px rgba(0,0,0,.3);text-align:center;font-size:26px;z-index:100}@media all and (max-height:920px){.s{margin-top:80px}}@media all and (max-width:900px){.s{width:calc(100% - 20px);margin-top:40px;border:none;padding:0 8px;border-radius:0}#l{max-width:80px;height:auto;margin:10px auto 20px}h1,h3{font-size:19px}i{border:none;height:auto}label{display:block;margin:4px 0 12px;color:#00d151;font-size:13px;position:relative;line-height:18px}input,select{width:calc(100% - 10px);font-size:16px;line-height:28px;padding:0 5px;border-bottom:solid 1px #00d151}select{width:100%;float:none;margin:0}}</style><script type=\"text/javascript\">setTimeout(function(){var element =  document.getElementById('msg');if ( element != null ) element.style.visibility = \"hidden\";},3200);</script>";

		#ifdef __FOTA
	char html_template[] = "%s%s<div class=\"s\"><svg version=\"1.1\" id=\"l\" x=\"0\" y=\"0\" viewBox=\"0 0 200 200\" xml:space=\"preserve\"><path d=\"M59.3,2.5c18.1,0.6,31.8,8,40.2,23.5c3.1,5.7,4.3,11.9,4.1,18.3c-0.1,3.6-0.7,7.1-1.9,10.6c-0.2,0.7-0.1,1.1,0.6,1.5c12.8,7.7,25.5,15.4,38.3,23c2.9,1.7,5.8,3.4,8.7,5.3c1,0.6,1.6,0.6,2.5-0.1c4.5-3.6,9.8-5.3,15.7-5.4c12.5-0.1,22.9,7.9,25.2,19c1.9,9.2-2.9,19.2-11.8,23.9c-8.4,4.5-16.9,4.5-25.5,0.2c-0.7-0.3-1-0.2-1.5,0.3c-4.8,4.9-9.7,9.8-14.5,14.6c-5.3,5.3-10.6,10.7-15.9,16c-1.8,1.8-3.6,3.7-5.4,5.4c-0.7,0.6-0.6,1,0,1.6c3.6,3.4,5.8,7.5,6.2,12.2c0.7,7.7-2.2,14-8.8,18.5c-12.3,8.6-30.3,3.5-35-10.4c-2.8-8.4,0.6-17.7,8.6-22.8c0.9-0.6,1.1-1,0.8-2c-2-6.2-4.4-12.4-6.6-18.6c-6.3-17.6-12.7-35.1-19-52.7c-0.2-0.7-0.5-1-1.4-0.9c-12.5,0.7-23.6-2.6-33-10.4c-8-6.6-12.9-15-14.2-25c-1.5-11.5,1.7-21.9,9.6-30.7C32.5,8.9,42.2,4.2,53.7,2.7c0.7-0.1,1.5-0.2,2.2-0.2C57,2.4,58.2,2.5,59.3,2.5z M76.5,81c0,0.1,0.1,0.3,0.1,0.6c1.6,6.3,3.2,12.6,4.7,18.9c4.5,17.7,8.9,35.5,13.3,53.2c0.2,0.9,0.6,1.1,1.6,0.9c5.4-1.2,10.7-0.8,15.7,1.6c0.8,0.4,1.2,0.3,1.7-0.4c11.2-12.9,22.5-25.7,33.4-38.7c0.5-0.6,0.4-1,0-1.6c-5.6-7.9-6.1-16.1-1.3-24.5c0.5-0.8,0.3-1.1-0.5-1.6c-9.1-4.7-18.1-9.3-27.2-14c-6.8-3.5-13.5-7-20.3-10.5c-0.7-0.4-1.1-0.3-1.6,0.4c-1.3,1.8-2.7,3.5-4.3,5.1c-4.2,4.2-9.1,7.4-14.7,9.7C76.9,80.3,76.4,80.3,76.5,81z M89,42.6c0.1-2.5-0.4-5.4-1.5-8.1C83,23.1,74.2,16.9,61.7,15.8c-10-0.9-18.6,2.4-25.3,9.7c-8.4,9-9.3,22.4-2.2,32.4c6.8,9.6,19.1,14.2,31.4,11.9C79.2,67.1,89,55.9,89,42.6z M102.1,188.6c0.6,0.1,1.5-0.1,2.4-0.2c9.5-1.4,15.3-10.9,11.6-19.2c-2.6-5.9-9.4-9.6-16.8-8.6c-8.3,1.2-14.1,8.9-12.4,16.6C88.2,183.9,94.4,188.6,102.1,188.6z M167.7,88.5c-1,0-2.1,0.1-3.1,0.3c-9,1.7-14.2,10.6-10.8,18.6c2.9,6.8,11.4,10.3,19,7.8c7.1-2.3,11.1-9.1,9.6-15.9C180.9,93,174.8,88.5,167.7,88.5z\"/></svg><h1>%s</h1><span>LAST STATE: %s<br>Firmware: %s<br>GUID: %02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X<br>MAC: %02X:%02X:%02X:%02X:%02X:%02X</span><form method=\"post\"><div class=\"w\"><h3>Wi-Fi Settings</h3><i><input name=\"sid\" value=\"%s\"><label>Network name</label></i><i><input name=\"wpw\"><label>Password</label></i></div><div class=\"w\"><h3>Supla Settings</h3><i><input name=\"svr\" value=\"%s\"><label>Server</label></i><i><input name=\"eml\" value=\"%s\"><label>E-mail</label></i></div><div class=\"w\"><h3>Additional Settings</h3><i><select name=\"upd\"><option value=\"0\" %s>NO<option value=\"1\" %s>YES</select><label>Firmware update</label></i></div><button type=\"submit\">SAVE</button></form></div><br><br>";
		#else
	char html_template[] = "%s%s<div class=\"s\"><svg version=\"1.1\" id=\"l\" x=\"0\" y=\"0\" viewBox=\"0 0 200 200\" xml:space=\"preserve\"><path d=\"M59.3,2.5c18.1,0.6,31.8,8,40.2,23.5c3.1,5.7,4.3,11.9,4.1,18.3c-0.1,3.6-0.7,7.1-1.9,10.6c-0.2,0.7-0.1,1.1,0.6,1.5c12.8,7.7,25.5,15.4,38.3,23c2.9,1.7,5.8,3.4,8.7,5.3c1,0.6,1.6,0.6,2.5-0.1c4.5-3.6,9.8-5.3,15.7-5.4c12.5-0.1,22.9,7.9,25.2,19c1.9,9.2-2.9,19.2-11.8,23.9c-8.4,4.5-16.9,4.5-25.5,0.2c-0.7-0.3-1-0.2-1.5,0.3c-4.8,4.9-9.7,9.8-14.5,14.6c-5.3,5.3-10.6,10.7-15.9,16c-1.8,1.8-3.6,3.7-5.4,5.4c-0.7,0.6-0.6,1,0,1.6c3.6,3.4,5.8,7.5,6.2,12.2c0.7,7.7-2.2,14-8.8,18.5c-12.3,8.6-30.3,3.5-35-10.4c-2.8-8.4,0.6-17.7,8.6-22.8c0.9-0.6,1.1-1,0.8-2c-2-6.2-4.4-12.4-6.6-18.6c-6.3-17.6-12.7-35.1-19-52.7c-0.2-0.7-0.5-1-1.4-0.9c-12.5,0.7-23.6-2.6-33-10.4c-8-6.6-12.9-15-14.2-25c-1.5-11.5,1.7-21.9,9.6-30.7C32.5,8.9,42.2,4.2,53.7,2.7c0.7-0.1,1.5-0.2,2.2-0.2C57,2.4,58.2,2.5,59.3,2.5z M76.5,81c0,0.1,0.1,0.3,0.1,0.6c1.6,6.3,3.2,12.6,4.7,18.9c4.5,17.7,8.9,35.5,13.3,53.2c0.2,0.9,0.6,1.1,1.6,0.9c5.4-1.2,10.7-0.8,15.7,1.6c0.8,0.4,1.2,0.3,1.7-0.4c11.2-12.9,22.5-25.7,33.4-38.7c0.5-0.6,0.4-1,0-1.6c-5.6-7.9-6.1-16.1-1.3-24.5c0.5-0.8,0.3-1.1-0.5-1.6c-9.1-4.7-18.1-9.3-27.2-14c-6.8-3.5-13.5-7-20.3-10.5c-0.7-0.4-1.1-0.3-1.6,0.4c-1.3,1.8-2.7,3.5-4.3,5.1c-4.2,4.2-9.1,7.4-14.7,9.7C76.9,80.3,76.4,80.3,76.5,81z M89,42.6c0.1-2.5-0.4-5.4-1.5-8.1C83,23.1,74.2,16.9,61.7,15.8c-10-0.9-18.6,2.4-25.3,9.7c-8.4,9-9.3,22.4-2.2,32.4c6.8,9.6,19.1,14.2,31.4,11.9C79.2,67.1,89,55.9,89,42.6z M102.1,188.6c0.6,0.1,1.5-0.1,2.4-0.2c9.5-1.4,15.3-10.9,11.6-19.2c-2.6-5.9-9.4-9.6-16.8-8.6c-8.3,1.2-14.1,8.9-12.4,16.6C88.2,183.9,94.4,188.6,102.1,188.6z M167.7,88.5c-1,0-2.1,0.1-3.1,0.3c-9,1.7-14.2,10.6-10.8,18.6c2.9,6.8,11.4,10.3,19,7.8c7.1-2.3,11.1-9.1,9.6-15.9C180.9,93,174.8,88.5,167.7,88.5z\"/></svg><h1>%s</h1><span>LAST STATE: %s<br>Firmware: %s<br>GUID: %02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X<br>MAC: %02X:%02X:%02X:%02X:%02X:%02X</span><form method=\"post\"><div class=\"w\"><h3>Wi-Fi Settings</h3><i><input name=\"sid\" value=\"%s\"><label>Network name</label></i><i><input name=\"wpw\"><label>Password</label></i></div><div class=\"w\"><h3>Supla Settings</h3><i><input name=\"svr\" value=\"%s\"><label>Server</label></i><i><input name=\"eml\" value=\"%s\"><label>E-mail</label></i></div><button type=\"submit\">SAVE</button></form></div><br><br>";
		#endif

	int bufflen = strlen(supla_esp_devconn_laststate())
	              +strlen(dev_name)
	              +strlen(SUPLA_ESP_SOFTVER)
	              +strlen(supla_esp_cfg.WIFI_SSID)
	              +strlen(supla_esp_cfg.Server)
	              +strlen(supla_esp_cfg.Email)
	              +strlen(html_template_header)
	              +strlen(html_template)
	              +200;

	buffer = (char*) malloc(bufflen);

	ets_snprintf(buffer,
	             bufflen,
	             html_template,
	             html_template_header,
	             data_saved == 1 ? "<div id=\"msg\" class=\"c\">Data saved</div>" : "",
	             dev_name,
	             supla_esp_devconn_laststate(),
	             SUPLA_ESP_SOFTVER,
	             (unsigned char)supla_esp_cfg.GUID[0],
	             (unsigned char)supla_esp_cfg.GUID[1],
	             (unsigned char)supla_esp_cfg.GUID[2],
	             (unsigned char)supla_esp_cfg.GUID[3],
	             (unsigned char)supla_esp_cfg.GUID[4],
	             (unsigned char)supla_esp_cfg.GUID[5],
	             (unsigned char)supla_esp_cfg.GUID[6],
	             (unsigned char)supla_esp_cfg.GUID[7],
	             (unsigned char)supla_esp_cfg.GUID[8],
	             (unsigned char)supla_esp_cfg.GUID[9],
	             (unsigned char)supla_esp_cfg.GUID[10],
	             (unsigned char)supla_esp_cfg.GUID[11],
	             (unsigned char)supla_esp_cfg.GUID[12],
	             (unsigned char)supla_esp_cfg.GUID[13],
	             (unsigned char)supla_esp_cfg.GUID[14],
	             (unsigned char)supla_esp_cfg.GUID[15],
	             (unsigned char)mac[0],
	             (unsigned char)mac[1],
	             (unsigned char)mac[2],
	             (unsigned char)mac[3],
	             (unsigned char)mac[4],
	             (unsigned char)mac[5],
	             supla_esp_cfg.WIFI_SSID,
	             supla_esp_cfg.Server,
	             supla_esp_cfg.Email
				#ifdef __FOTA
	             ,
	             supla_esp_cfg.FirmwareUpdate == 0 ? "selected" : "",
	             supla_esp_cfg.FirmwareUpdate == 1 ? "selected" : ""
				#endif
	             );

	#endif

	#endif /*BOARD_CFG_HTML_TEMPLATE*/

	supla_log(LOG_DEBUG, "HTTP OK (%i) Free heap size: %i", buffer!=NULL, system_get_free_heap_size());

	if ( buffer ) {
		supla_esp_http_ok((struct espconn *)arg, buffer);
		free(buffer);
		free(start_settings);

		free(start_settings);
		free(board_name);
		free(thermometer_name);
		free(button_type);
		free(relay_level);
		free(led_select);
		free(button_cfg_type);
		free(pin_type);
		// supla_log(LOG_DEBUG, "system_get_free_heap_size: %i", system_get_free_heap_size());

	}


}


void ICACHE_FLASH_ATTR
supla_esp_discon_callback(void *arg) {

	supla_log(LOG_DEBUG, "Disconnect");

	struct espconn *conn = (struct espconn *)arg;

	if ( conn->reverse != NULL ) {
		free(conn->reverse);
		conn->reverse = NULL;
	}
}

void ICACHE_FLASH_ATTR
supla_esp_connectcb(void *arg)
{
	struct espconn *conn = (struct espconn *)arg;

	espconn_set_opt(conn, ESPCONN_COPY);

	TrivialHttpParserVars *pVars = malloc(sizeof(TrivialHttpParserVars));
	memset(pVars, 0, sizeof(TrivialHttpParserVars));
	conn->reverse = pVars;

	espconn_regist_recvcb(conn, supla_esp_recv_callback );
	espconn_regist_disconcb(conn, supla_esp_discon_callback);
}


void ICACHE_FLASH_ATTR
supla_esp_cfgmode_start(void) {

	char APSSID[] = AP_SSID;
	char mac[6];

	#ifdef BOARD_BEFORE_CFGMODE_START
	supla_esp_board_before_cfgmode_start();
	#endif

	supla_esp_devconn_before_cfgmode_start();

	wifi_get_macaddr(SOFTAP_IF, (unsigned char*)mac);

	struct softap_config apconfig;
	wifi_softap_get_config(&apconfig);

	struct espconn *conn;

	if ( supla_esp_cfgmode_entertime != 0 )
		return;

	supla_esp_devconn_stop();

	supla_esp_cfgmode_entertime = system_get_time();

	supla_log(LOG_DEBUG, "ENTER CFG MODE");
	supla_esp_cfg.StronaHTML = 0;
	supla_esp_gpio_state_cfgmode();

	#ifdef WIFI_SLEEP_DISABLE
	wifi_set_sleep_type(NONE_SLEEP_T);
	#endif

	int apssid_len = strlen(APSSID);

//#if defined(INCAN_TYPE_SELECTION)

//#else
	if ( apssid_len+14 > 32 )
		apssid_len = 18;
//#endif
	memcpy(apconfig.ssid, APSSID, apssid_len);
//#if defined(INCAN_TYPE_SELECTION)

//#else

	ets_snprintf((char*)&apconfig.ssid[apssid_len],
	             14,
	             "-%02X%02X%02X%02X%02X%02X",
	             (unsigned char)mac[0],
	             (unsigned char)mac[1],
	             (unsigned char)mac[2],
	             (unsigned char)mac[3],
	             (unsigned char)mac[4],
	             (unsigned char)mac[5]);

//#endif
	apconfig.password[0] = 0;
//#if defined(INCAN_TYPE_SELECTION)
//	apconfig.ssid_len = apssid_len;
//#else
	apconfig.ssid_len = apssid_len+13;
//#endif
	apconfig.channel = 1;
	apconfig.authmode = AUTH_OPEN;
	apconfig.ssid_hidden = 0;
	apconfig.max_connection = 4;
	apconfig.beacon_interval = 100;

	wifi_set_opmode(SOFTAP_MODE);
	wifi_softap_set_config(&apconfig);

	conn = (struct espconn *)malloc(sizeof(struct espconn));
	memset( conn, 0, sizeof( struct espconn ) );

	espconn_create(conn);
	espconn_regist_time(conn, 5, 0);

	conn->type = ESPCONN_TCP;
	conn->state = ESPCONN_NONE;

	conn->proto.tcp = (esp_tcp *)zalloc(sizeof(esp_tcp));
	conn->proto.tcp->local_port = 80;

	espconn_regist_connectcb(conn, supla_esp_connectcb);
	espconn_accept(conn);


}

char ICACHE_FLASH_ATTR
supla_esp_cfgmode_started(void) {

	return supla_esp_cfgmode_entertime == 0 ? 0 : 1;
}
