
#include <os_type.h>
#include <osapi.h>
#include <eagle_soc.h>
#include <ets_sys.h>
#include <gpio.h>
#include "stdlib_noniso.h"
#include "supla-dev/log.h"

#include "supla_w1.h"
#include "supla_ds18b20.h"
#include "supla_dht.h"
#include "supla_temperatura.h"
#include "supla_esp_devconn.h"
#include "supla_esp_cfg.h"
#include "supla_esp.h"

#include "supla_esp.h"
#include "supla-dev/proto.h"


ETSTimer supla_temperatura_timer1;

void DS18B20_ICACHE_FLASH supla_change_termometr_th(void *timer_arg){

	supla_change_termometr ^= 1;
	if (supla_change_termometr == 0) {
		// supla_log(LOG_DEBUG, "supla_ds18b20_start");
		supla_ds18b20_start_2();
	}
	else if (supla_change_termometr == 1) {
		supla_ds18b20_stop();
		// supla_log(LOG_DEBUG, "supla_DHT_start");
		supla_dht_start_2();
	}
	// else supla_log(LOG_DEBUG, "**************");

	// switch (supla_change_termometr) {
	// case 0:
	//      supla_log(LOG_DEBUG, "supla_ds18b20_start");
	//      supla_ds18b20_start_2();
	//
	//      break;
	// case 1:
	//      supla_ds18b20_stop();
	//      supla_log(LOG_DEBUG, "supla_DHT_start");
	//      supla_dht_start_2();
	//      break;
	// }

}

void DS18B20_ICACHE_FLASH supla_temperatura_start(void){
	if (nr_temperature >= 1 && nr_temperature != 252 ) {
		os_timer_disarm(&supla_temperatura_timer1);
		os_timer_setfn(&supla_temperatura_timer1, supla_change_termometr_th, NULL);
		os_timer_arm (&supla_temperatura_timer1, 3150, 1);
	}
}

void DS18B20_ICACHE_FLASH supla_check_temperatura(void){
	if (supla_check_dht()) {
		supla_esp_cfg.ThermometerTypeDHT = THERMOMETER_TYPE_DHT22;
		supla_esp_cfg.ThermometerType = THERMOMETER_TYPE_DHT22;
		// supla_log(LOG_DEBUG, "THERMOMETER_TYPE_DHT22");
	}
	else if (supla_check_ds18b20()) {
		supla_esp_cfg.ThermometerType = THERMOMETER_TYPE_DS18B20;
		// supla_log(LOG_DEBUG, "THERMOMETER_TYPE_DS18B20");
	}
	else {
		supla_esp_cfg.ThermometerType = THERMOMETER_TYPE_NONE;
		// supla_log(LOG_DEBUG, "THERMOMETER_TYPE_NONE");
	}
}
