/*
   ============================================================================
   Name        : supla_esp_board_set.h
   Author      : Espablo
   Copyright   : GPLv2
   ============================================================================
 */

#ifndef SUPLA_ESP_BOARD_SET_H_
#define SUPLA_ESP_BOARD_SET_H_

#include "supla_esp.h"


#define GPIO                    0
#define CHANNELS                1
#define B_WEJSCIE               3

#define MAX_LED_CONFIG_SUPORT   3
#define MAX_TEMP_SENSOR_SUPORT  4
#define GPIO_INPUT              0
#define GPIO_OUTPUT             1
#define SUPLA_HOSTNAME_MAXSIZE  101

#define BUTTON_CONFIGURE_5s INPUT_TYPE_BTN_MONOSTABLE
#define BUTTON_CONFIGURE_x10 INPUT_TYPE_BTN_BISTABLE

#define LED_LOW_LEVEL						0
#define LED_HIGH_LEVEL					1
#define ONLY_SOCKET_CHANNEL     1

uint8 switch_chanel_gpio;
uint8 input_counter;
uint8 relay_counter;
int new_channel;
int new_nr_channel;
uint8 led_set;
int led_relay;
uint8 button_configure_set;
uint8 uart_tx_io;
uint8 uart_rx_io;

uint8 nr_channel;
uint8 nr_button;
uint8 nr_sensor;
uint8 nr_temperature;
uint8 nr_module;
uint8 _nr_module;
uint8 nr_pin_configurable;
uint8 nr_relay;

uint8 temperature_channel;
uint8 temperature_and_humidity_channel;
uint8 sensor_channel;
uint8 socket_channel;
uint8 phaze50Hz;

enum {
	NONE,
	EXTERNAL_RELAY,
	// EXTERNAL_BUTTON,
	SENSOR,
	TERM_DS18B20,
	TERM_DHT11,
	TERM_DHT22,
	// I2C_SCL,
	// I2C_SDA,
	END_USER_CONFIGURABLE
};
const char * NodeMCU_GPIO[17];
const char * Supported_RelayLevel[2];
const char * Supported_RelayFlag[3];


void ICACHE_FLASH_ATTR supla_esp_board_set_channels_gpio(TDS_SuplaDeviceChannel_B *channel_set, uint8 channels_gpio);
void check_gpio_for_uart_and_pullup(uint8 check_gpio, uint8 inout);
// int supla_choice_thermometer_type(int type);

void ICACHE_FLASH_ATTR Setting_Up_Button( uint8 button_gpio, uint8 relay_gpio, uint8 monobistable);
void ICACHE_FLASH_ATTR Setting_Up_Relay(uint8 relay_gpio, TDS_SuplaDeviceChannel_B *channels);
void ICACHE_FLASH_ATTR Setting_Up_Button_Configure(uint8 gpio, uint8 stable);
void ICACHE_FLASH_ATTR Setting_Up_Buttons_RS( uint8 gpio_buton_up, uint8 gpio_buton_down, uint8 relay_up, uint8 relay_down, uint8 mono_bistable, TDS_SuplaDeviceChannel_B *channels);
void ICACHE_FLASH_ATTR Setting_Up_Relay_RS(uint8 relay_up, uint8 relay_down, TDS_SuplaDeviceChannel_B *channels);
void ICACHE_FLASH_ATTR Setting_Up_Temperature(uint8 gpio, int type, TDS_SuplaDeviceChannel_B *channels);
// void ICACHE_FLASH_ATTR Setting_Up_Led_Configure(uint8 led_gpio);
void ICACHE_FLASH_ATTR Setting_Up_Led_Configure(uint8 led_gpio, uint8 led_level);

void ICACHE_FLASH_ATTR Setting_Up_Channel_Relay(TDS_SuplaDeviceChannel_B *channels, uint8 relays_gpio);
void ICACHE_FLASH_ATTR Setting_Up_Channel_Temperature(TDS_SuplaDeviceChannel_B *channels, int type);
void ICACHE_FLASH_ATTR Setting_Up_Channel_Relay_RS(TDS_SuplaDeviceChannel_B *channels);
void ICACHE_FLASH_ATTR Setting_Up_Channel_Relay_Mono(TDS_SuplaDeviceChannel_B *channels, uint8 relays_gpio);
void ICACHE_FLASH_ATTR Setting_Up_Sensor(uint8 sensor_gpio, TDS_SuplaDeviceChannel_B *channels);
void ICACHE_FLASH_ATTR Setting_Up_Button_Relay( uint8 button_gpio, uint8 relay_gpio, uint8 monobistable, TDS_SuplaDeviceChannel_B *channels);
void ICACHE_FLASH_ATTR Setting_Up_User_Configurable(uint8 gpio, TDS_SuplaDeviceChannel_B *channel);

int ICACHE_FLASH_ATTR chartoint(int into);
extern const char * IntToChar[];
extern const char * IntToCharWeb[];

#endif // SUPLA_ESP_BOARD_SET_H_
