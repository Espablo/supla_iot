
#ifndef SUPLA_HTML_INCAN_H_
#define SUPLA_HTML_INCAN_H_

#define HTML_STRONA_START     0
#define HTML_STRONA_HARDWARE  1
#define HTML_STRONA_ADDITIONAL  2

extern const char * Supported_GPIO[];

char * html_template_header_inCan(void);
char * html_template_inCan(char strona);
#endif /* SUPLA_HTML_INCAN_H_ */
