/*
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef SUPLA_ESP_CFG_H_
#define SUPLA_ESP_CFG_H_

#include <c_types.h>

#include "supla-dev/proto.h"
#include "supla_esp.h"

#define BTN_TYPE_MONOSTABLE	0
#define BTN_TYPE_BISTABLE	1
//****************************************dla modulu inCan****************************************

#define SNR_TYPE_NO		0
#define SNR_TYPE_NC		1

#define THERMOMETER_TYPE_NONE			0
#define THERMOMETER_TYPE_DS18B20	1
#define THERMOMETER_TYPE_DHT11		2
#define THERMOMETER_TYPE_DHT22		3

#define ROLLER_SHUTTER_OFF				0
#define ROLLER_SHUTTER_ON					1

#define RELAY_LOW_LEVEL						0
#define RELAY_HIGH_LEVEL					1


#define PIN_GPIO_0		0
#define PIN_GPIO_1		1
#define PIN_GPIO_2		2
#define PIN_GPIO_3		3
#define PIN_GPIO_4		4
#define PIN_GPIO_5		5
#define PIN_GPIO_9		9
#define PIN_GPIO_10		10
#define PIN_GPIO_12		12
#define PIN_GPIO_13		13
#define PIN_GPIO_14		14
#define PIN_GPIO_15 	15
#define PIN_GPIO_16		16

//****************************************KONIEC dla modulu inCan*********************************

typedef struct {

	char TAG[6];
	char GUID[SUPLA_GUID_SIZE];
	char Server[SERVER_MAXSIZE];
	int LocationID;
  char LocationPwd[SUPLA_LOCATION_PWD_MAXSIZE];

  char WIFI_SSID[WIFI_SSID_MAXSIZE];
  char WIFI_PWD[WIFI_PWD_MAXSIZE];

  char CfgButtonType;
  char Button1Type;
  char Button2Type;

char Button3Type;
char Button4Type;
char Button5Type;

  char StatusLedOff;
  char InputCfgTriggerOff;

  char FirmwareUpdate;
	char Test;

  char Email[SUPLA_EMAIL_MAXSIZE];
  char AuthKey[SUPLA_AUTHKEY_SIZE];

  char UpsideDown;

	unsigned int Time1[CFG_TIME1_COUNT];
  unsigned int Time2[CFG_TIME2_COUNT];

 	char Trigger;

	char zero[200];
//****************************************dla modulu inCan****************************************

  char Sensor1Type;
	char Sensor2Type;
	char Sensor3Type;
	char Sensor4Type;
	char Sensor5Type;
	char Sensor6Type;
	char Sensor7Type;
	char Sensor8Type;


  char ThermometerType;
	char ThermometerTypeDHT;
	char RollerShutter;

	char Relay1_GPIO;
	char Relay2_GPIO;
	char Button1_GPIO;
	char Button2_GPIO;
	char Sensor1_GPIO;
	char Sensor2_GPIO;
	char Thermometer_GPIO;
	char Thermometer_DHT_GPIO;
	char LedConfig_GPIO;
	char BtnConfig_GPIO;
  char Config_GPIO;
	char StronaHTML;
	char Relay1Level;
	char Relay2Level;
	// char ButtonConfigType;

	char BoardNames;
	char NumberOfChannels;
	char UartSwap;
	char HostName[SUPLA_HOSTNAME_MAXSIZE];
	char LedConfigLEVEL;
	char PinMode_UserConfigurable[16];
	char GPIO_UserConfigurable[16];
	char RelayLevel[16];
	char RelayFlag[16];
//****************************************KONIEC dla modulu inCan*********************************

}SuplaEspCfg;

typedef struct {
		char TAG[6];
		char GUID[SUPLA_GUID_SIZE];
		char AuthKey[SUPLA_AUTHKEY_SIZE];
		char Server[SERVER_MAXSIZE];
		char Email[SUPLA_EMAIL_MAXSIZE];
		int LocationID;
		char LocationPwd[SUPLA_LOCATION_PWD_MAXSIZE];
		char WIFI_SSID[WIFI_SSID_MAXSIZE];
		char WIFI_PWD[WIFI_PWD_MAXSIZE];
		char CfgButtonType;
		char Button1Type;
		char Button2Type;
		char StatusLedOff;
		char InputCfgTriggerOff;
		char FirmwareUpdate;
		char Test;
		char UpsideDown;
		unsigned int Time1[2];
		unsigned int Time2[2];
		char Trigger;
		char zero[200];

		char BoardNames;
 }SuplaEspCfg_old_v6;


 typedef struct {
		char TAG[6];
		char GUID[SUPLA_GUID_SIZE];
		char Server[SERVER_MAXSIZE];
		int LocationID;
		char LocationPwd[SUPLA_LOCATION_PWD_MAXSIZE];
		char WIFI_SSID[WIFI_SSID_MAXSIZE];
		char WIFI_PWD[WIFI_PWD_MAXSIZE];
		char CfgButtonType;
		char Button1Type;
		char Button2Type;
		char StatusLedOff;
		char InputCfgTriggerOff;
		char FirmwareUpdate;
		char Test;
		char Email[SUPLA_EMAIL_MAXSIZE];
		char AuthKey[SUPLA_AUTHKEY_SIZE];
		char UpsideDown;
		unsigned int Time1[2];
		unsigned int Time2[2];
		char Trigger;
		char zero[200];

		char BoardNames;
 }SuplaEspCfg_old_v5B;

 typedef struct {
		char TAG[6];
		char GUID[SUPLA_GUID_SIZE];
		char Server[SERVER_MAXSIZE];
		int LocationID;
		char LocationPwd[SUPLA_LOCATION_PWD_MAXSIZE];
		char WIFI_SSID[WIFI_SSID_MAXSIZE];
		char WIFI_PWD[WIFI_PWD_MAXSIZE];
		char CfgButtonType;
		char Button1Type;
		char Button2Type;
		char StatusLedOff;
		char InputCfgTriggerOff;
		char FirmwareUpdate;
		char Test;
		unsigned int FullOpeningTime[2];
		unsigned int FullClosingTime[2];
		char Email[SUPLA_EMAIL_MAXSIZE];
		char AuthKey[SUPLA_AUTHKEY_SIZE];
		char UpsideDown;
		char zero[200];

		char BoardNames;
 }SuplaEspCfg_old_v5A;

typedef struct {

	char Relay[RELAY_MAX_COUNT];

  int color[RS_MAX_COUNT];
  char color_brightness[RS_MAX_COUNT];
  char brightness[RS_MAX_COUNT];

	int rs_position[RS_MAX_COUNT];
	char zero[200];

/*
	char ltag;
	char len;
	char log[20][200];
*/

}SuplaEspState;

extern SuplaEspCfg supla_esp_cfg;
extern SuplaEspState supla_esp_state;

char CFG_ICACHE_FLASH_ATTR supla_esp_write_state(char *message);
char CFG_ICACHE_FLASH_ATTR supla_esp_read_state(char *message);

char CFG_ICACHE_FLASH_ATTR supla_esp_cfg_init(void);
char CFG_ICACHE_FLASH_ATTR supla_esp_cfg_ready_to_connect(void);
char CFG_ICACHE_FLASH_ATTR supla_esp_cfg_save(SuplaEspCfg *cfg);
void CFG_ICACHE_FLASH_ATTR supla_esp_save_state(int delay);
void CFG_ICACHE_FLASH_ATTR factory_defaults(char save);


//char CFG_ICACHE_FLASH_ATTR supla_esp_write_log(char *log);


#endif /* SUPLA_ESP_CFG_H_ */
