/*
   ============================================================================
   Name        : supla_esp_board_set.c
   Author      : Espablo
   Copyright   : GPLv2
   ============================================================================
 */
#include <os_type.h>
#include <user_interface.h>
#include <gpio.h>

#include "supla_esp.h"
#include "supla_dht.h"
#include "supla_ds18b20.h"
#include "supla_esp_board_set.h"
#include "supla_esp_gpio.h"

#include "supla_esp_cfg.h"
#include "supla-dev/log.h"


const char * Supported_RelayFlag[3] = {
	"RESET",
	"RESTORE_FORCE"
};

const char * Supported_RelayLevel[2] = {
	"LOW LEVEL",
	"HIGH LEVEL"
};
const char * User_Configurable[END_USER_CONFIGURABLE] = {
	"NONE",
	"EXTERNAL RELAY",
	// "EXTERNAL BUTTON",
	"SENSOR",
	"TERM DS18B20",
	"TERM DHT11",
	"TERM DHT22"
	// "I2C_SCL",
	// "I2C_SDA"
};

const char * Supported_ButtonConfigure[2] = {
	"5s",
	"x10"
};

const char * Supported_Button[2] = {
	"Monostable",
	"Bistable"
};


const char * Supported_LED[MAX_LED_CONFIG_SUPORT] = {
	"OFF",
	"ON",
	"Configuration only"
};

const char * Supported_Temperature[MAX_TEMP_SENSOR_SUPORT] = {
	"NONE",
	"DS18B20",
	"DHT11",
	"DHT22"
};

const char * NodeMCU_GPIO[17] = {
	"(D3)",
	"(TX)",
	"(D4)",
	"(RX)",
	"(D2)",
	"(D1)",
	"",
	"",
	"",
	"(D11)",
	"(D12)",
	"",
	"(D6)",
	"(D7)",
	"(D5)",
	"(D8)",
	"(D0)"
};

const char * IntToChar[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
const char * IntToCharWeb[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "31"};


#ifdef MULTIBOARD_TYPE_SELECTION
void ICACHE_FLASH_ATTR supla_esp_board_gpio_init(void) {
	nr_sensor = 0;
	nr_button = 0;
	nr_pin_configurable = 0;
	nr_temperature = 0;
	input_counter = 0;
	button_configure_set = 0;
	relay_counter = 0;
	phaze50Hz = false;
	led_relay = -1;
	supla_esp_board_set_channels_gpio(0, GPIO);

}

void supla_esp_board_set_channels(TDS_SuplaDeviceChannel_B *channels, unsigned char *channel_count) {
	nr_channel = 0;
	nr_pin_configurable = 0;
	new_channel = -1;
	temperature_channel = 0;
	socket_channel = 0;
	supla_esp_board_set_channels_gpio(channels, CHANNELS);
	*channel_count = nr_channel;

}
#endif

void ICACHE_FLASH_ATTR Setting_Up_Sensor(uint8 sensor_gpio, TDS_SuplaDeviceChannel_B *channels) {

	if (switch_chanel_gpio == GPIO) {

		if (new_channel > -1) {
			supla_input_cfg[input_counter].channel = new_channel;
			if (sensor_gpio == 16) supla_esp_cfg.NumberOfChannels = new_channel;
			new_channel = -1;
		}
		else {
			supla_input_cfg[input_counter].channel = input_counter;
			if (sensor_gpio == 16) supla_esp_cfg.NumberOfChannels = input_counter;
		}
		supla_input_cfg[input_counter].gpio_id = sensor_gpio;
		supla_input_cfg[input_counter].type = INPUT_TYPE_SENSOR;
		supla_input_cfg[input_counter].flags = INPUT_FLAG_PULLUP;
		input_counter++;
		if (sensor_gpio == 0) {
			supla_input_cfg[input_counter].type = INPUT_TYPE_SENSOR;
			supla_input_cfg[input_counter].gpio_id = B_WEJSCIE;
			input_counter++;
		}
		check_gpio_for_uart_and_pullup (sensor_gpio, GPIO_INPUT);
		nr_sensor++;
	}
	else{
		channels[nr_channel].Number = nr_channel;
		channels[nr_channel].Type = SUPLA_CHANNELTYPE_SENSORNO;
		channels[nr_channel].value[0] = !(gpio__input_get(sensor_gpio));
		nr_channel++;
	}

}

void ICACHE_FLASH_ATTR Setting_Up_Button_Relay( uint8 button_gpio, \
                                                uint8 relay_gpio, \
                                                uint8 monobistable, \
                                                TDS_SuplaDeviceChannel_B *channels) {

	if (switch_chanel_gpio == GPIO) {
		if (new_channel > -1) {
			supla_input_cfg[input_counter].channel = new_channel;
			new_channel = -1;
		}
		else supla_input_cfg[input_counter].channel = input_counter;

		supla_input_cfg[input_counter].gpio_id = button_gpio;

		if (phaze50Hz == false) supla_input_cfg[input_counter].flags = INPUT_FLAG_PULLUP;
		supla_input_cfg[input_counter].relay_gpio_id = relay_gpio;

		if (monobistable != INPUT_TYPE_BTN_BISTABLE && monobistable != INPUT_TYPE_BTN_MONOSTABLE) {
			monobistable = monobistable == BTN_TYPE_BISTABLE ? INPUT_TYPE_BTN_BISTABLE : INPUT_TYPE_BTN_MONOSTABLE;
		}

		supla_input_cfg[input_counter].type = monobistable;

		if (new_channel > -1) {
			supla_relay_cfg[relay_counter].channel = new_channel;
			new_channel = -1;
		}
		else supla_relay_cfg[relay_counter].channel = input_counter;

		supla_relay_cfg[relay_counter].gpio_id = relay_gpio;

		if (supla_esp_cfg.RelayLevel[relay_counter] == RELAY_LOW_LEVEL) {
			if (supla_esp_cfg.RelayFlag[relay_counter] == 0) {
				supla_relay_cfg[relay_counter].flags = RELAY_FLAG_RESET \
				                                       | RELAY_FLAG_LO_LEVEL_TRIGGER;
			}
			else  {
				supla_relay_cfg[relay_counter].flags = RELAY_FLAG_RESTORE_FORCE \
				                                       | RELAY_FLAG_LO_LEVEL_TRIGGER;
			}
		}
		else {
			if (supla_esp_cfg.RelayFlag[relay_counter] == 0) {
				supla_relay_cfg[relay_counter].flags = RELAY_FLAG_RESET;
			}
			else  {
				supla_relay_cfg[relay_counter].flags = RELAY_FLAG_RESTORE_FORCE;
			}
		}


		check_gpio_for_uart_and_pullup (button_gpio, GPIO_INPUT);
		check_gpio_for_uart_and_pullup (relay_gpio, GPIO_OUTPUT);
		relay_counter++;
		input_counter++;
		nr_button++;
	}
	else {
		Setting_Up_Channel_Relay(channels, relay_gpio);
	}
}

void ICACHE_FLASH_ATTR Setting_Up_Button( uint8 button_gpio, \
                                          uint8 relay_gpio, \
                                          uint8 monobistable) {

	if (switch_chanel_gpio == GPIO) {
		if (new_channel > -1) {
			supla_input_cfg[input_counter].channel = new_channel;
			new_channel = -1;
		}
		else supla_input_cfg[input_counter].channel = input_counter;
		supla_input_cfg[input_counter].gpio_id = button_gpio;
		supla_input_cfg[input_counter].flags = INPUT_FLAG_PULLUP;
		supla_input_cfg[input_counter].relay_gpio_id = relay_gpio;
		if (monobistable != INPUT_TYPE_BTN_BISTABLE && monobistable != INPUT_TYPE_BTN_MONOSTABLE) {
			monobistable = monobistable == BTN_TYPE_BISTABLE ? INPUT_TYPE_BTN_BISTABLE : INPUT_TYPE_BTN_MONOSTABLE;
		}
		supla_input_cfg[input_counter].type = monobistable;

		check_gpio_for_uart_and_pullup (button_gpio, GPIO_INPUT);
		check_gpio_for_uart_and_pullup (relay_gpio, GPIO_OUTPUT);

		input_counter++;
		nr_button++;
	}
}

void ICACHE_FLASH_ATTR Setting_Up_Relay(uint8 relay_gpio, TDS_SuplaDeviceChannel_B *channels) {

	if (switch_chanel_gpio == GPIO) {
		supla_relay_cfg[relay_counter].channel = input_counter;
		supla_relay_cfg[relay_counter].gpio_id = relay_gpio;
		if (supla_esp_cfg.RelayLevel[relay_counter] == RELAY_LOW_LEVEL) {
			if (supla_esp_cfg.RelayFlag[relay_counter] == 0) {
				supla_relay_cfg[relay_counter].flags = RELAY_FLAG_RESET \
				                                       | RELAY_FLAG_LO_LEVEL_TRIGGER;
			}
			else  {
				supla_relay_cfg[relay_counter].flags = RELAY_FLAG_RESTORE_FORCE \
				                                       | RELAY_FLAG_LO_LEVEL_TRIGGER;
			}
		}
		else {
			if (supla_esp_cfg.RelayFlag[relay_counter] == 0) {
				supla_relay_cfg[relay_counter].flags = RELAY_FLAG_RESET;
			}
			else  {
				supla_relay_cfg[relay_counter].flags = RELAY_FLAG_RESTORE_FORCE;
			}
		}
		check_gpio_for_uart_and_pullup (relay_gpio, GPIO_OUTPUT);
		relay_counter++;
		input_counter++;
	}
	else{
		Setting_Up_Channel_Relay(channels, relay_gpio);
	}
}

void ICACHE_FLASH_ATTR Setting_Up_Button_Configure(uint8 gpio, uint8 type) {

	if (button_configure_set == 0) {
		if (type == BUTTON_CONFIGURE_5s || type == BUTTON_CONFIGURE_x10) {
			supla_esp_cfg.CfgButtonType = type ==  BUTTON_CONFIGURE_x10 ? BTN_TYPE_BISTABLE : BTN_TYPE_MONOSTABLE;
			supla_input_cfg[input_counter].type = type;
		}
		else {
			button_configure_set++;
			supla_input_cfg[input_counter].type = supla_esp_cfg.CfgButtonType == BTN_TYPE_BISTABLE ? BUTTON_CONFIGURE_x10 : BUTTON_CONFIGURE_5s;
		}
	}

	supla_input_cfg[input_counter].gpio_id = gpio;
	supla_input_cfg[input_counter].flags = INPUT_FLAG_PULLUP | INPUT_FLAG_CFG_BTN;
	input_counter++;
	if (gpio == 0) {
		supla_input_cfg[input_counter].type = INPUT_TYPE_SENSOR;
		supla_input_cfg[input_counter].gpio_id = B_WEJSCIE;
		input_counter++;
	}
	check_gpio_for_uart_and_pullup (gpio, GPIO_INPUT);
}

void ICACHE_FLASH_ATTR Setting_Up_Led_Configure(uint8 led_gpio, uint8 led_level) {
	supla_esp_cfg.LedConfig_GPIO = led_gpio;
	supla_esp_cfg.LedConfigLEVEL = led_level;
	check_gpio_for_uart_and_pullup (led_gpio, GPIO_OUTPUT);
	led_set = 1;
}
void ICACHE_FLASH_ATTR Setting_Up_Led_Relay(uint8 led_gpio, uint8 relay_gpio) {

	check_gpio_for_uart_and_pullup (led_gpio, GPIO_OUTPUT);
}

void ICACHE_FLASH_ATTR Setting_Up_Buttons_RS( uint8 gpio_buton_up, \
                                              uint8 gpio_buton_down, \
                                              uint8 relay_up, \
                                              uint8 relay_down, \
                                              uint8 mono_bistable, \
                                              TDS_SuplaDeviceChannel_B *channels) {
	supla_esp_cfg.RollerShutter = ROLLER_SHUTTER_ON;
	if (switch_chanel_gpio == GPIO) {

		if (mono_bistable != INPUT_TYPE_BTN_BISTABLE && mono_bistable != INPUT_TYPE_BTN_MONOSTABLE_RS) {
			mono_bistable = mono_bistable == BTN_TYPE_BISTABLE ? INPUT_TYPE_BTN_BISTABLE : INPUT_TYPE_BTN_MONOSTABLE_RS;
		}

		supla_input_cfg[input_counter].gpio_id = gpio_buton_up;
		supla_input_cfg[input_counter].relay_gpio_id = relay_up;
		supla_input_cfg[input_counter].type = mono_bistable;
		supla_input_cfg[input_counter].flags = INPUT_FLAG_PULLUP;
		uint8 tmp_counter = input_counter;
		input_counter++;

		supla_input_cfg[input_counter].gpio_id = gpio_buton_down;
		supla_input_cfg[input_counter].relay_gpio_id = relay_down;
		supla_input_cfg[input_counter].type = mono_bistable;
		supla_input_cfg[input_counter].flags = INPUT_FLAG_PULLUP;
		input_counter++;

		if (supla_esp_cfg.RelayLevel[relay_counter] == RELAY_LOW_LEVEL) {
			supla_relay_cfg[relay_counter].flags = RELAY_FLAG_LO_LEVEL_TRIGGER;
		}
		supla_relay_cfg[relay_counter].gpio_id = relay_up;
		supla_relay_cfg[relay_counter].channel = tmp_counter;
		supla_rs_cfg[0].up = &supla_relay_cfg[relay_counter];
		relay_counter++;

		if (supla_esp_cfg.RelayLevel[relay_counter] == RELAY_LOW_LEVEL) {
			supla_relay_cfg[relay_counter].flags = RELAY_FLAG_LO_LEVEL_TRIGGER;
		}
		supla_relay_cfg[relay_counter].gpio_id = relay_down;
		supla_relay_cfg[relay_counter].channel = tmp_counter;
		supla_rs_cfg[0].down = &supla_relay_cfg[relay_counter];

		check_gpio_for_uart_and_pullup (gpio_buton_up, GPIO_INPUT);
		check_gpio_for_uart_and_pullup (gpio_buton_down, GPIO_INPUT);
		check_gpio_for_uart_and_pullup (relay_up, GPIO_OUTPUT);
		check_gpio_for_uart_and_pullup (relay_down, GPIO_OUTPUT);
		relay_counter++;
		nr_button++;
	}
	else{
		Setting_Up_Channel_Relay_RS(channels);
	}
}

void ICACHE_FLASH_ATTR Setting_Up_Relay_RS(uint8 relay_up, \
                                           uint8 relay_down, \
                                           TDS_SuplaDeviceChannel_B *channels) {
	if (switch_chanel_gpio == GPIO) {
		supla_relay_cfg[relay_counter].gpio_id = relay_up;
		supla_relay_cfg[relay_counter].channel = input_counter;
		supla_rs_cfg[0].up = &supla_relay_cfg[relay_counter];
		relay_counter++;

		supla_relay_cfg[relay_counter].gpio_id = relay_down;
		supla_relay_cfg[relay_counter].channel = input_counter;
		supla_rs_cfg[0].down = &supla_relay_cfg[relay_counter];

		check_gpio_for_uart_and_pullup (relay_up, GPIO_OUTPUT);
		check_gpio_for_uart_and_pullup (relay_down, GPIO_OUTPUT);
		relay_counter++;
	}
	else {
		Setting_Up_Channel_Relay_RS(channels);
	}
}

void ICACHE_FLASH_ATTR Setting_Up_Temperature(uint8 gpio, int type, TDS_SuplaDeviceChannel_B *channels) {

	if (switch_chanel_gpio == GPIO) {
		if (nr_temperature < 252) {
			nr_temperature++;
		}

		if(type != SUPLA_CHANNELTYPE_DHT11 && type != SUPLA_CHANNELTYPE_DHT22 && type != SUPLA_CHANNELTYPE_THERMOMETERDS18B20) {

			if(type == THERMOMETER_TYPE_DHT22 || type == THERMOMETER_TYPE_DHT11) {
				supla_esp_cfg.Thermometer_DHT_GPIO = gpio;
				if (nr_temperature == 253) {
					supla_esp_cfg.ThermometerTypeDHT = type;
				}
			}
			else if(type == THERMOMETER_TYPE_DS18B20) {
				supla_esp_cfg.Thermometer_GPIO = gpio;
			}
			else {
				supla_esp_cfg.Thermometer_DHT_GPIO = gpio;
				supla_esp_cfg.Thermometer_GPIO = gpio;
			}
		}
		check_gpio_for_uart_and_pullup (gpio, GPIO_INPUT);
		input_counter++;

	}
	else{
		if(type != SUPLA_CHANNELTYPE_DHT11 && type != SUPLA_CHANNELTYPE_DHT22 && type != SUPLA_CHANNELTYPE_THERMOMETERDS18B20) {
			if(type == THERMOMETER_TYPE_DHT22) {
				type = SUPLA_CHANNELTYPE_DHT22;
			}
			else if(type == THERMOMETER_TYPE_DHT11) {
				type = SUPLA_CHANNELTYPE_DHT11;
			}
			else if(type == THERMOMETER_TYPE_DS18B20) {
				type = SUPLA_CHANNELTYPE_THERMOMETERDS18B20;
			}
		}
		if (type != THERMOMETER_TYPE_NONE) {
			Setting_Up_Channel_Temperature(channels, type);
		}
	}
}



void ICACHE_FLASH_ATTR Setting_Up_Channel_Relay(TDS_SuplaDeviceChannel_B *channels, uint8 relays_gpio){

	channels[nr_channel].Number = nr_channel;
	channels[nr_channel].Type = SUPLA_CHANNELTYPE_RELAY;
	if (new_nr_channel == 0) new_nr_channel = nr_channel;
	if (supla_input_cfg[new_nr_channel].type == INPUT_TYPE_BTN_MONOSTABLE || socket_channel == ONLY_SOCKET_CHANNEL) {
		channels[nr_channel].FuncList =  SUPLA_BIT_RELAYFUNC_LIGHTSWITCH \
		                                | SUPLA_BIT_RELAYFUNC_POWERSWITCH;
	}
	else {
		channels[nr_channel].FuncList =  SUPLA_BIT_RELAYFUNC_LIGHTSWITCH \
		                                | SUPLA_BIT_RELAYFUNC_POWERSWITCH \
		                                | SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEGATEWAYLOCK \
		                                | SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEGATE \
		                                | SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEGARAGEDOOR \
		                                | SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEDOORLOCK \
		                                | SUPLA_BIT_RELAYFUNC_STAIRCASETIMER;
	}
	channels[nr_channel].Default = SUPLA_CHANNELFNC_LIGHTSWITCH;
	channels[nr_channel].value[0] = supla_esp_gpio_relay_on(relays_gpio);
	nr_channel++;
	new_nr_channel = 0;
	socket_channel = 0;
}

void ICACHE_FLASH_ATTR Setting_Up_Channel_Relay_Mono(TDS_SuplaDeviceChannel_B *channels, uint8 relays_gpio){

	channels[nr_channel].Number = nr_channel;
	channels[nr_channel].Type = SUPLA_CHANNELTYPE_RELAY;
	channels[nr_channel].FuncList =  SUPLA_BIT_RELAYFUNC_LIGHTSWITCH \
	                                | SUPLA_BIT_RELAYFUNC_POWERSWITCH;
	channels[nr_channel].Default = SUPLA_CHANNELFNC_LIGHTSWITCH;
	channels[nr_channel].value[0] = supla_esp_gpio_relay_on(relays_gpio);
	nr_channel++;
}

void ICACHE_FLASH_ATTR Setting_Up_Channel_Relay_RS(TDS_SuplaDeviceChannel_B *channels){
	channels[nr_channel].Number = nr_channel;
	channels[nr_channel].Type = SUPLA_CHANNELTYPE_RELAY;
	channels[nr_channel].FuncList =  SUPLA_BIT_RELAYFUNC_CONTROLLINGTHEROLLERSHUTTER;
	channels[nr_channel].Default = SUPLA_CHANNELFNC_CONTROLLINGTHEROLLERSHUTTER;
	channels[nr_channel].value[0] = (*supla_rs_cfg[0].position)-1;
	nr_channel++;
}

void ICACHE_FLASH_ATTR Setting_Up_Channel_Temperature(TDS_SuplaDeviceChannel_B *channels, int type){


	channels[nr_channel].Number = nr_channel;
	channels[nr_channel].Type = type;
	channels[nr_channel].FuncList = 0;


	if(type == SUPLA_CHANNELTYPE_DHT11 || type == SUPLA_CHANNELTYPE_DHT22) {
		channels[nr_channel].Default = SUPLA_CHANNELFNC_HUMIDITYANDTEMPERATURE;
		temperature_and_humidity_channel = nr_channel;
		supla_get_temp_and_humidity(channels[nr_channel].value);
	}
	else {
		channels[nr_channel].Default = 0;
		temperature_channel = nr_channel;
		supla_get_temperature(channels[nr_channel].value);
	}
	nr_channel++;
}

int ICACHE_FLASH_ATTR chartoint(int into){
	if (into >= 17) into = into - 7;
	return into;
}

void check_gpio_for_uart_and_pullup(uint8 check_gpio, uint8 inout) {

	if (check_gpio == 0) {
		if (inout == GPIO_INPUT) {
			if (phaze50Hz == false) PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO0_U);
			else PIN_PULLUP_DIS(PERIPHS_IO_MUX_GPIO0_U);
		}
		else PIN_PULLUP_DIS(PERIPHS_IO_MUX_GPIO0_U);
	}
	if (check_gpio == 1) {
		supla_esp_cfg.UartSwap = 1;
		if (inout == GPIO_INPUT) {
			uart_tx_io =  GPIO_INPUT;
			PIN_PULLUP_EN(PERIPHS_IO_MUX_U0TXD_U);
		}
		else uart_tx_io =  GPIO_OUTPUT;
	}
	if (check_gpio == 2) {
		if (inout == GPIO_INPUT) {
			if (phaze50Hz == false) PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO2_U);
			else PIN_PULLUP_DIS(PERIPHS_IO_MUX_GPIO2_U);
		}
		else PIN_PULLUP_DIS(PERIPHS_IO_MUX_GPIO2_U);
	}

	if (check_gpio == 3) {
		supla_esp_cfg.UartSwap = 1;
		if (inout == GPIO_INPUT) {
			uart_rx_io =  GPIO_INPUT;
			PIN_PULLUP_EN(PERIPHS_IO_MUX_U0RXD_U);
		}
		else uart_rx_io =  GPIO_OUTPUT;
	}

	if (check_gpio == 4) {
		if (inout == GPIO_INPUT) {
			if (phaze50Hz == false) PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO4_U);
			else PIN_PULLUP_DIS(PERIPHS_IO_MUX_GPIO4_U);
		}
		else PIN_PULLUP_DIS(PERIPHS_IO_MUX_GPIO4_U);
	}

	if (check_gpio == 5) {
		if (inout == GPIO_INPUT) {
			if (phaze50Hz == false) PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO5_U);
			else PIN_PULLUP_DIS(PERIPHS_IO_MUX_GPIO5_U);
		}
		else PIN_PULLUP_DIS(PERIPHS_IO_MUX_GPIO5_U);
	}

	if (check_gpio == 9) {
		if (inout == GPIO_INPUT) {
			if (phaze50Hz == false) PIN_PULLUP_EN(PERIPHS_IO_MUX_SD_DATA2_U);
			else PIN_PULLUP_DIS(PERIPHS_IO_MUX_SD_DATA2_U);
		}
		else PIN_PULLUP_DIS(PERIPHS_IO_MUX_SD_DATA2_U);
	}

	if (check_gpio == 10) {
		if (inout == GPIO_INPUT) {
			if (phaze50Hz == false) PIN_PULLUP_EN(PERIPHS_IO_MUX_SD_DATA3_U);
			else PIN_PULLUP_DIS(PERIPHS_IO_MUX_SD_DATA3_U);
		}
		else PIN_PULLUP_DIS(PERIPHS_IO_MUX_SD_DATA3_U);
	}

	if (check_gpio == 12) {
		if (inout == GPIO_INPUT) {
			if (phaze50Hz == false) PIN_PULLUP_EN(PERIPHS_IO_MUX_MTDI_U);
			else PIN_PULLUP_DIS(PERIPHS_IO_MUX_MTDI_U);
		}
		else PIN_PULLUP_DIS(PERIPHS_IO_MUX_MTDI_U);
	}

	if (check_gpio == 13) {
		if (inout == GPIO_INPUT) {
			if (phaze50Hz == false) PIN_PULLUP_EN(PERIPHS_IO_MUX_MTCK_U);
			else PIN_PULLUP_DIS(PERIPHS_IO_MUX_MTCK_U);
		}
		else PIN_PULLUP_DIS(PERIPHS_IO_MUX_MTCK_U);
	}

	if (check_gpio == 14) {
		if (inout == GPIO_INPUT) {
			if (phaze50Hz == false) PIN_PULLUP_EN(PERIPHS_IO_MUX_MTMS_U);
			else PIN_PULLUP_DIS(PERIPHS_IO_MUX_MTMS_U);
		}
		else PIN_PULLUP_DIS(PERIPHS_IO_MUX_MTMS_U);
	}

	if (check_gpio == 15) {
		if (inout == GPIO_INPUT) {
			if (phaze50Hz == false) PIN_PULLUP_EN(PERIPHS_IO_MUX_MTDO_U);
			else PIN_PULLUP_DIS(PERIPHS_IO_MUX_MTDO_U);
		}
		else PIN_PULLUP_DIS(PERIPHS_IO_MUX_MTDO_U);
	}
	if (inout == GPIO_OUTPUT) {
		GPIO_OUTPUT_SET(GPIO_ID_PIN(check_gpio), 0);
	}
}

void ICACHE_FLASH_ATTR Setting_Up_User_Configurable(uint8 gpio, TDS_SuplaDeviceChannel_B *channel){

	supla_esp_cfg.GPIO_UserConfigurable[nr_pin_configurable] = gpio;

	if (supla_esp_cfg.PinMode_UserConfigurable[nr_pin_configurable] == NONE) {
		// return;
	}
	else if (supla_esp_cfg.PinMode_UserConfigurable[nr_pin_configurable] == EXTERNAL_RELAY) {
		Setting_Up_Relay(gpio, channel );
	}
	// else if (supla_esp_cfg.PinMode_UserConfigurable[nr_pin_configurable] == EXTERNAL_BUTTON) {
	//
	// }
	else if (supla_esp_cfg.PinMode_UserConfigurable[nr_pin_configurable] == SENSOR) {
		Setting_Up_Sensor(gpio, channel);
	}
	else if (supla_esp_cfg.PinMode_UserConfigurable[nr_pin_configurable] == TERM_DS18B20) {
		nr_temperature = 252;
		Setting_Up_Temperature(gpio, THERMOMETER_TYPE_DS18B20, channel);
		nr_temperature = 254;
	}
	else if (supla_esp_cfg.PinMode_UserConfigurable[nr_pin_configurable] == TERM_DHT11) {
		nr_temperature = 252;
		Setting_Up_Temperature(gpio, THERMOMETER_TYPE_DHT11, channel);
		nr_temperature = 254;
	}
	else if (supla_esp_cfg.PinMode_UserConfigurable[nr_pin_configurable] == TERM_DHT22) {
		nr_temperature = 252;
		Setting_Up_Temperature(gpio, THERMOMETER_TYPE_DHT22, channel);
		nr_temperature = 254;
	}
	// else if (supla_esp_cfg.PinMode_UserConfigurable[nr_pin_configurable] == I2C_SCL) {
	//
	// }
	// else if (supla_esp_cfg.PinMode_UserConfigurable[nr_pin_configurable] == I2C_SDA) {
	//
	// }

	nr_pin_configurable++;
	// supla_log(LOG_DEBUG, "nr_pin_configurable - %i", nr_pin_configurable);
}
