



#ifndef _SUPLA_GFX_H_
#define _SUPLA_GFX_H_

#include "supla_esp.h"


#ifdef OLED_DISPLAY


#include "supla_ssd1306.h"
#include "supla_bitmap.h"
#include "supla_font.h"

#define PROGMEM   ICACHE_RODATA_ATTR
#define ICACHE_RODATA_ATTR  __attribute__((section(".irom.text")))

#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef _swap_int16_t
#define _swap_int16_t(a, b) { int16_t t = a; a = b; b = t; }
#endif

uint8_t ssd1306_bufor[BUF_SIZE];
extern int cursor_x, cursor_y;

void ICACHE_FLASH_ATTR ssd1306_position_xy(int x, int y);
void ICACHE_FLASH_ATTR ssd1306_put_int(int data, uint8_t txt_size, uint8_t color, uint8_t bg);
void ICACHE_FLASH_ATTR ssd1306_puts(char * str, uint8_t txt_size, uint8_t color, uint8_t bg);
void ICACHE_FLASH_ATTR ssd1306_drawChar(int x, int y,  char c, uint8_t color, uint8_t bg, uint8_t size);
void ICACHE_FLASH_ATTR ssd1306_fill_rect(int x, int y,  int w, int h, uint8_t color);
void ICACHE_FLASH_ATTR ssd1306_drawFastVLine(int x, int y,  int h, uint8_t color);
void ICACHE_FLASH_ATTR ssd1306_drawLine(int x0, int y0, int x1, int y1, uint8_t color);
void ICACHE_FLASH_ATTR ssd1306_drawBitmap(int x, int y, const uint8_t bitmap[], int w, int h, uint8_t color);


#endif

#endif /* _SUPLA_GFX_H_ */
